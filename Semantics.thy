theory Semantics
  imports Main Syntax
begin

type_synonym 'a state = "var \<Rightarrow> 'a"
type_synonym 'a heap  = "'a  \<rightharpoonup> 'a"
type_synonym 'a fsem  = "fsym \<Rightarrow> 'a list \<Rightarrow> 'a"

type_synonym locks = "lsym set"

datatype 'a conf
  = Abort
  | Stop locks "'a state" "'a heap"
  | Run cmd locks "'a state" "'a heap"

fun state :: "'a conf \<Rightarrow> (locks * 'a state * 'a heap)" where
"state Abort = (undefined, undefined, undefined)" |
"state (Stop L s h) = (L,s,h)" |
"state (Run c L s h) = (L,s,h)"

datatype tick
  = Tick | Fst | Snd
type_synonym sched = "tick list"

locale semantics =
  (* interpretation of function symbols *)
  fixes F :: "'a fsem"

  (* introduce a few semantic constants + any axioms needed *)
  fixes true  :: 'a
  fixes false :: 'a
  assumes true_neq_false: "true \<noteq> false"

  fixes low   :: 'a
  fixes high  :: 'a
  fixes to_level :: "'a \<Rightarrow> 'level::complete_lattice"
  fixes to_val   :: "'level \<Rightarrow> 'a"
  fixes level_str  :: "'level \<Rightarrow> string"

  (* fix an interpretation for predefined symbols *)
  assumes low   [simp]: "F (''low'')   [] = low"
  assumes high  [simp]: "F (''high'')  [] = high"
  assumes not   [simp]: "F (''not'')   [a] = (if a = true then false else true)"
  assumes imp   [simp]: "F (''imp'')   [a, b] = (if a = true \<longrightarrow> b = true then true else false)"
  assumes eq    [simp]: "F (''='')     [a, b] = (if a = b then true  else false)"
  assumes ite   [simp]: "F (''ite'')   [a, b, c] = (if a = true then b else c)"
  assumes lower [simp]: "F (''lower'') [a, b] = (if (to_level a) \<le> (to_level b) then true  else false)"
  assumes low_bot [simp]: "to_level low = (bot::'level)"
  assumes high_top [simp]: "to_level high = (top::'level)"
  assumes to_val_to_level [simp]: "to_level (to_val lev) = lev"
  assumes level_str_works [simp]: "F (level_str lev) [] = (to_val lev)"

context semantics
begin

definition
  level_expr :: "'level \<Rightarrow> expr"
  where
  "level_expr lev \<equiv> (App ((level_str lev)) [])"
  
fun
  expr_sem  :: "expr      \<Rightarrow> 'a state \<Rightarrow> 'a" and
  expr_sems :: "expr list \<Rightarrow> 'a state \<Rightarrow> 'a list" where
"expr_sem (Var x) s = (s x)" |
"expr_sem (App f as) s = F f (expr_sems as s)" |
"expr_sems [] s = []" |
"expr_sems (e#es) s = expr_sem e s # expr_sems es s"

lemma expr_sems_map:
"expr_sems es s = map (\<lambda> e. expr_sem e s) es"
  by (induction es, auto)

lemma level_expr_sem [simp]:
  "expr_sem (level_expr lev) s = (to_val lev)"
  by(simp add: level_expr_def)

lemma expr_sems_append[simp]:
"expr_sems (as @ bs) s = expr_sems as s @ expr_sems bs s"
  by (induction as) simp_all

definition pure_sem :: "expr \<Rightarrow> 'a state \<Rightarrow> bool" where 
"pure_sem b s \<equiv> expr_sem b s = true"

lemma pure_sem_intro[intro]:
"expr_sem b s = true \<Longrightarrow> pure_sem b s"
  unfolding pure_sem_def by simp

lemma [simp]: "expr_sem Low  s = low"
  unfolding Low_def  by simp
lemma [simp]: "expr_sem High s = high"
  unfolding High_def by simp
lemma [simp]: "pure_sem (Lower l l') s = (to_level (expr_sem l s) \<le> to_level (expr_sem l' s))"
  unfolding Lower_def pure_sem_def using true_neq_false by simp
lemma [simp]: "pure_sem (Eq a b) s = (expr_sem a s = expr_sem b s)"
  unfolding Eq_def pure_sem_def using true_neq_false by auto
lemma [simp]: "pure_sem (Not p) s = (\<not> pure_sem p s)"
  unfolding Not_def pure_sem_def using true_neq_false by simp
lemma [simp]: "pure_sem (Imp p q) s = (pure_sem p s \<longrightarrow> pure_sem q s)"
  unfolding Imp_def pure_sem_def using true_neq_false by simp
lemma [simp]: "expr_sem (Ite a b c) s = (if pure_sem a s then expr_sem b s else expr_sem c s)"
  unfolding Ite_def pure_sem_def using true_neq_false by simp


inductive cmd_step :: "sched \<Rightarrow> 'a conf \<Rightarrow> 'a conf \<Rightarrow> bool" where
"cmd_step [Tick] (Run Skip L s h) (Stop L s h)" |

"\<lbrakk>v = expr_sem e s\<rbrakk>
  \<Longrightarrow> cmd_step [Tick] (Run (Assign x e) L s h) (Stop L (s(x:=v)) h)" |

(* Load *)
"\<lbrakk>a = expr_sem p s; h a = None\<rbrakk>
  \<Longrightarrow> cmd_step [Tick] (Run (Load x p) L s h) Abort" |

"\<lbrakk>a = expr_sem p s; h a = Some v\<rbrakk>
  \<Longrightarrow> cmd_step [Tick] (Run (Load x p) L s h) (Stop L (s(x:=v)) h)" |

(* Store *)
"\<lbrakk>a = expr_sem p s; h a = None\<rbrakk>
  \<Longrightarrow> cmd_step [Tick] (Run (Store  p e) L s h) Abort" |

"\<lbrakk>a = expr_sem p s; h a = Some v; v' = expr_sem e s\<rbrakk>
  \<Longrightarrow> cmd_step [Tick] (Run (Store  p e) L s h) (Stop L s (h(a \<mapsto> v')))" |

(* Lock/Unlock *)

"\<lbrakk>l \<in> L\<rbrakk>
  \<Longrightarrow> cmd_step [Tick] (Run (Lock l) L s h) (Stop (L - {l}) s h)" |

"\<lbrakk>l \<notin> L\<rbrakk>
  \<Longrightarrow> cmd_step [Tick] (Run (Unlock l) L s h) (Stop (L \<union> {l}) s h)" |

(* Seq *)
"\<lbrakk>cmd_step \<sigma> (Run c1 L s h) Abort\<rbrakk>
  \<Longrightarrow> cmd_step \<sigma> (Run (Seq c1 c2) L s h) Abort" |

"\<lbrakk>cmd_step \<sigma> (Run c1 L s h) (Stop L' s' h')\<rbrakk>
  \<Longrightarrow> cmd_step \<sigma> (Run (Seq c1 c2) L s h) (Run c2 L' s' h')" |

"\<lbrakk>cmd_step \<sigma> (Run c1 L s h) (Run c1' L' s' h')\<rbrakk>
  \<Longrightarrow> cmd_step \<sigma> (Run (Seq c1 c2) L s h) (Run (Seq c1' c2) L' s' h')" |

(* Par first *)
"\<lbrakk>cmd_step \<sigma> (Run c1 L s h) Abort\<rbrakk>
  \<Longrightarrow> cmd_step (Fst#\<sigma>) (Run (Par c1 c2) L s h) Abort" |

"\<lbrakk>cmd_step \<sigma> (Run c1 L s h) (Stop L' s' h')\<rbrakk>
  \<Longrightarrow> cmd_step (Fst#\<sigma>) (Run (Par c1 c2) L s h) (Run c2 L' s' h')" |

"\<lbrakk>cmd_step \<sigma> (Run c1 L s h) (Run c1' L' s' h')\<rbrakk>
  \<Longrightarrow> cmd_step (Fst#\<sigma>) (Run (Par c1 c2) L s h) (Run (Par c1' c2) L' s' h')" |

(* Par second *)
"\<lbrakk>cmd_step \<sigma> (Run c2 L s h) Abort\<rbrakk>
  \<Longrightarrow> cmd_step (Snd#\<sigma>) (Run (Par c1 c2) L s h) Abort" |

"\<lbrakk>cmd_step \<sigma> (Run c2 L s h) (Stop L' s' h')\<rbrakk>
  \<Longrightarrow> cmd_step (Snd#\<sigma>) (Run (Par c1 c2) L s h) (Run c1 L' s' h')" |

"\<lbrakk>cmd_step \<sigma> (Run c2 L s h) (Run c2' L' s' h')\<rbrakk>
  \<Longrightarrow> cmd_step (Snd#\<sigma>) (Run (Par c1 c2) L s h) (Run (Par c1 c2') L' s' h')" |

(* If *)
"\<lbrakk>  pure_sem b s\<rbrakk>
  \<Longrightarrow> cmd_step [Tick] (Run (If b c1 c2) L s h) (Run c1 L s h)" |

"\<lbrakk>\<not> pure_sem b s\<rbrakk>
  \<Longrightarrow> cmd_step [Tick] (Run (If b c1 c2) L s h) (Run c2 L s h)" |

(* While *)
"\<lbrakk>  pure_sem b s\<rbrakk>
  \<Longrightarrow> cmd_step [Tick] (Run (While b c) L s h) (Run (Seq c (While b c)) L s h)" |

"\<lbrakk>\<not> pure_sem b s\<rbrakk>
  \<Longrightarrow> cmd_step [Tick] (Run (While b c) L s h) (Stop L s h)"

inductive_cases cmd_step_stop[elim!]:   "cmd_step \<sigma> (Stop L s h) k'"
inductive_cases cmd_step_abort[elim!]:  "cmd_step \<sigma> Abort k'"

inductive_cases cmd_step_skip[elim!]:   "cmd_step \<sigma> (Run  Skip L s h) k'"
inductive_cases cmd_step_assign[elim!]: "cmd_step \<sigma> (Run (Assign x e) L s h) k'"
inductive_cases cmd_step_load[elim!]:   "cmd_step \<sigma> (Run (Load   x p) L s h) k'"
inductive_cases cmd_step_store[elim!]:  "cmd_step \<sigma> (Run (Store  p e) L s h) k'"
inductive_cases cmd_step_lock[elim!]:   "cmd_step \<sigma> (Run (Lock l) L s h) k'"
inductive_cases cmd_step_unlock[elim!]: "cmd_step \<sigma> (Run (Unlock l) L s h) k'"
inductive_cases cmd_step_seq[elim!]:    "cmd_step \<sigma> (Run (Seq  c1 c2)  L s h) k'"
inductive_cases cmd_step_par[elim!]:    "cmd_step \<sigma> (Run (Par  c1 c2)  L s h) k'"
inductive_cases cmd_step_par_fst[elim!]:"cmd_step (Fst#\<sigma>) (Run (Par  c1 c2)  L s h) k'"
inductive_cases cmd_step_par_snd[elim!]:"cmd_step (Snd#\<sigma>) (Run (Par  c1 c2)  L s h) k'"
inductive_cases cmd_step_if[elim!]:     "cmd_step \<sigma> (Run (If b c1 c2)  L s h) k'"
inductive_cases cmd_step_while:         "cmd_step \<sigma> (Run (While b c)   L s h) k'"

lemma cmd_step_sched_nonempty:
"cmd_step \<sigma> k k' \<Longrightarrow> \<sigma> \<noteq> []"
  by (induction \<sigma> k k' rule: cmd_step.induct) auto
corollary [simp]: "cmd_step [] k k' = False"
  using cmd_step_sched_nonempty[of "[]" k k'] by auto

lemma cmd_sched_determ:
"\<lbrakk>cmd_step \<sigma>1 (Run c L s h) k; cmd_step \<sigma>2 (Run c L s' h') k'; \<sigma> = \<sigma>1@\<sigma>1'; \<sigma> = \<sigma>2@\<sigma>2'\<rbrakk> \<Longrightarrow> (\<sigma>1 = \<sigma>2)"
  by (induction \<sigma>1 "Run c L s h" k arbitrary: \<sigma> \<sigma>2 c k' rule: cmd_step.induct, auto elim: cmd_step_while)

lemma cmd_step_determ[simp]:
"cmd_step \<sigma> k k' \<Longrightarrow> cmd_step \<sigma> k k'' = (k' = k'')"
  by (rule iffI, induction \<sigma> k k' arbitrary: k'' rule: cmd_step.induct, auto elim: cmd_step_while)

(* can't use rtranclp as it unwinds at the end *)
inductive cmd_steps :: "sched \<Rightarrow> 'a conf \<Rightarrow> 'a conf \<Rightarrow> bool" where
cmd_steps_refl[intro]: "cmd_steps [] k k" |
cmd_steps_step:        "cmd_step \<sigma> k k' \<Longrightarrow> cmd_steps \<sigma>' k' k'' \<Longrightarrow> cmd_steps (\<sigma>@\<sigma>') k k''"

lemma cmd_step1_intro:
"cmd_step \<sigma> k k' \<Longrightarrow> cmd_steps \<sigma> k k'"
  using cmd_steps_step[of \<sigma> k k' "[]" k'] by auto

lemma cmd_steps_Zero[simp]:
"cmd_steps [] k k' = (k = k')"
  by (auto elim: cmd_steps.cases)

lemma cmd_steps_Abort[simp]:
"cmd_steps \<sigma> Abort k = (k = Abort \<and> \<sigma> = [])"
  by (auto elim: cmd_steps.cases)

lemma cmd_steps_Stop[simp]:
"cmd_steps \<sigma> (Stop L s h) k = (k = (Stop L s h) \<and> \<sigma> = [])"
  by (auto elim: cmd_steps.cases)

lemma cmd_steps_Run:
"cmd_steps \<sigma> (Run c L s h) k''
  \<Longrightarrow> \<sigma> = [] \<and> k'' = (Run c L s h) \<or> (\<exists> \<sigma>' \<sigma>'' k'. \<sigma> = (\<sigma>'@\<sigma>'') \<and> cmd_step \<sigma>' (Run c L s h) k' \<and> cmd_steps \<sigma>'' k' k'')"
  by (auto elim: cmd_steps.cases)

lemma cmd_steps_Run':
"cmd_steps \<sigma> (Run c L s h) (Stop L'' s'' h'')
  \<Longrightarrow> (\<exists> \<sigma>' \<sigma>'' k'. \<sigma> = (\<sigma>'@\<sigma>'') \<and> cmd_step \<sigma>' (Run c L s h) k' \<and> cmd_steps \<sigma>'' k' (Stop L'' s'' h''))"
  by (auto dest!: cmd_steps_Run)

lemma Stop_dest[dest!]:
"cmd_steps \<sigma> (Stop L s h) k
  \<Longrightarrow> \<sigma> = [] \<and> k = (Stop L s h)"
  by (induction \<sigma> "(Stop L s h)" "k" rule: cmd_steps.induct, auto)

lemma Skip_elim[elim!]:
"cmd_steps \<sigma> (Run Skip L s h) (Stop L' s' h') \<Longrightarrow>
  (\<And> v. \<lbrakk>\<sigma> = [Tick]; L = L'; s' = s; h' = h\<rbrakk> \<Longrightarrow> R) \<Longrightarrow> R"
  by (auto elim: cmd_steps.cases)

lemma Assign_elim[elim!]:
"cmd_steps \<sigma> (Run (Assign x e) L s h) (Stop L' s' h') \<Longrightarrow>
  (\<And> v. \<lbrakk>\<sigma> = [Tick]; v = expr_sem e s; s' = s(x:=v); h' = h; L = L'\<rbrakk> \<Longrightarrow> R) \<Longrightarrow> R"
  by (auto elim: cmd_steps.cases)

lemma Load_elim[elim!]:
"cmd_steps \<sigma> (Run (Load x p) L s h) (Stop L' s' h') \<Longrightarrow>
  (\<And> a v. \<lbrakk>\<sigma> = [Tick]; a = expr_sem p s; h a = Some v; s' = s(x:=v); h' = h; L = L'\<rbrakk> \<Longrightarrow> R) \<Longrightarrow> R"
  by (auto elim: cmd_steps.cases)

lemma Store_elim[elim!]:
"cmd_steps \<sigma> (Run (Store p e) L s h) (Stop L' s' h') \<Longrightarrow>
  (\<And> a v. \<lbrakk>\<sigma> = [Tick]; a = expr_sem p s; v = expr_sem e s; s' = s; h' = h(a \<mapsto> v); L = L'\<rbrakk> \<Longrightarrow> R) \<Longrightarrow> R"
  by (auto elim: cmd_steps.cases)

lemma Seq_dest:
"cmd_steps \<sigma> k k'  \<Longrightarrow> k = (Run (Seq c1 c2) L s h)  \<Longrightarrow> k' = (Stop L'' s'' h'') \<Longrightarrow>
  (\<exists> \<sigma>1 \<sigma>2 L' s' h'. \<sigma> = \<sigma>1@\<sigma>2 \<and> \<sigma>1 \<noteq> [] \<and> cmd_steps \<sigma>1 (Run c1 L s h) (Stop L' s' h') \<and> cmd_steps \<sigma>2 (Run c2 L' s' h') (Stop L'' s'' h''))"
proof (induction \<sigma> k k' arbitrary: c1 c2 L s h rule: cmd_steps.induct)
  case (cmd_steps_refl k)
  then show ?case by blast
next
  case (cmd_steps_step \<sigma> k k' \<sigma>' k'')
  hence step: "cmd_step \<sigma> (Run (Seq c1 c2) L s h) k'" by simp
  show ?case
  proof(cases rule: cmd_step_seq[OF step])
    case 1
    then show ?thesis
      using cmd_steps_Abort cmd_steps_step.hyps(2) cmd_steps_step.prems(2) by blast 
  next
    case (2 L' s' h')
    then show ?thesis
      by (metis cmd_step1_intro cmd_step_sched_nonempty cmd_steps_step.hyps(2) cmd_steps_step.prems(2))
  next
    case (3 c1' L' s' h')
    then show ?thesis
      by (smt Nil_is_append_conv append.assoc cmd_steps.cmd_steps_step cmd_steps_step.IH cmd_steps_step.prems(2))
  qed
qed

lemma Seq_elim[elim!]:
"cmd_steps \<sigma> (Run (Seq c1 c2) L s h) (Stop L'' s'' h'') \<Longrightarrow>
  (\<And> \<sigma>1 \<sigma>2 L' s' h'. \<lbrakk>\<sigma> = \<sigma>1@\<sigma>2; \<sigma>1 \<noteq> []; cmd_steps \<sigma>1 (Run c1 L s h) (Stop L' s' h'); cmd_steps \<sigma>2 (Run c2 L' s' h') (Stop L'' s'' h'')\<rbrakk> \<Longrightarrow> R) \<Longrightarrow> R"
  by (drule Seq_dest, auto)

lemma If_true[dest]:
"cmd_step [Tick] (Run (If b c1 c2) L s h) k' \<Longrightarrow>   pure_sem b s \<Longrightarrow> k' = (Run c1 L s h)"
  by (auto elim!: cmd_step.cases)

lemma If_false[dest]:
"cmd_step [Tick] (Run (If b c1 c2) L s h) k' \<Longrightarrow> \<not> pure_sem b s \<Longrightarrow> k' = (Run c2 L s h)"
  by (auto elim!: cmd_step.cases)

lemma If_elim[elim!]:
"cmd_steps \<sigma> (Run (If b c1 c2) L s h) (Stop L' s' h') \<Longrightarrow>
  \<lbrakk>\<And> \<sigma>'. \<sigma> = Tick#\<sigma>' \<Longrightarrow>   pure_sem b s \<Longrightarrow> cmd_steps \<sigma>' (Run c1 L s h) (Stop L' s' h') \<Longrightarrow> R;
   \<And> \<sigma>'. \<sigma> = Tick#\<sigma>' \<Longrightarrow> \<not> pure_sem b s \<Longrightarrow> cmd_steps \<sigma>' (Run c2 L s h) (Stop L' s' h') \<Longrightarrow> R\<rbrakk>
 \<Longrightarrow> R"
  by (cases "pure_sem b s", auto elim: cmd_steps.cases)

lemma If_dest:
"cmd_steps \<sigma> (Run (If b c1 c2) L s h) (Stop L' s' h') \<Longrightarrow>
    (\<exists> \<sigma>'. \<sigma> = Tick#\<sigma>' \<and>   pure_sem b s \<and> cmd_steps \<sigma>' (Run c1 L s h) (Stop L' s' h'))
  \<or> (\<exists> \<sigma>'. \<sigma> = Tick#\<sigma>' \<and> \<not> pure_sem b s \<and> cmd_steps \<sigma>' (Run c2 L s h) (Stop L' s' h'))"
  by auto

lemma While_exit:
"cmd_step \<sigma> (Run (While b c) L s h) k' \<Longrightarrow> \<not> pure_sem b s \<Longrightarrow> k' = (Stop L s h)"
  by (auto elim!: cmd_step.cases)

lemma While_unwind:
"cmd_step \<sigma> (Run (While b c) L s h) k' \<Longrightarrow>   pure_sem b s \<Longrightarrow> k' = (Run (Seq c (While b c)) L s h)"
  by (auto elim!: cmd_step.cases)

lemma app_one:
"(xs @ ys = [x]) = (xs = [x] \<and> ys = [] \<or> xs = [] \<and> ys = [x])"
  by (metis Nil_is_append_conv append.left_neutral butlast_append butlast_snoc)

lemma While_elim:
  assumes steps:  "cmd_steps \<sigma> (Run (While b c) L s h) (Stop L' s' h')"
  assumes unwind: "\<And> \<sigma>'. \<sigma> = Tick#\<sigma>' \<Longrightarrow>  pure_sem b s \<Longrightarrow> cmd_steps \<sigma>' (Run (Seq c (While b c)) L s h) (Stop L' s' h') \<Longrightarrow> R"
  assumes exit:   "\<sigma> = [Tick] \<Longrightarrow>  \<not> pure_sem b s \<Longrightarrow> s' = s \<Longrightarrow> h' = h \<Longrightarrow> R"
  shows   "R"
proof (cases "pure_sem b s")
  case False
  with steps exit show ?thesis 
    by (metis Stop_dest append_self_conv cmd_step_while cmd_steps_Run' conf.inject(1))
next
  case True
  note cond = `pure_sem b s`
  with steps unwind cond show ?thesis 
    by (smt append_Cons append_self_conv2 cmd_step_while cmd_steps_Run')
qed

lemma While_dest:
"cmd_steps \<sigma> (Run (While b c) L s h) (Stop L' s' h')
  \<Longrightarrow>  (\<exists> \<sigma>'. \<sigma> = Tick#\<sigma>' \<and> pure_sem b s \<and> cmd_steps \<sigma>' (Run (Seq c (While b c)) L s h) (Stop L' s' h'))
     \<or> \<sigma> = [Tick] \<and> \<not> pure_sem b s \<and> s' = s \<and> h' = h"
  by (auto elim: While_elim)

lemma
expr_coindidence [simp]: "x \<notin> expr_free  e  \<Longrightarrow> expr_sem  e  (s(x:=v)) = expr_sem  e  s" and
exprs_coindidence[simp]: "x \<notin> expr_frees es \<Longrightarrow> expr_sems es (s(x:=v)) = expr_sems es s"
  by (induction e s and es s rule: expr_sem_expr_sems.induct) auto

lemma
expr_ren[simp]: "y \<notin> expr_free e \<Longrightarrow> expr_sem (expr_ren e x y) (s(y := s x)) = expr_sem e s" and
exprs_ren[simp]: "y \<notin> expr_frees es \<Longrightarrow> expr_sems (expr_rens es x y) (s(y := s x)) = expr_sems es s"
  by (induction e s and es s rule: expr_sem_expr_sems.induct) auto
corollary
pure_ren[simp]: "y \<notin> expr_free e \<Longrightarrow> pure_sem (expr_ren e x y) (s(y := s x)) = pure_sem e s"
  unfolding pure_sem_def by simp



lemma
pure_eq [simp]: "x \<notin> expr_free p  \<Longrightarrow> pure_sem  p  (s(x:=v)) = pure_sem  p  s"
  unfolding pure_sem_def by simp

definition coincide :: "('b \<Rightarrow> 'c) \<Rightarrow> ('b \<Rightarrow> 'c) \<Rightarrow> 'b set \<Rightarrow> bool" where
"coincide s s' xs \<equiv> \<forall> x. x\<in>xs \<longrightarrow> s x = s' x"
lemma
coincide_emp  [simp]:  "coincide s s' {}" and
coincide_refl [simp]:  "coincide s s xs" and
coincide_sym  [intro]: "coincide s1 s2 xs \<Longrightarrow> coincide s2 s1 xs" and
coincide_trans[intro]: "coincide s s' xs \<Longrightarrow> coincide s' s'' xs \<Longrightarrow> coincide s s'' xs" and
coincide_mono [intro]: "\<lbrakk>coincide s s' xs; ys \<subseteq> xs\<rbrakk> \<Longrightarrow> coincide s s' ys" and
coincide_singleton[simp]: "coincide s s' {x} = (s x = s' x)" and
coincide_insert[simp]: "coincide s s' (insert x xs) = (s x = s' x \<and> coincide s s' xs)" and
coincide_union[simp]:  "coincide s s' (xs \<union> ys) = (coincide s s' xs \<and> coincide s s' ys)" and
coincide_inter1[intro]:  "coincide s s' xs \<Longrightarrow> coincide s s' (xs \<inter> ys)" and
coincide_inter2[intro]:  "coincide s s' ys \<Longrightarrow> coincide s s' (xs \<inter> ys)" and
coincide_update[intro]: "coincide s s' xs \<Longrightarrow> coincide (s(x:=v)) (s'(x:=v)) xs"
  unfolding coincide_def by auto

lemma coincide_intro:
"(\<And> x. x\<in>xs \<Longrightarrow> s x = s' x) \<Longrightarrow> coincide s s' xs"
  unfolding coincide_def by auto

lemma coincide_reduce:
"\<lbrakk>coincide s s' xs;
  \<And> x. (x \<in> xs \<Longrightarrow> s x = s' x) \<Longrightarrow> (x \<in> ys \<Longrightarrow> t x = t' x)\<rbrakk>
  \<Longrightarrow> coincide t t' ys"
  unfolding coincide_def by blast

fun conf_mod :: "'a conf \<Rightarrow> var set" where
"conf_mod Abort = {}" |
"conf_mod (Stop L s h) = {}" |
"conf_mod (Run c L s h) = cmd_mod c"

fun conf_vars :: "'a conf \<Rightarrow> var set" where
"conf_vars Abort = {}" |
"conf_vars (Stop L s h) = {}" |
"conf_vars (Run c L s h) = cmd_vars c"

fun conf_upd :: "'a conf \<Rightarrow> var \<Rightarrow> 'a \<Rightarrow> 'a conf" where
"conf_upd Abort x v = Abort" |
"conf_upd (Stop L s h) x v = (Stop L (s(x:=v)) h)" |
"conf_upd (Run c L s h) x v = (Run c L (s(x:=v)) h)"

lemma conf_upd_Stop[elim!]:
  assumes "conf_upd k z v = Stop (set \<Gamma>) s' H"
  obtains s where "k = Stop (set \<Gamma>) s H" "s' = (s(z:=v))"
  using assms by (cases k) auto

fun conf_coincide :: "'a conf \<Rightarrow> 'a conf \<Rightarrow> var set \<Rightarrow> bool" where
"conf_coincide Abort k xs = (k = Abort)" |
"conf_coincide (Stop L s h) k xs = (\<exists> s'. k = (Stop L s' h) \<and> coincide s s' xs)" |
"conf_coincide (Run c L s h) k xs = (\<exists> s'. k = (Run c L s' h) \<and> coincide s s' xs)"

lemma
[simp]: "conf_coincide k Abort xs = (k = Abort)" and
[simp]: "conf_coincide k (Stop L s h) xs = (\<exists> s'. k = (Stop L s' h) \<and> coincide s' s xs)" and
[simp]: "conf_coincide k (Run c L s h) xs = (\<exists> s'. k = (Run c L s' h) \<and> coincide s' s xs)"
  by (case_tac k, auto)+

fun conf_merge :: "'a state \<Rightarrow> 'a conf \<Rightarrow> var set \<Rightarrow> 'a conf" where
"conf_merge s' Abort V = Abort" |
"conf_merge s' (Stop L s'' h) V = (Stop L (\<lambda>v. if v \<in> V then s'' v else s' v) h)" |
"conf_merge s' (Run c L s'' h) V = (Run c L (\<lambda>v. if v \<in> V then s'' v else s' v) h)"

lemma conf_merge_conf_coincide: 
  "conf_coincide k (conf_merge s' k xs) xs"
  by(cases k, auto simp: coincide_def)

lemma cmd_step_preserves_mod:
"cmd_step \<sigma> k k' \<Longrightarrow> conf_mod k' \<subseteq> conf_mod k"
  by (induction \<sigma> k k' rule: cmd_step.induct) auto

lemma cmd_step_preserves_vars:
"cmd_step \<sigma> k k' \<Longrightarrow> conf_vars k' \<subseteq> conf_vars k"
  by (induction \<sigma> k k' rule: cmd_step.induct) auto

lemma cmd_steps_preserves_mod:
"cmd_steps \<sigma> k k' \<Longrightarrow> conf_mod k' \<subseteq> conf_mod k"
  by (induction \<sigma> k k' rule: cmd_steps.induct)
     (auto dest: cmd_step_preserves_mod)

lemma cmd_step_mod:
  assumes "cmd_step \<sigma> (Run c L s h) k'"
  shows   "k' = (Run c' L' s' h') \<Longrightarrow> coincide s s' (- cmd_mod c)"
          "k' = (Stop L' s' h')   \<Longrightarrow> coincide s s' (- cmd_mod c)"
  using assms apply (induction \<sigma> " (Run c L s h)" k' arbitrary: c c' rule: cmd_step.induct)
  unfolding coincide_def by auto

lemma cmd_steps_mod:
  assumes "cmd_steps \<sigma> (Run c L s h) k'"
  shows   "k' = (Run c' L' s' h') \<Longrightarrow> coincide s s' (- cmd_mod c)"
          "k' = (Stop L' s' h')   \<Longrightarrow> coincide s s' (- cmd_mod c)"
  using assms apply (induction \<sigma> "(Run c L s h)" k' arbitrary: c L s h rule: cmd_steps.induct)
     apply simp_all
proof -
  fix \<sigma> k' \<sigma>' c L s h
  assume step: "cmd_step \<sigma> (Run c L s h) k'"
  assume steps: "cmd_steps \<sigma>' k' (Run c' L' s' h') "
  assume hyp: "(\<And>c L s h. k' = Run c L s h \<Longrightarrow> coincide s s' (- cmd_mod c))"
  show "coincide s s' (- cmd_mod c)"
  proof(cases k')
    case Abort
    then show ?thesis 
      using cmd_steps_Abort steps by blast
  next
    case (Stop x21 x22 x23)
    then show ?thesis 
      using cmd_steps_Stop steps by blast
  next
    case (Run x31 x32 x33 x34)
    then show ?thesis 
      using steps hyp step 
      by (metis Compl_anti_mono cmd_step_mod(1) coincide_mono coincide_trans conf_mod.simps(3) semantics.cmd_step_preserves_mod semantics_axioms)
  qed
next
  fix \<sigma> k' \<sigma>' c L s h
  assume step: "cmd_step \<sigma> (Run c L s h) k'"
  assume steps: "cmd_steps \<sigma>' k' (Stop L' s' h') "
  assume hyp: "(\<And>c L s h. k' = Run c L s h \<Longrightarrow> coincide s s' (- cmd_mod c))"
  show "coincide s s' (- cmd_mod c)"
  proof(cases k')
    case Abort
    then show ?thesis 
      using cmd_steps_Abort steps by blast
  next
    case (Stop x21 x22 x23)
    then show ?thesis 
      using cmd_steps_Stop steps 
      using cmd_step_mod(2) local.step by auto
  next
    case (Run x31 x32 x33 x34)
    then show ?thesis 
      using steps hyp step 
      by (metis Compl_anti_mono cmd_step_mod(1) coincide_mono coincide_trans conf_mod.simps(3) semantics.cmd_step_preserves_mod semantics_axioms)
  qed
qed

lemma
expr_coindidence_strong [simp]: "coincide s s' (expr_free  e)  \<Longrightarrow> expr_sem  e  s' = expr_sem  e  s" and
exprs_coindidence_strong[simp]: "coincide s s' (expr_frees es) \<Longrightarrow> expr_sems es s' = expr_sems es s" 
  by (induction e s and es s rule: expr_sem_expr_sems.induct) auto

lemma
expr_coindidence_strong' [simp]: "coincide s s' vs \<Longrightarrow> (expr_free  e) \<subseteq> vs  \<Longrightarrow> expr_sem  e  s' = expr_sem  e  s" and
exprs_coindidence_strong'[simp]: "coincide s s' vs \<Longrightarrow> (expr_frees es) \<subseteq> vs \<Longrightarrow> expr_sems es s' = expr_sems es s" 
  using expr_coindidence_strong exprs_coindidence_strong coincide_mono by blast+


lemma 
pure_coindidence_strong [simp]: "coincide s s' (expr_free  e)  \<Longrightarrow> pure_sem  e  s' = pure_sem  e  s"
  unfolding pure_sem_def by simp

lemma 
pure_coindidence_strong' [simp]: "coincide s s' vs \<Longrightarrow> (expr_free  e) \<subseteq> vs  \<Longrightarrow> pure_sem  e  s' = pure_sem  e  s"
  unfolding pure_sem_def by simp


lemma coincide_eq:
  "coincide s s' vs \<Longrightarrow> v \<in> vs \<Longrightarrow> s v = s' v"
  by(auto simp: coincide_def)

lemma cmd_step_coincide_strong':
"\<lbrakk>cmd_step \<sigma> (Run c L s h) k; coincide s s' V\<rbrakk> \<Longrightarrow>
   cmd_vars c \<subseteq> V \<Longrightarrow> cmd_step \<sigma> (Run c L s' h) (conf_merge s' k V)"
  apply(induction \<sigma> "Run c L s h" k arbitrary: c s' V rule: cmd_step.induct)
                      apply (auto intro!: cmd_step.intros)
             apply(subst cmd_step.simps,simp,rule ext, simp, metis coincide_eq)+
  done

lemma cmd_step_coincide_strong:
"\<lbrakk>cmd_step \<sigma> (Run c L s h) k; coincide s s' (cmd_vars c)\<rbrakk>
  \<Longrightarrow> \<exists> k'. cmd_step \<sigma> (Run c L s' h) k' \<and> conf_coincide k k' (cmd_vars c)"
  using cmd_step_coincide_strong' conf_merge_conf_coincide 
  by blast


fun seq :: "'a conf \<Rightarrow> cmd \<Rightarrow> 'a conf" where
"seq  Abort         c2 = Abort" |
"seq (Stop   L s h) c2 = Run c2 L s h" |
"seq (Run c1 L s h) c2 = Run (Seq c1 c2) L s h"

lemma step_seq:
"cmd_step \<sigma> (Run (Seq c1 c2) L s H) k
  \<Longrightarrow> \<exists> k1. cmd_step \<sigma> (Run c1 L s H) k1 \<and> k = (seq k1 c2)"
  by auto

fun par :: "tick \<Rightarrow> cmd \<Rightarrow> cmd \<Rightarrow> 'a conf \<Rightarrow> 'a conf" where
"par \<tau> c1 c2 Abort = Abort" |
"par Fst c1 c2 (Stop L s h) = Run c2 L s h" |
"par Snd c1 c2 (Stop L s h) = Run c1 L s h" |
"par Fst c1 c2 (Run c1' L s h) = Run (Par c1' c2) L s h" |
"par Snd c1 c2 (Run c2' L s h) = Run (Par c1 c2') L s h"

lemma step_par:
  assumes "cmd_step \<sigma> (Run (Par c1 c2) L s H) k"
  obtains \<tau> \<sigma>' k1 where "\<sigma> = (\<tau>#\<sigma>')" "\<tau> = Fst \<or> \<tau> = Snd" "cmd_step \<sigma>' (Run (if \<tau> = Fst then c1 else c2) L s H) k1" "k = (par \<tau> c1 c2 k1)"
proof -
  assume prem:
    "\<And>\<tau> \<sigma>' k1. \<sigma> = \<tau> # \<sigma>' \<Longrightarrow> \<tau> = Fst \<or> \<tau> = Snd \<Longrightarrow> cmd_step \<sigma>' (Run (if \<tau> = Fst then c1 else c2) L s H) k1 \<Longrightarrow> k = par \<tau> c1 c2 k1 \<Longrightarrow> thesis"
  show thesis
    by (cases rule: cmd_step_par[OF assms], auto intro!: prem)
qed

lemma step_par_split:
  assumes "cmd_step \<sigma> (Run (Par c1 c2) L s H) k"
    obtains
      (fst) \<sigma>' k1 where "\<sigma> = Fst # \<sigma>'" "cmd_step \<sigma>' (Run c1 L s H) k1" "k = (par Fst c1 c2 k1)" |
      (snd) \<sigma>' k2 where "\<sigma> = Snd # \<sigma>'" "cmd_step \<sigma>' (Run c2 L s H) k2" "k = (par Snd c1 c2 k2)"
  using assms by (auto elim: step_par)

lemma step_par_fst:
  assumes "cmd_step (Fst#\<sigma>') (Run (Par c1 c2) L s H) k"
  obtains k1 where "cmd_step \<sigma>' (Run c1 L s H) k1" "k = (par Fst c1 c2 k1)"
  using assms by (auto elim: step_par)

lemma step_par_snd:
  assumes "cmd_step (Snd#\<sigma>') (Run (Par c1 c2) L s H) k"
  obtains k2 where "cmd_step \<sigma>' (Run c2 L s H) k2" "k = (par Snd c1 c2 k2)"
  using assms by (auto elim: step_par)

lemma step_ex:
  assumes "cmd_step \<sigma> (Run c L s h) k"
  assumes "x \<notin> cmd_vars c"
  shows   "cmd_step \<sigma> (Run c L (s(x:=v)) h) (conf_upd k x v)"
proof -
  have
    "coincide s (s(x:=v)) (-{x})"
    unfolding coincide_def by auto
  from cmd_step_coincide_strong'[OF assms(1) this] assms(2) have
    "cmd_step \<sigma> (Run c L (s(x := v)) h) (conf_merge (s(x := v)) k (-{x}))" by simp
  then show ?thesis by (cases k, auto)
qed

lemma steps_both:
  assumes
    "cmd_steps \<sigma> (Run c1 L1 s1 h1) k2" and
    "cmd_steps \<sigma> (Run c1 L1 s1' h1') k2'"
  obtains
    (refl)
      "\<sigma> = []" "k2 = (Run c1 L1 s1 h1)" "k2' = (Run c1 L1 s1' h1')" |
    (step) \<sigma>1 \<sigma>2 k k' where
      "\<sigma> = \<sigma>1@\<sigma>2" "\<sigma>1 \<noteq> []"
    "cmd_step  \<sigma>1 (Run c1 L1 s1 h1) k"
    "cmd_step  \<sigma>1 (Run c1 L1 s1' h1') k'"
    "cmd_steps \<sigma>2 k  k2"
    "cmd_steps \<sigma>2 k' k2'"
  using assms(1)
proof (cases rule: cmd_steps.cases)
  case cmd_steps_refl
  with assms(2) that(1) show ?thesis by simp
next
  case (cmd_steps_step \<sigma>1 k \<sigma>2)
  note 1 = this

  from assms(2)
  show ?thesis proof (cases rule: cmd_steps.cases)
    case cmd_steps_refl
    with 1 show ?thesis by simp
  next
    case (cmd_steps_step \<sigma>1' k' \<sigma>2')
    note 2 = this
    have
      \<sigma>1: "\<sigma>1 = \<sigma>1'"
      by (rule cmd_sched_determ, rule 1(2), rule 2(2), rule 1(1), rule 2(1))
    hence
      \<sigma>2: "\<sigma>2' = \<sigma>2" using 1(1) 2(1) by simp
    thm that(2)
    note b = 1(3) 2(3)
    show ?thesis 
      apply (rule that(2))
      using \<sigma>1 \<sigma>2 1 2 by auto
  qed
qed

end
end
