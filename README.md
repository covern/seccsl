# SecCSL Isabelle/HOL Formalisation and Soundness Proof

The complete and self-contained Isabelle/HOL formalization of SecCSL.

## Building

Requires Isabelle 2018: https://isabelle.in.tum.de/website-Isabelle2018/

```
isabelle build -c -d . -v SecCSL
```

or simply type `make`.

## Theory Organisation

* `SecCSL.thy`: top-level lemmas and theorems
* `Syntax.thy`: syntax of expressions, assertions, commands
* `Semantics.thy`: semantics of expressions, commands
* `Separation.thy`: separation logic connectives
* `Logic.thy`: definition of secure entailment and proof rules
* `Locks.thy`: helper lemmas on the lock invariants
* `Shared.thy`: helper lemmas on shared region invariant
* `Soundness.thy`: security definition and details of the soundness proof

