theory Separation
  imports Semantics HOL.Hilbert_Choice
begin

type_synonym 'a val  = "'a state \<Rightarrow> 'a"
type_synonym 'a pure = "'a state \<Rightarrow> bool"
type_synonym 'a rel  = "'a state \<Rightarrow> 'a state \<Rightarrow> bool"
type_synonym 'a pred = "'a state * 'a heap \<Rightarrow> 'a state * 'a heap \<Rightarrow> bool"

lemma map_add_empty_elim[elim!]:
"(Map.empty = m1 ++ m2) \<Longrightarrow> (\<lbrakk>m1 = Map.empty; m2 = Map.empty\<rbrakk> \<Longrightarrow> R) \<Longrightarrow> R"
  by (metis map_add_None)

inductive perm :: "'a list \<Rightarrow> 'a list \<Rightarrow> bool" where
perm_base[simp]: "\<lbrakk>bs = []\<rbrakk> \<Longrightarrow> perm [] bs" |
perm_rec:        "\<lbrakk>bs = bs1 @ a # bs2; perm as (bs1 @ bs2)\<rbrakk> \<Longrightarrow> perm (a#as) bs"

inductive_cases perm_Nil[elim!]:  "perm [] bs"
inductive_cases perm_Cons[elim!]: "perm (a#as) bs"

lemma perm_map_distinct:
"\<lbrakk>set xs = set ys; distinct xs; distinct ys\<rbrakk>
   \<Longrightarrow> perm (map f xs) (map f ys)"
proof (induction xs arbitrary: ys)
  case Nil
  then show ?case by simp
next
  case (Cons a xs)
  from `set (a # xs) = set ys` have
    "a \<in> set ys" by auto
  then obtain ys1 ys2 where
    split: "ys = ys1 @ a # ys2" by (meson split_list)
  with `distinct ys` have
    a: "a \<notin> set ys1" "a \<notin> set ys2" by auto

  from `distinct (a#xs)` have
    "distinct xs" by simp
  moreover from `distinct ys` split have
    "distinct (ys1@ys2)" by simp
  moreover from `distinct (a#xs)` `set (a#xs) = set ys` split a have
    "set xs = set (ys1 @ ys2)" by (simp add: insert_eq_iff)
  ultimately have
    hyp: "perm (map f xs) (map f (ys1@ys2))" using Cons.IH by blast
  with split show ?case by (auto intro: perm_rec)
qed

corollary perm_distinct:
"\<lbrakk>set xs = set ys; distinct xs; distinct ys\<rbrakk>
   \<Longrightarrow> perm xs ys"
  using perm_map_distinct[where ?f="id"] by simp

lemma perm_map_remdups:
"\<lbrakk>set xs = set ys\<rbrakk>
   \<Longrightarrow> perm (map f (remdups xs)) (map f (remdups ys))"
  by (auto intro: perm_map_distinct)

context semantics
begin

definition sep :: "'a heap \<Rightarrow> 'a heap \<Rightarrow> 'a heap \<Rightarrow> bool" where
"sep h h1 h2 \<equiv> dom h1 \<inter> dom h2 = {} \<and> h = h1 ++ h2"

lemma sep_com: "sep h h1 h2 = sep h h2 h1"
  unfolding sep_def using map_add_comm by blast
lemma sep_com_intro[intro]: "sep h h1 h2 \<Longrightarrow> sep h h2 h1"
  using sep_com by blast

lemma
[simp]: "sep Map.empty h1 h2 = (Map.empty = h1 \<and> Map.empty = h2)" and
[simp]: "sep h h1 Map.empty = (h = h1)" and
[simp]: "sep h Map.empty h2 = (h = h2)"
  unfolding sep_def by auto

lemma sep_ass[intro]:
"\<lbrakk>sep h h1 h2; sep h1 ha hb\<rbrakk> \<Longrightarrow> sep h ha (hb ++ h2)"
  unfolding sep_def by auto

lemma pred_eq:
  assumes "\<And> s h s' h'. P (s,h) (s',h') = Q (s,h) (s',h')"
  shows   "(P :: 'a pred) = Q"
proof -
  have "\<And> sh sh'. P sh sh' = Q sh sh'" using assms by auto
  then have "\<And> sh. P sh = Q sh" ..
  then show ?thesis ..
qed

(* basic SL connectives *)

fun pure :: "'a pure \<Rightarrow> 'a rel" where
"pure b s s' = (b s \<and> b s')"

fun rel :: "'a rel \<Rightarrow> 'a pred" where
"rel b (s,h) (s',h') = (b s s' \<and> h = Map.empty \<and> h' = Map.empty)"

fun rimp :: "'a rel \<Rightarrow> 'a rel \<Rightarrow> 'a rel" where
"rimp p q s s' = (p s s' \<longrightarrow> q s s')"

definition cond :: "'a pure \<Rightarrow> 'a pred \<Rightarrow> 'a pred \<Rightarrow> 'a pred" where
"cond b p q sh sh' = (b (fst sh) = b (fst sh') \<and> (if b (fst sh) then p sh sh' else q sh sh'))"

inductive ex :: "var \<Rightarrow> 'a pred \<Rightarrow> 'a pred" where
ex_intro:
"p (s(x:=v),h) (s'(x:=v'),h') \<Longrightarrow> ex x p (s,h) (s',h')"
inductive_cases
ex_elim: "ex b p (s,h) (s',h')"

lemma
cond_true [simp]: "\<lbrakk>b s; b s'\<rbrakk> \<Longrightarrow> cond b p q (s,h) (s',h') = p (s,h) (s',h')" and
cond_false[simp]: "\<lbrakk>\<not> b s; \<not> b s'\<rbrakk> \<Longrightarrow> cond b p q (s,h) (s',h') = q (s,h) (s',h')"
  unfolding cond_def by simp_all

inductive
sec :: "'level \<Rightarrow> 'a val \<Rightarrow> 'a val \<Rightarrow> 'a rel" where
sec_intro:
  "\<lbrakk>l s = l s'; to_level (l s) \<le> lev \<Longrightarrow> e s = e s'\<rbrakk> \<Longrightarrow> sec lev e l s s'"
inductive_cases
sec_elim: "sec lev e l s s'"

lemma sec_expr_low[simp]:
"sec lev (expr_sem e) (expr_sem Low) s s'
  = (expr_sem e s = expr_sem e s')"
  unfolding sec.simps by auto

fun emp :: "'a pred" where
  "emp (s,h) (s',h') = (h = Map.empty \<and> h' = Map.empty)"

fun const :: "'a heap \<Rightarrow> 'a heap \<Rightarrow> 'a pred" where
  "const H H' (s,h) (s',h') = (h = H \<and> h' = H')"

lemma
true_emp[simp]: "rel (pure (\<lambda> s. True)) = emp" and
const_emp[simp]: "const Map.empty Map.empty = emp"
  by (auto intro!: pred_eq)

inductive
pto :: "'a val \<Rightarrow> 'a val \<Rightarrow> 'a pred" where
pto_intro:
  "pto p v (s,[p s \<mapsto> v s]) (s',[p s' \<mapsto> v s'])"
inductive_cases
pto_elim: "pto p v (s,h) (s',h')"

(* separating conjunction *)

inductive
star :: "'a pred \<Rightarrow> 'a pred \<Rightarrow> 'a pred" where
star_intro:
  "\<lbrakk>sep h h1 h2; sep h' h1' h2'; P (s,h1) (s',h1'); Q (s,h2) (s',h2')\<rbrakk>
    \<Longrightarrow> star P Q (s,h) (s',h')"
inductive_cases
star_elim: "star P Q (s,h) (s',h')" (* don't automatically do this *)

lemma
star_emp_left [simp]: "(star emp P) = P" and
star_emp_right[simp]: "(star P emp) = P"
  by (auto intro!: pred_eq star_intro elim!: star_elim)

lemma
star_rel_left[simp]:  "star (rel t) P (s,h) (s',h') = ((rel t) (s,Map.empty) (s',Map.empty) \<and> P (s,h) (s',h'))" and
star_cond_true[simp]:  "\<lbrakk>b s; b s'\<rbrakk> \<Longrightarrow> star (cond b p q) r (s,h) (s',h') = star p r (s,h) (s',h')" and
star_cond_false[simp]: "\<lbrakk>\<not> b s; \<not> b s'\<rbrakk> \<Longrightarrow> star (cond b p q) r (s,h) (s',h') = star q r (s,h) (s',h')"
  by (auto intro!: star_intro sec_intro elim!: star_elim sec_elim)

lemma star_com_intro: "star P Q (s,h) (s',h') \<Longrightarrow> star Q P (s,h) (s',h')"
proof (erule star_elim)
  fix h1 h2 h1' h2'

  assume "sep h h1 h2" "sep h' h1' h2'"
  then have sep: "sep h h2 h1" "sep h' h2' h1'"
    by auto

  assume "P (s, h1) (s', h1')" "Q (s, h2) (s', h2')"
  with sep show "star Q P (s,h) (s',h')"
    by (auto intro: star_intro)
qed

corollary star_com: "star P Q = star Q P"
  by (auto intro: pred_eq star_com_intro)

lemma star_ass_left: "star (star P Q) R (s,h) (s',h') \<Longrightarrow> star P (star Q R) (s,h) (s',h')"
proof (auto elim!: star_elim)
  fix hp hp' hq hq' hr hr' hpq hpq'

  assume "sep h hpq hr"  "sep h' hpq' hr'"
         "sep hpq hp hq" "sep hpq' hp' hq'"
  then have sep:
         "sep (hq ++ hr) hq hr" "sep (hq' ++ hr') hq' hr'"
         "sep h hp (hq ++ hr)" "sep h' hp' (hq' ++ hr')"
    unfolding sep_def by auto

  assume "P (s, hp) (s', hp')" "Q (s, hq) (s', hq')" "R (s, hr) (s', hr')"
  with sep show "star P (star Q R) (s, h) (s', h')"
    by (auto intro: star_intro)
qed

lemma star_ass_right: "star P (star Q R) (s,h) (s',h') \<Longrightarrow> star (star P Q) R (s,h) (s',h')"
proof (auto elim!: star_elim)
  fix hp hp' hq hq' hr hr' hqr hqr'

  assume "sep h hp hqr"  "sep h' hp' hqr'"
         "sep hqr hq hr" "sep hqr' hq' hr'"
  then have sep:
         "sep (hp ++ hq) hp hq" "sep (hp' ++ hq') hp' hq'"
         "sep h (hp ++ hq) hr" "sep h' (hp' ++ hq') hr'"
    unfolding sep_def by auto

  assume "P (s, hp) (s', hp')" "Q (s, hq) (s', hq')" "R (s, hr) (s', hr')"
  with sep show "star (star P Q) R (s,h) (s',h')"
    by (auto intro: star_intro)
qed

corollary star_ass[simp]: "star (star P Q) R = star P (star Q R)"
  by (auto intro: pred_eq star_ass_left star_ass_right)

lemma star_com12_: "star P (star Q A) = star Q (star P A)" using star_ass[of P Q A] star_com[of P Q] by simp
lemma star_com23:  "star A (star P Q) = star A (star Q P)" using star_com[of P Q] by simp
lemma star_com23_: "star A (star P (star Q B)) = star A (star Q (star P B))" using star_com12_[of P Q B] by simp

lemma star_ac: "(star P (star Q R)) = (star Q (star P R))"
  using star_com star_ass by simp

fun stars :: "('a pred) list \<Rightarrow> 'a pred" where
stars_Nil:  "stars [] = emp" |
stars_Cons: "stars (p#ps) = star p (stars ps)"

lemma stars_app[simp]:
"stars (ps@qs) = star (stars ps) (stars qs)"
  by (induction ps, auto)

lemma stars_reorder_perm:
"perm ps qs
  \<Longrightarrow> stars ps = stars qs"
proof (induction rule: perm.induct)
  case perm_base
  then show ?case by simp
next
  case (perm_rec bs bs1 a bs2 as)
  note stuff = this
  have "star a (star (stars bs1) (stars bs2))
                 = star (star a (stars bs1)) (stars bs2)" by simp
  also have "... = star (star (stars bs1) a) (stars bs2)" by (auto simp: star_com)
  also have "... = star (stars bs1) (star a (stars bs2))" by simp
  finally show ?case using stuff by simp
qed

corollary stars_reorder_distinct:
"\<lbrakk>set ps = set qs; distinct ps; distinct qs\<rbrakk>
  \<Longrightarrow> stars ps = stars qs"
  by (auto intro: stars_reorder_perm perm_distinct)

(* lifting to syntax *)
                                   
primrec rexpr_sem :: "'level \<Rightarrow> rexpr \<Rightarrow> 'a rel" where
"rexpr_sem lev (Pure e) = pure (pure_sem e)" |
"rexpr_sem lev (Sec e l) = sec lev (expr_sem e) (expr_sem l)" |
"rexpr_sem lev (RImp e f) = rimp (rexpr_sem lev e) (rexpr_sem lev f)"

text {*
 boolean operators on relational formula. Note these are distinct
 from a pure boolean formula (@{term "Pure e"}) interpreted relationally
*}
definition
  PTrue :: expr where "PTrue \<equiv> (Eq (App ''low'' []) (App ''low'' []))"

lemma PTrue_sem[simp]:
  "pure_sem PTrue s" by (simp add: PTrue_def)

definition
  PFalse :: expr where "PFalse \<equiv> Not PTrue"

lemma PFalse_sem[simp]:
  "\<not> pure_sem PFalse s" by (simp add: PFalse_def)

definition RNot :: "rexpr \<Rightarrow> rexpr" where
  "RNot e \<equiv> (RImp e (Pure PFalse))"

lemma RNot_sem[simp]:
  "rexpr_sem lev (RNot e) s s' = (\<not> rexpr_sem lev e s s')"
  by (simp add: RNot_def)

definition ROr :: "rexpr \<Rightarrow> rexpr \<Rightarrow> rexpr" where
  "ROr e f \<equiv> (RImp (RNot e) f)"

lemma ROr_sem[simp]:
  "rexpr_sem lev (ROr e f) s s' = (rexpr_sem lev e s s' \<or> rexpr_sem lev f s s')"
  by (auto simp: ROr_def)

definition RAnd :: "rexpr \<Rightarrow> rexpr \<Rightarrow> rexpr" where
  "RAnd e f \<equiv> (RNot (ROr (RNot e) (RNot f)))"

lemma RAnd_sem[simp]:
  "rexpr_sem lev (RAnd e f) s s' = (rexpr_sem lev e s s' \<and> rexpr_sem lev f s s')"
  by (auto simp: RAnd_def)

lemma RImp_sem:
  "rexpr_sem lev (RImp e f) s s' = (rexpr_sem lev e s s' \<longrightarrow> rexpr_sem lev f s s')"
  by simp

definition RIff :: "rexpr \<Rightarrow> rexpr \<Rightarrow> rexpr" where
  "RIff e f \<equiv> (RAnd (RImp e f) (RImp f e))"

lemma RIff_sem[simp]:
  "rexpr_sem lev (RIff e f) s s' = (rexpr_sem lev e s s' = rexpr_sem lev f s s')"
  by(auto simp: RIff_def)

definition RIte :: "rexpr \<Rightarrow> rexpr \<Rightarrow> rexpr \<Rightarrow> rexpr" where
  "RIte e f g \<equiv> (RAnd (RImp e f) (RImp (RNot e) g))"

lemma RIte_sem[simp]:
  "rexpr_sem lev (RIte e f g) s s' = (if rexpr_sem lev e s s' then rexpr_sem lev f s s' else rexpr_sem lev g s s')"
  by(auto simp: RIte_def)

lemma rexpr_free_RNot[simp]:
  "rexpr_free (RNot r) = rexpr_free r"
  by(auto simp: RNot_def PFalse_def PTrue_def)

lemma rexpr_free_ROr[simp]:
  "rexpr_free (ROr r s) = rexpr_free r \<union> rexpr_free s"
  by(auto simp: ROr_def)

lemma rexpr_free_RAnd[simp]:
  "rexpr_free (RAnd r s) = rexpr_free r \<union> rexpr_free s"
  by(auto simp: RAnd_def)

lemma rexpr_free_RIff[simp]:
  "rexpr_free (RIff r s) = rexpr_free r \<union> rexpr_free s"
  by(auto simp: RIff_def)

lemma rexpr_free_RIte[simp]:
  "rexpr_free (RIte r s t) = rexpr_free r \<union> rexpr_free s \<union> rexpr_free t"
  by(auto simp: RIte_def)

primrec assert_sem  :: "'level \<Rightarrow> assert \<Rightarrow> 'a pred" where
"assert_sem lev Emp          = emp" |
"assert_sem lev (Rel p)      = rel (rexpr_sem lev p)" |
"assert_sem lev (Pto p l e)  = stars [rel (sec lev (expr_sem p) (expr_sem l)), rel (sec lev (expr_sem e) (expr_sem l)), pto (expr_sem p) (expr_sem e)]" |
"assert_sem lev (And  p q)   = (\<lambda> (s,h) (s',h'). assert_sem lev p (s,h) (s',h') \<and> assert_sem lev q (s,h) (s',h'))" |
"assert_sem lev (Star p q)   = star (assert_sem lev p) (assert_sem lev q)" |
"assert_sem lev (Cond b p q) = cond (pure_sem b) (assert_sem lev p) (assert_sem lev q)" |
"assert_sem lev (Ex x p)     = ex x (assert_sem lev p)"

lemma propositional_embedding:
  "(\<forall>s. pure_sem e s) = (\<forall>s s'. assert_sem lev (Rel (Pure e)) (s,Map.empty) (s',Map.empty))"
  by simp

lemma Stars_sem[simp]:
"assert_sem lev (Stars ps) = stars (map (assert_sem lev) ps)"
  by (induction ps, auto)

(* heap access *)

lemma
(* the first two loop badly *)
pto_read:  "pto p e (s,h) (s',h') \<Longrightarrow> h (p s) = Some (e s)" and
pto_read': "pto p e (s,h) (s',h') \<Longrightarrow> h' (p s') = Some (e s')" and
pto_frame_read[simp]:  "star (pto p e) r (s,h) (s',h') \<Longrightarrow> h (p s) = Some (e s)" and
pto_frame_read'[simp]: "star (pto p e) r (s,h) (s',h') \<Longrightarrow> h' (p s') = Some (e s')"
  by (auto simp: sep_def map_add_Some_iff elim!: star_elim pto_elim)

lemma pto_write:
"pto p e (s,h) (s',h') \<Longrightarrow> pto p e' (s,h(p s \<mapsto> e' s)) (s',h'(p s' \<mapsto> e' s'))"
  by (auto elim!: pto_elim intro!: pto_intro)

lemma pto_frame_write:
"star (pto p e) r (s,h) (s',h')
  \<Longrightarrow> star (pto p e') r (s,h(p s \<mapsto> e' s)) (s',h'(p s' \<mapsto> e' s'))"
proof -
  assume "star (pto p e) r (s,h) (s',h')"
  then obtain h1 h2 h1' h2' where
    "sep h h1 h2" "sep h' h1' h2'"
    "pto p e (s,h1) (s',h1')"
    "r (s, h2) (s', h2')"
    by (auto elim: star_elim)
  then have
    "sep (h(p s \<mapsto> e' s)) (h1(p s \<mapsto> e' s)) h2"
    "sep (h'(p s' \<mapsto> e' s')) (h1'(p s' \<mapsto> e' s')) h2'"
    "pto p e' (s, h1(p s \<mapsto> e' s)) (s', h1'(p s' \<mapsto> e' s'))"
    "r (s, h2) (s', h2')"
    by (auto simp: sep_def domIff map_add_upd_left intro: pto_intro elim!: pto_elim)
  then show "star (pto p e') r (s,h(p s \<mapsto> e' s)) (s',h'(p s' \<mapsto> e' s'))" ..
qed

(* some more functions *)

definition implies :: "'a pred \<Rightarrow> 'a pred \<Rightarrow> bool" where
"implies P Q \<equiv> \<forall> s h s' h'. P (s,h) (s',h') \<longrightarrow> Q (s,h) (s',h')"

lemma
implies_refl[simp]:   "implies P P" and
implies_trans[intro]: "implies P Q \<Longrightarrow> implies Q R \<Longrightarrow> implies P R"
  unfolding implies_def by blast+

lemma implies_conseq[intro]:
"\<lbrakk>implies P Q; P (s,h) (s',h')\<rbrakk> \<Longrightarrow> Q (s,h) (s',h')"
  unfolding implies_def by blast

lemma implies_frame[intro]: (* frame is typically on the right hand side of the star *)
"implies P Q \<Longrightarrow> implies (star P R) (star Q R)"
  unfolding implies_def by (auto intro: star_intro elim!: star_elim)

lemma
star_mono:       "\<lbrakk>implies P P'; implies Q Q'; star P Q (s,h) (s',h')\<rbrakk> \<Longrightarrow> star P' Q' (s,h) (s',h')" and
star_mono_left:  "\<lbrakk>implies P P'; star P Q (s,h) (s',h')\<rbrakk> \<Longrightarrow> star P' Q (s,h) (s',h')" and
star_mono_right: "\<lbrakk>implies Q Q'; star P Q (s,h) (s',h')\<rbrakk> \<Longrightarrow> star P Q' (s,h) (s',h')"
  by (auto intro: star_intro elim!: star_elim)

(* symmetry of assertions wrt. the two states *)

definition pred_sym :: "'a pred \<Rightarrow> bool" where
"pred_sym P \<equiv> \<forall> s h s' h'. P (s,h) (s',h') = P (s',h') (s,h)"

lemma pred_sym_intro:
"(\<And> s h s' h'. P (s,h) (s',h') \<Longrightarrow> P (s',h') (s,h)) \<Longrightarrow> pred_sym P"
  unfolding pred_sym_def by blast

lemma pred_sym_swap:
"\<lbrakk>pred_sym P; P (s,h) (s',h')\<rbrakk> \<Longrightarrow> P (s',h') (s,h)"
  unfolding pred_sym_def by blast

lemma rexpr_sem_sym[intro]: "rexpr_sem lev a s s' \<Longrightarrow> rexpr_sem lev a s' s"
  by(induction a arbitrary: s s',
     auto elim!: sec.cases intro: sec_intro)

lemma assert_sem_sym[intro]: "pred_sym (assert_sem lev a)"
  by (induction a)
     (auto dest: pred_sym_swap intro!: star_intro ex_intro pred_sym_intro elim!: star_elim ex_elim simp: sec.simps pto.simps cond_def)

(* coincidence wrt. a single non-free variable (used in assignments) *)

lemma emp_coincidence[simp]:
"emp (s(x:=v),h) (s'(x:=v'),h') = emp (s,h) (s',h')"
  by auto
lemma pure_coincidence[simp]:
"x \<notin> expr_free b \<Longrightarrow> pure (pure_sem b) (s(x:=v)) (s'(x:=v')) = pure (pure_sem b) s s'"
  by auto
lemma sec_coincidence[simp]:
"x \<notin> expr_free e \<Longrightarrow> x \<notin> expr_free l \<Longrightarrow> sec lev (expr_sem e) (expr_sem l) (s(x:=v)) (s'(x:=v')) = sec lev (expr_sem e) (expr_sem l) s s'"
  by (auto intro: sec_intro elim!: sec_elim)
lemma pto_coincidence[simp]:
"x \<notin> expr_free p \<Longrightarrow> x \<notin> expr_free e \<Longrightarrow> pto (expr_sem p) (expr_sem e) (s(x:=v),h) (s'(x:=v'),h') = pto (expr_sem p) (expr_sem e) (s,h) (s',h')"
  by (auto simp: pto.simps)
lemma star_coincidence:
"\<lbrakk>\<And> h h'. P (s1, h) (s1', h') = P (s2, h) (s2', h');
  \<And> h h'. Q (s1, h) (s1', h') = Q (s2, h) (s2', h')\<rbrakk>
  \<Longrightarrow> star P Q (s1, h) (s1', h') = star P Q (s2, h) (s2', h')"
  by (auto intro: star_intro elim!: star_elim)
lemma star_coincidence_intro:
"\<lbrakk>\<And> h h'. P (s1, h) (s1', h') = P (s2, h) (s2', h');
  \<And> h h'. Q (s1, h) (s1', h') = Q (s2, h) (s2', h');
  star P Q (s1, h) (s1', h')\<rbrakk>
  \<Longrightarrow> star P Q (s2, h) (s2', h')"
  by (auto intro: star_intro elim!: star_elim)
lemma ex_coincidence_same[simp]:
"ex x (assert_sem lev p) (s(x:=v),h) (s'(x:=v'),h') = ex x (assert_sem lev p) (s,h) (s',h')"
  by (auto intro!: ex_intro elim!: ex_elim)

lemma rexpr_sem_coincidence[simp]:
  "x \<notin> rexpr_free p \<Longrightarrow> rexpr_sem lev p (s(x := v)) (s'(x := v')) = rexpr_sem lev p s s'"
  by(induction p, auto)

lemma assert_coincidence[simp]:
"x \<notin> assert_free a \<Longrightarrow> assert_sem lev a (s(x:=v),h) (s'(x:=v'),h') = assert_sem lev a (s,h) (s',h')"
  apply (induction a arbitrary: h h' s s')
  apply (auto intro!: star_coincidence simp: cond_def)
  by (elim ex_elim;
      rename_tac va vb, rule_tac v=va and v'=vb in ex_intro, 
      metis fun_upd_twist fun_upd_upd)+

(* coincidence wrt. an entire set of non-free variable (used in framing) *)

lemma pure_coincidence_strong:
"\<lbrakk>coincide s1 s2 (expr_free b); coincide s1' s2' (expr_free b)\<rbrakk>
  \<Longrightarrow> pure (pure_sem b) s2 s2' = pure (pure_sem b) s1 s1'"
  by auto
lemma sec_coincidence_strong:
"\<lbrakk>coincide s1 s2 (expr_free e); coincide s1' s2' (expr_free e);
  coincide s1 s2 (expr_free l); coincide s1' s2' (expr_free l)\<rbrakk>
  \<Longrightarrow> sec lev (expr_sem e) (expr_sem l) s2 s2' = sec lev (expr_sem e) (expr_sem l) s1 s1'"
  by (auto simp: sec.simps)
lemma rexpr_sem_coincidence_strong:
"\<lbrakk>coincide s1 s2 (rexpr_free p); coincide s1' s2' (rexpr_free p)\<rbrakk>
  \<Longrightarrow> rexpr_sem lev p s2 s2' = rexpr_sem lev p s1 s1'"
  by(induction p, auto simp: sec_coincidence_strong)


lemma pto_coincidence_strong:
"\<lbrakk>coincide s1 s2 (expr_free p); coincide s1' s2' (expr_free p);
  coincide s1 s2 (expr_free e); coincide s1' s2' (expr_free e)\<rbrakk>
  \<Longrightarrow> pto (expr_sem p) (expr_sem e) (s2,h) (s2',h') = pto (expr_sem p) (expr_sem e) (s1,h) (s1',h')"
  by (auto simp: pto.simps)

lemma coincide_strengthen[simp]:
  "coincide s1 s2 (P - {x}) \<Longrightarrow> coincide (s1(x := v)) (s2(x := v)) P"
  by (simp add: coincide_reduce)

lemma assert_coincidence_strong:
"\<lbrakk>coincide s1 s2 (assert_free a); coincide s1' s2' (assert_free a)\<rbrakk>
  \<Longrightarrow> assert_sem lev a (s2,h) (s2',h') = assert_sem lev a (s1,h) (s1',h')"
  apply (induction a arbitrary: h h' s1 s2 s1' s2')
        apply (auto intro!: star_coincidence simp: rexpr_sem_coincidence_strong cond_def pure_coincidence_strong pto_coincidence_strong sec_coincidence_strong elim!: ex_elim,
               (metis (full_types) ex_intro coincide_strengthen)+)
  done

fun assert_lows :: "'level \<Rightarrow> assert \<Rightarrow> 'a state \<Rightarrow> 'a set" where
"assert_lows lev Emp s = {}" |
"assert_lows lev (Rel p) s = {}" |
"assert_lows lev (Pto p l e) s  = (if to_level (expr_sem l s) \<le> lev then {expr_sem p s} else {})" |
"assert_lows lev (And  p q) s   = assert_lows lev p s \<union> assert_lows lev q s" |
"assert_lows lev (Star p q) s   = assert_lows lev p s \<union> assert_lows lev q s" |
"assert_lows lev (Cond b p q) s = (if pure_sem b s then assert_lows lev p s else assert_lows lev q s)" |
"assert_lows lev (Ex x p) s = (if (\<forall>v s. assert_lows lev p (s(x:=v)) = assert_lows lev p s) then assert_lows lev p s else {})"

text {*
  A syntactic check for @{term assert_wf}
*}
primrec
  assert_wf :: "assert \<Rightarrow> var set \<Rightarrow> bool"
  where
  "assert_wf Emp vs = True" |
  "assert_wf (Rel p) vs = True" |
  "assert_wf (Pto p l e) vs = ((expr_free p \<union> expr_free l) \<inter> vs = {})" |
  "assert_wf (And p q) vs = (assert_wf p vs \<and> assert_wf q vs)" |
  "assert_wf (Star p q) vs = (assert_wf p vs \<and> assert_wf q vs)" |
  "assert_wf (Cond b p q) vs = (assert_wf p vs \<and> assert_wf q vs \<and> expr_free b \<inter> vs = {})" |
  "assert_wf (Ex x p) vs = (assert_wf p (insert x vs))"

primrec 
  lows_free :: "assert \<Rightarrow> var set" where
  "lows_free Emp = {}" |
  "lows_free (Rel p) = {}" |
  "lows_free (Pto p l e) = (expr_free p \<union> expr_free l)" |
  "lows_free (And p q) = (lows_free p \<union> lows_free q)" |
  "lows_free (Star p q) = (lows_free p \<union> lows_free q)" |
  "lows_free (Cond b p q) = (lows_free p \<union> lows_free q \<union> expr_free b)" |
  "lows_free (Ex x p) = ((lows_free p) - {x})"

lemma assert_wf_lows_free:
  "assert_wf p vs \<Longrightarrow> vs \<inter> lows_free p = {}"
  by(induction p arbitrary: vs, auto)

lemma assert_wf_unI:
  "assert_wf p vs \<Longrightarrow> vs' \<inter> lows_free p = {} \<Longrightarrow> assert_wf p (vs \<union> vs')"
  apply(induction p arbitrary: vs)
  using assert_wf_lows_free by force+ 

lemma lows_free_assert_wf_mono:
  "lows_free q \<subseteq> lows_free p \<Longrightarrow>
   assert_wf p vs \<Longrightarrow> assert_wf q vs \<Longrightarrow> vs \<subseteq> vs' \<Longrightarrow>
   assert_wf p vs' \<Longrightarrow> assert_wf q vs'"
proof(induction q arbitrary: p vs vs')
  case Emp
  then show ?case by simp
next
  case (Rel x)
  then show ?case by simp
next
  case (Pto x1 x2a x3)
  then show ?case 
    by(auto dest!: assert_wf_lows_free) 
next
  case (And q1 q2)
  then show ?case 
    by(simp, blast dest: assert_wf_lows_free)
next
  case (Star q1 q2)
  then show ?case
    by(simp, blast dest: assert_wf_lows_free)
next
  case (Cond x1 q1 q2)
  then show ?case
    apply simp
    using assert_wf_lows_free
    by (metis empty_subsetI eq_iff equalityD2 inf_mono inf_sup_aci(1))
next
  case (Ex x1 q)
  then show ?case
  apply simp
  using assert_wf_lows_free assert_wf_unI 
  by (smt Diff_Diff_Int Diff_cancel Diff_insert0 Int_Un_distrib Un_Diff_Int Un_insert_left insert_disjoint(1) subset_Un_eq)
qed

lemma assert_wf_mono:
  "assert_wf P vs \<Longrightarrow> vs' \<subseteq> vs \<Longrightarrow> assert_wf P vs'"
  apply(induct P arbitrary: vs vs')
  using insert_mono by auto blast

lemma assert_wf_empty_simp:
  "assert_wf p {} = (\<forall>vs. lows_free p \<inter> vs = {} \<longrightarrow> assert_wf p vs)"
  using assert_wf_mono assert_wf_unI 
  by (metis inf_bot_right inf_commute sup_bot.left_neutral)

lemma blah:
  "A  \<inter> B = {} \<Longrightarrow> x \<in> A \<Longrightarrow> x \<notin> B" 
  "A  \<inter> B = {} \<Longrightarrow> x \<in> B \<Longrightarrow> x \<notin> A" by blast+

lemma assert_wf_lows [simp]:
  "assert_wf p vs \<Longrightarrow> x \<in> vs \<Longrightarrow> assert_lows lev p (s(x := v)) = assert_lows lev p s"
  apply(induction p arbitrary: x v s vs)
  by (auto dest: expr_coindidence dest: blah(1) blah(2))

lemma assert_lows_sym'[simp]:
"assert_sem lev a (s,h) (s',h') \<Longrightarrow> lev' \<le> lev \<Longrightarrow> assert_lows lev' a s' = assert_lows lev' a s"
  apply (induction a arbitrary: lev' h h' s s')
        apply (auto elim!: sec_elim star_elim simp: cond_def elim!: ex_elim)
  using dual_order.trans by blast+

lemma assert_lows_sym[simp]:
"assert_sem lev a (s,h) (s',h') \<Longrightarrow> assert_lows lev a s' = assert_lows lev a s"
  by simp

lemma assert_lows_dom1[intro]:
  assumes "assert_sem lev a (s,h) (s',h')"
  shows   "assert_lows lev' a s \<subseteq> dom h"
using assms proof (induction a arbitrary: h h' s s')
  case Emp
  then show ?case by simp
next
  case (Rel x)
  then show ?case by simp
next
  case (Pto p l e)
  then show ?case by (auto elim!: pto_elim)
next
  case (And b p q)
  then show ?case by (simp, blast)
next
  case (Star p q)
  from Star.prems obtain hp hp' hq hq' where
    sep: "sep h hp hq" "sep h' hp' hq'" and
    p:   "assert_sem lev p (s,hp) (s',hp')" and
    q:   "assert_sem lev q (s,hq) (s',hq')"
    by (auto elim: star_elim)

  from p Star.IH have "assert_lows lev' p s \<subseteq> dom hp" by simp
  moreover from q Star.IH have "assert_lows lev' q s \<subseteq> dom hq" by simp
  moreover from sep have dom: "dom hp \<subseteq> dom h" "dom hq \<subseteq> dom h" unfolding sep_def by auto
  ultimately have "assert_lows lev' p s \<union> assert_lows lev' q s \<subseteq> dom h" by blast
  then show ?case by simp
next
  case (Cond b p q)
  then show ?case by (simp add: cond_def, meson)
next
  case (Ex x p)
  then show ?case by (auto elim!: ex_elim, blast) 
qed

lemma assert_lows_dom2[intro]:
  assumes "assert_sem lev a (s,h) (s',h')"
  assumes "lev' \<le> lev"
  shows   "assert_lows lev' a s \<subseteq> dom h'"
proof -
  from assms have "assert_sem lev a (s', h') (s, h)" by (auto intro: pred_sym_swap)
  then have "assert_lows lev' a s' \<subseteq> dom h'" ..
  with assms show ?thesis by simp
qed

lemma assert_lows_coincidence[simp]:
"x \<notin> assert_free a \<Longrightarrow> assert_lows lev a (s(x:=v)) = assert_lows lev a s"
  by (induction a) auto

lemma assert_lows_coincidence_strong[simp]:
"coincide s s' (assert_free a) \<Longrightarrow> assert_lows lev a s' = assert_lows lev a s"
  apply (induction a arbitrary: s s')
  by (auto, (metis coincide_strengthen)+)

lemma coincide_sep_subset_left:
  assumes "sep h h1 h2" and "sep h' h1' h2'"
  assumes "xs \<subseteq> dom h1" "xs \<subseteq> dom h1'"
  shows   "coincide h h' xs = coincide h1 h1' xs"
  using assms unfolding coincide_def sep_def
  by (metis map_add_comm map_add_dom_app_simps(1) set_rev_mp)

lemma coincide_sep_subset_right:
  assumes "sep h h1 h2" and "sep h' h1' h2'"
  assumes "xs \<subseteq> dom h2" "xs \<subseteq> dom h2'"
  shows   "coincide h h' xs = coincide h2 h2' xs"
  using assms unfolding coincide_def sep_def
  by (metis map_le_def map_le_map_add subset_eq)

lemma coincide_sep:
  assumes sep: "sep h h1 h2" "sep h' h1' h2'"
  assumes dom1: "xs \<subseteq> dom h1" and dom2: "ys \<subseteq> dom h2" 
  assumes co1: "coincide h1 h1' xs"
  assumes co2: "coincide h2 h2' ys"
  shows        "coincide h h' (xs \<union> ys)"
proof -
  have "coincide h h' xs"
    unfolding coincide_def proof (clarify)
    fix x
    assume "x \<in> xs"
    with dom1 co1 have "h1 x = h1' x" "x \<in> dom h1" unfolding coincide_def by blast+
    with sep show "h x = h' x"
      by (metis domD map_add_comm map_add_find_right sep_def) 
  qed
  moreover have "coincide h h' ys"
    unfolding coincide_def proof (clarify)
    fix x
    assume "x \<in> ys"
    with dom2 co2 have "h2 x = h2' x" "x \<in> dom h2"
      unfolding coincide_def by blast+
    with sep show "h x = h' x"
      by (metis domD map_add_find_right sep_def) 
  qed
  ultimately show ?thesis by simp
qed

theorem assert_lows_coincide[intro]:
"assert_sem lev a (s, h) (s', h') \<Longrightarrow> lev' \<le> lev \<Longrightarrow> coincide h h' (assert_lows lev' a s)"
proof (induction a arbitrary: h h' s s')
  case Emp
  then show ?case by simp
next
  case (Rel x)
  then show ?case by simp
next
  case (Pto p l e)
  then show ?case apply (auto elim!: sec_elim pto_elim) using dual_order.trans by blast+
next
  case (And p q)
  then show ?case by auto
next
  case (Star p q)
  from Star.prems obtain hp hp' hq hq' where
    sep: "sep h hp hq" "sep h' hp' hq'" and
    p:   "assert_sem lev p (s,hp) (s',hp')" and
    q:   "assert_sem lev q (s,hq) (s',hq')"
    by (auto elim: star_elim)
  from p Star.IH `lev' \<le> lev` have cp: "coincide hp hp' (assert_lows lev' p s)" by blast
  from q Star.IH `lev' \<le> lev` have cq: "coincide hq hq' (assert_lows lev' q s)" by blast
  from p have "(assert_lows lev' p s) \<subseteq> dom hp" by (rule assert_lows_dom1)
  moreover from q have "(assert_lows lev' q s) \<subseteq> dom hq" by (rule assert_lows_dom1)
  ultimately have "coincide h h' (assert_lows lev' p s \<union> assert_lows lev' q s)" using sep cp cq coincide_sep by blast
  then show ?case by simp
next
  case (Cond b p q)
  then show ?case by (auto simp: cond_def)
next
  case (Ex x p)
  then show ?case by (auto elim!: ex_elim, blast)
qed

lemma assert_lows_coincide_store[intro]:
  assumes "star (pto p v) (assert_sem lev r) (s1, h1) (s1', h1')"
  assumes "lev' \<le> lev"
  shows   "coincide (h1(p s1 \<mapsto> v')) (h1'(p s1' \<mapsto> v'')) (assert_lows lev' r s1)"
  unfolding coincide_def using assms apply (auto elim!: star_elim pto_elim simp: sep_def)
  using assert_lows_dom1 apply blast (*
  apply (metis assert_lows_coincide assert_lows_sym' coincide_insert insert_Diff map_add_dom_app_simps(1) pred_sym_swap semantics.assert_lows_dom1 semantics.assert_sem_sym semantics_axioms subsetCE) 
  apply (smt assert_lows_coincide coincide_insert dom_Map.empty dom_fun_upd insert_Diff map_add_dom_app_simps(2) option.distinct(1) singletonD)
  apply (metis assert_lows_coincide coincide_def domIff Map.empty_map_add fun_upd_apply map_add_upd_left option.exhaust)  *)
     defer
  using assert_lows_dom1 apply blast (* master blaster *)
  using assert_lows_dom2 apply blast
proof -
  fix h2 :: "'a \<Rightarrow> 'a option" and h2' :: "'a \<Rightarrow> 'a option" and x :: 'a
  assume a1: "lev' \<le> lev"
  assume a2: "x \<in> assert_lows lev' r s1"
  assume "h1 = [p s1 \<mapsto> v s1] ++ h2"
  assume "h1' = [p s1' \<mapsto> v s1'] ++ h2'"
  assume a3: "assert_sem lev r (s1, h2) (s1', h2')"
  assume a4: "([p s1 \<mapsto> v s1] ++ h2) x \<noteq> ([p s1' \<mapsto> v s1'] ++ h2') x"
  have "h2 x = h2' x"
    using a3 a2 a1 by (meson assert_lows_coincide coincide_def)
  then show "\<exists>y. h2' (p s1') = Some y"
    using a4 a3 a2 by (metis (no_types) assert_lows_dom1 domIff map_add_dom_app_simps(1) subsetCE)
next
  (* FML *)
  fix h2 h2' x
  show "lev' \<le> lev \<Longrightarrow>
       assert_sem lev r (s1, h2) (s1', h2') \<Longrightarrow>
       p s1' = p s1 \<Longrightarrow>
       x \<noteq> p s1 \<Longrightarrow>
       x \<in> assert_lows lev' r s1 \<Longrightarrow>
       h1 = [p s1 \<mapsto> v s1] ++ h2 \<Longrightarrow>
       ([p s1 \<mapsto> v s1] ++ h2) x \<noteq> ([p s1 \<mapsto> v s1'] ++ h2') x \<Longrightarrow>
       h1' = [p s1 \<mapsto> v s1'] ++ h2' \<Longrightarrow> h2 (p s1) = None \<Longrightarrow> \<exists>y. h2' (p s1) = Some y"
proof -
  fix h2 :: "'a \<Rightarrow> 'a option" and h2' :: "'a \<Rightarrow> 'a option" and x :: 'a
  assume "h1' = [p s1 \<mapsto> v s1'] ++ h2'"
  assume a1: "assert_sem lev r (s1, h2) (s1', h2')"
  assume a2: "([p s1 \<mapsto> v s1] ++ h2) x \<noteq> ([p s1 \<mapsto> v s1'] ++ h2') x"
  assume "h1 = [p s1 \<mapsto> v s1] ++ h2"
  assume a3: "x \<noteq> p s1"
  assume a4: "lev' \<le> lev"
  assume a5: "x \<in> assert_lows lev' r s1"
  have f6: "\<forall>z a f aa. (Map.empty(aa::'a := z::'a option) ++ f) a = f a \<or> aa = a"
    by (metis (no_types) domIff fun_upd_apply map_add_dom_app_simps(2))
  have "coincide h2 h2' (assert_lows lev' r s1)"
    using a4 a1 by (meson assert_lows_coincide)
  then show "\<exists>y. h2' (p s1) = Some y"
    using f6 a5 a3 a2 by (metis coincide_def)
qed
qed

lemma assert_lows_coincide_store_eq[intro]:
  assumes "p s1 = p s1'" (* variant to properly match *)
  assumes "star (pto p v) (assert_sem lev r) (s1, h1) (s1', h1')"
  assumes "lev' \<le> lev"
  shows   "coincide (h1(p s1' \<mapsto> v')) (h1'(p s1' \<mapsto> v'')) (assert_lows lev' r s1)"
  using assert_lows_coincide_store[of p v lev r s1 h1 s1' h1' lev' v' v''] assms by simp

lemma assert_lows_coincide_star_left[intro]:
"star (assert_sem lev a) Q (s, h) (s', h') \<Longrightarrow> lev' \<le> lev \<Longrightarrow> coincide h h' (assert_lows lev' a s)"
  using coincide_sep_subset_left assert_lows_dom1 assert_lows_dom2
  by (auto elim!: star_elim)

lemma assert_lows_coincide_star_right[intro]:
"star P (assert_sem lev a) (s, h) (s', h') \<Longrightarrow>  lev' \<le> lev \<Longrightarrow> coincide h h' (assert_lows lev' a s)"
  using coincide_sep_subset_right assert_lows_dom1 assert_lows_dom2
  by (auto elim!: star_elim)

lemma rexpr_ren[simp]:
  "y \<notin> rexpr_free r \<Longrightarrow>
  rexpr_sem lev (rexpr_ren r x y) (s(y := s x)) (s'(y := s' x)) = rexpr_sem lev r s s'"
  by(induct r, auto intro!: sec_intro elim!: sec_elim)


lemma expr_ren_expr_free[simp]:
  "y \<notin> expr_free e \<Longrightarrow> x \<notin> expr_free (expr_ren e x y)"
  "y \<notin> expr_frees es \<Longrightarrow> x \<notin> expr_frees (expr_rens es x y)"
  by (induction e x y and es x y rule: expr_ren_expr_rens.induct, auto) 
   

lemma expr_ren_undo[simp]:
  assumes nin: "y \<notin> expr_free e"
  shows "expr_sem (expr_ren (expr_ren e x y) y x) s = expr_sem e s"
  using assms expr_ren_expr_free expr_ren
proof -
  have f1: "s(y := s x, y := (s(y := s x)) x) = s(y := s x)"
    by simp
  have "s(y := s x, x := (s(y := s x)) y) = s(y := s x)"
    by (metis (no_types) fun_upd_same fun_upd_triv fun_upd_twist)
  then show ?thesis
    using f1 by (metis expr_ren expr_ren_expr_free(1) nin)
qed

lemma rexpr_ren_rexpr_free[simp]:
  "y \<notin> rexpr_free e \<Longrightarrow> x \<notin> rexpr_free (rexpr_ren e x y)"
  by(induction e, auto simp: expr_ren_expr_free)

lemma rexpr_ren_undo[simp]:
  assumes nin: "y \<notin> rexpr_free e"
  shows "rexpr_sem lev (rexpr_ren (rexpr_ren e x y) y x) s s' = rexpr_sem lev e s s'"
  using assms rexpr_ren_rexpr_free rexpr_ren 
proof -
  have f1: "s(y := s x, y := (s(y := s x)) x) = s(y := s x)"
    by simp
  have g1: "s(y := s x, x := (s(y := s x)) y) = s(y := s x)"
    by (metis (no_types) fun_upd_same fun_upd_triv fun_upd_twist)
  have f2: "s'(y := s' x, y := (s'(y := s' x)) x) = s'(y := s' x)"
    by simp
  have g2: "s'(y := s' x, x := (s'(y := s' x)) y) = s'(y := s' x)"
    by (metis (no_types) fun_upd_same fun_upd_triv fun_upd_twist)
  show ?thesis
    using f1 g1 f2 g2 by (metis rexpr_ren rexpr_ren_rexpr_free nin)
qed

lemma expr_ren_expr_free_simp[simp]: 
  "expr_free (expr_ren e x y) = 
  (if x \<in> expr_free e then (expr_free e - {x}) \<union> {y} else expr_free e)"
  "expr_frees (expr_rens es x y) = 
  (if x \<in> expr_frees es then (expr_frees es - {x}) \<union> {y} else expr_frees es)"
  by(induction e and es rule: expr_free_expr_frees.induct, auto)

lemma rexpr_ren_rexpr_free_simp[simp]: 
  "rexpr_free (rexpr_ren e x y) = 
  (if x \<in> rexpr_free e then (rexpr_free e - {x}) \<union> {y} else rexpr_free e)"
  by(induction e , auto)

lemma assert_ren_assert_vars[simp]:
  "y \<notin> assert_vars p \<Longrightarrow> x \<notin> assert_vars (assert_ren p x y)"
  by(induct p arbitrary: x y, auto)

lemma expr_ren_noop[simp]:
  "y \<notin> expr_free e \<Longrightarrow> expr_ren e y x = e"
  "y \<notin> expr_frees es \<Longrightarrow> expr_rens es y x = es"
  by(induction e y x and es y x rule: expr_ren_expr_rens.induct, auto)

lemma rexpr_ren_noop[simp]:
  "y \<notin> rexpr_free e \<Longrightarrow> rexpr_ren e y x = e"
  by(induction e, auto)


lemma assert_ren_undo:
  assumes nin: "y \<notin> assert_vars p"
  shows "assert_sem lev (assert_ren (assert_ren p x y) y x) (s,h) (s',h') = assert_sem lev p (s,h) (s',h')"
  using assms
 apply(induct p arbitrary: s h s' h')
        apply clarsimp+
      apply(auto intro!: pto_intro sec_intro elim!: pto_elim elim: sec_elim simp: pto.simps)[2]
     apply (auto simp: star.simps cond_def pure_sem_def)[2]
  by (auto simp: ex.simps)


lemma assert_free_vars:
  "assert_free p \<subseteq> assert_vars p"
  by(induct p, auto)

lemma assert_ren_mod_ren[simp]:
  "y \<notin> assert_vars p \<Longrightarrow> assert_sem lev (assert_ren p x y) (s(x:=v),h) (s'(x:=v'),h') = assert_sem lev (assert_ren p x y) (s,h) (s',h')"
  using assert_free_vars assert_coincidence assert_ren_assert_vars by blast

(* TODO: not sure why this proof cannot be found more easily ... *)
lemma assert_ren_ex:
  assumes hyp: "\<And>x y s h s' h'.
           y \<notin> assert_vars p \<Longrightarrow>
           assert_sem lev p (s, h) (s', h') = assert_sem lev (assert_ren p x y) (s(y := s x), h) (s'(y := s' x), h')"
  assumes nin: "y \<notin> assert_vars (assert.Ex x1 p)"
  shows "assert_sem lev (assert.Ex x1 p) (s, h) (s', h') =
         assert_sem lev (assert_ren (assert.Ex x1 p) x y) (s(y := s x), h) (s'(y := s' x), h')"
proof(clarsimp simp: ex.simps | safe)+
  from nin have neq: "y \<noteq> x1" by simp
  from nin have nin: "y \<notin> assert_vars p" by simp
  fix v v'
  assume a: "x = x1" and b: "assert_sem lev p (s(x1 := v), h) (s'(x1 := v'), h')"
  from a b assms assert_ren_mod_ren show "\<exists>v v'. assert_sem lev (assert_ren p x1 y) (s(y := v), h) (s'(y := v'), h')"
  proof -
    have f6: "\<forall>a f aa. f(x := aa::'a, y := a) = f(y := a, x := aa)"
      using neq a by (simp add: fun_upd_twist)
    have "assert_sem lev (assert_ren p x y) (s(x := v, y := v), h) (s'(x := v', y := (s'(x := v')) x), h')"
      using fun_upd_same assert_ren_mod_ren nin b 
      by (metis a hyp) 
    then show ?thesis 
      using f6
      using a nin by auto
  qed
next 
  from nin have neq: "y \<noteq> x1" by simp
  from nin have nin: "y \<notin> assert_vars p" by simp
  assume "x = x1"
  fix v v'
  assume "assert_sem lev (assert_ren p x1 y) (s(y := v), h) (s'(y := v'), h')"
  thus "\<exists>v v'. assert_sem lev p (s(x1 := v), h) (s'(x1 := v'), h')"
    using neq nin hyp assert_ren_mod_ren
  proof -
    have "\<exists>a aa ab. assert_sem lev (assert_ren p x1 y) (s(y := a), h) (s'(x1 := ab, y := aa), h')"
      by (metis (no_types) \<open>assert_sem lev (assert_ren p x1 y) (s(y := v), h) (s'(y := v'), h')\<close> fun_upd_triv)
    then have "\<exists>a aa ab. assert_sem lev (assert_ren p x1 y) (s(y := aa), h) (s'(x1 := ab, y := ab, x1 := a), h')"
      by (metis (no_types) fun_upd_twist fun_upd_upd neq)
    then have "\<exists>a aa ab ac. assert_sem lev (assert_ren p x1 y) (s(x1 := ac, y := aa), h) (s'(x1 := a, y := a, x1 := ab), h')"
      by (metis (no_types) fun_upd_triv)
    then have "\<exists>a aa ab ac. assert_sem lev (assert_ren p x1 y) (s(x1 := aa, y := aa, x1 := ac), h) (s'(x1 := a, y := a, x1 := ab), h')"
      by (simp add: fun_upd_twist neq)
    then have "\<exists>a aa. assert_sem lev (assert_ren p x1 y) (s(x1 := aa, y := aa), h) (s'(x1 := a, y := (s'(x1 := a)) x1), h')"
      by (metis (no_types) assert_ren_mod_ren fun_upd_apply nin)
    then show ?thesis
      by (metis fun_upd_same hyp nin)
  qed
next
  from nin have neq: "y \<noteq> x1" by simp
  from nin have nin: "y \<notin> assert_vars p" by simp
  fix v v'
  assume neqx: "x \<noteq> x1" and b: "assert_sem lev p (s(x1 := v), h) (s'(x1 := v'), h')"
  show "\<exists>v v'. assert_sem lev (assert_ren p x y) (s(y := s x, x1 := v), h) (s'(y := s' x, x1 := v'), h')"
  proof(cases "x=y")
    case True
    then show ?thesis
      using neqx b neq nin assert_ren_mod_ren
      by (metis fun_upd_triv hyp)
  next
    case False
    then show ?thesis
      using hyp neqx b neq nin assert_ren_mod_ren fun_upd_same fun_upd_triv fun_upd_twist 
    proof -
      have "\<forall>f a. f x = (f(x1 := a::'a)) x"
        using neqx by auto
      then have "\<exists>a aa. assert_sem lev (assert_ren p x y) (s(x1 := a, y := s x), h) (s'(x1 := aa, y := s' x), h')"
        by (metis (no_types) b hyp nin)
      then show ?thesis
        by (simp add: fun_upd_twist neq)
    qed
  qed
next
  from nin have neq: "y \<noteq> x1" by simp
  from nin have nin: "y \<notin> assert_vars p" by simp
  fix v v'
  assume neqx: "x \<noteq> x1"  and b: "assert_sem lev (assert_ren p x y) (s(y := s x, x1 := v), h) (s'(y := s' x, x1 := v'), h')"
  show "\<exists>v v'. assert_sem lev p (s(x1 := v), h) (s'(x1 := v'), h')"
    using nin neq hyp neqx b assert_ren_mod_ren
  proof -
    have "\<exists>a aa. assert_sem lev (assert_ren p x y) (s(x1 := a, y := s x), h) (s'(x1 := aa, y := s' x), h')"
      by (metis (no_types) b fun_upd_twist neq)
    then have "\<exists>a cs aa. assert_sem lev (assert_ren p cs y) (s(x1 := aa, y := (s(x1 := aa)) cs), h) (s'(x1 := a, y := (s'(x1 := a)) cs), h')"
    using neqx by auto
    then show ?thesis
      by (meson hyp nin)
  qed
qed

  
lemma assert_ren:
  "y \<notin> assert_vars p \<Longrightarrow>
   assert_sem lev p (s,h) (s',h') = assert_sem lev (assert_ren p x y) (s(y := s x),h) (s'(y := s' x),h')"
  apply(induct p arbitrary: x y s h s' h')
        apply clarsimp+
      apply(auto intro!: pto_intro sec_intro elim!: pto_elim elim: sec_elim simp: pto.simps)[2]
    apply (auto simp: star.simps cond_def)[2]
  using assert_ren_ex by metis

lemma expr_ren_triv[simp]:
  "expr_ren e y y = e"
  "expr_rens es y y = es"
  by(induct rule: expr_ren_expr_rens.induct, auto)

lemma rexpr_ren_triv[simp]:
  "rexpr_ren r y y = r"
  by(induct r, auto)

lemma assert_ren_triv[simp]:
  "assert_ren p y y = p"
  by(induct p, auto)


lemma expr_ren_free_subset: 
  "expr_free (expr_ren e x y) \<subseteq> (expr_free e - {x}) \<union> {y}"
  "expr_frees (expr_rens es x y) \<subseteq> (expr_frees es - {x}) \<union> {y}"
  by(induct rule: expr_ren_expr_rens.induct) auto

lemma rexpr_ren_free_subset:
  "rexpr_free (rexpr_ren r x y) \<subseteq> (rexpr_free r - {x}) \<union> {y}"
  using expr_ren_free_subset by(induct r, auto)

lemma assert_ren_vars_subset:
  "assert_vars (assert_ren p x y) \<subseteq> (assert_vars p - {x}) \<union> {y}"
  apply(induct p)
  using rexpr_ren_free_subset expr_ren_free_subset subsetD by auto

lemma assert_ren_free_subset:
  "assert_free (assert_ren p x y) \<subseteq> (assert_free p - {x}) \<union> {y}"
  apply(induct p)
  using rexpr_ren_free_subset expr_ren_free_subset subsetD by auto

lemma assert_ren_mod[simp]: 
  "x \<noteq> y \<Longrightarrow>
   assert_sem lev (assert_ren p x y) (s(x := v),h) (s'(x := v'),h') =
   assert_sem lev (assert_ren p x y) (s,h) (s',h')"
  using assert_ren_free_subset assert_coincidence
  by (metis (no_types, lifting) Diff_disjoint Un_insert_right assert_free_vars blah(1) insertE singletonI subsetCE sup_bot.right_neutral)

lemma assert_sem_ren_ex_1:
  "y \<notin> assert_vars p \<Longrightarrow> x \<noteq> y \<Longrightarrow>
  assert_sem lev (Ex x p) (s,h) (s',h') \<Longrightarrow> assert_sem lev (Ex y (assert_ren p x y)) (s,h) (s',h')"
  apply (clarsimp simp: ex.simps)
  using assert_ren assert_ren_mod fun_upd_twist
proof -
  fix v :: 'a and v' :: 'a
  assume a1: "y \<notin> assert_vars p"
  assume a2: "x \<noteq> y"
  assume "assert_sem lev p (s(x := v), h) (s'(x := v'), h')"
  then have "\<forall>a aa. assert_sem lev (assert_ren p x y) (s(x := v, y := (s(x := v)) x, x := a), h) (s'(x := v', y := (s'(x := v')) x, x := aa), h')"
  using a2 a1 by (meson assert_ren_mod assert_ren)
then show "\<exists>a aa. assert_sem lev (assert_ren p x y) (s(y := a), h) (s'(y := aa), h')"
  using a2 by auto
qed

lemma assert_sem_ren_ex_2:
  "y \<notin> assert_vars p \<Longrightarrow> x \<noteq> y \<Longrightarrow>
  assert_sem lev (Ex y (assert_ren p x y)) (s,h) (s',h') \<Longrightarrow> assert_sem lev (Ex x p) (s,h) (s',h')"
  apply (clarsimp simp: ex.simps)
  apply (subst assert_ren[where x=x], assumption, simp)
  using assert_ren assert_ren_mod[where x=x] fun_upd_twist
proof -
  fix v :: 'a and v' :: 'a
  assume a1: "x \<noteq> y"
  assume a2: "assert_sem lev (assert_ren p x y) (s(y := v), h) (s'(y := v'), h')"
  have "\<forall>a f aa. f(x := aa::'a, y := a) = f(y := a, x := aa)"
    using a1 by (meson fun_upd_twist)
    then show "\<exists>a aa. assert_sem lev (assert_ren p x y) (s(x := a, y := a), h) (s'(x := aa, y := aa), h')"
using a2 a1 by auto
qed

lemma assert_sem_ren_ex:
  "y \<notin> assert_vars p \<Longrightarrow>
  assert_sem lev (Ex x p) (s,h) (s',h') = assert_sem lev (Ex y (assert_ren p x y)) (s,h) (s',h')"
  using assert_sem_ren_ex_1 assert_sem_ren_ex_2 by(cases "x=y") auto

end

end
