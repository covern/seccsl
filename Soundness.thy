theory Soundness
  imports Shared
begin

context concurrency
begin

abbreviation maybe_shared :: "bool \<Rightarrow> assert" where
  "maybe_shared sh \<equiv> (if sh then shared else Emp)"

definition inv :: "bool \<Rightarrow> 'level \<Rightarrow> ctx \<Rightarrow> assert \<Rightarrow> assert \<Rightarrow> 'a pred" where
 (* - the shared part of the heap
    - shape of the local heap
    - invariants of shared, non-held locks
    - the shared part of the heap 
    - fix a specific frame *)
"inv sh lev \<Gamma> p f
  = stars ([assert_sem lev p,
            assert_sem lev (lockinvs \<Gamma>),
            assert_sem lev (maybe_shared sh),
            assert_sem lev f])"

lemma inv_reorder:
"set \<Gamma> = set \<Gamma>' \<Longrightarrow> inv sh lev \<Gamma> p f = inv sh lev \<Gamma>' p f"
  unfolding inv_def apply (clarsimp)
  by (metis lockinvs_reorder)

lemma inv_pure[simp]:
"inv sh lev \<Gamma> (Star (Rel (Pure b)) p) f (s, H) (s', H')
  = (pure_sem b s \<and> pure_sem b s' \<and> inv sh lev \<Gamma> p f (s, H) (s', H'))"
  unfolding inv_def by simp

lemma inv_rel:
"inv sh lev \<Gamma> (Star (Rel r) p) f (s, H) (s', H')
  = (rexpr_sem lev r s s' \<and> inv sh lev \<Gamma> p f (s, H) (s', H'))"
  unfolding inv_def by simp

lemma inv_low[simp]:
"inv sh lev \<Gamma> (Star (Rel (Sec b (level_expr lev))) p) f (s, H) (s', H')
  = (expr_sem b s = expr_sem b s' \<and> inv sh lev \<Gamma> p f (s, H) (s', H'))"
  unfolding inv_def 
  by (simp add: sec.simps)

(* reverse only holds if resource invariants and frame are precise *)
lemma inv_and:
"inv sh lev \<Gamma> (And p q) f (s, H) (s', H')
  \<Longrightarrow> (inv sh lev \<Gamma> p f (s, H) (s', H') \<and> inv sh lev \<Gamma> q f (s, H) (s', H'))"
   unfolding inv_def by (auto elim!: star_elim intro!: star_intro)

inductive step :: "bool \<Rightarrow> 'level \<Rightarrow> cmd \<Rightarrow> ctx \<Rightarrow> assert \<Rightarrow> ctx \<Rightarrow> assert \<Rightarrow> 'a rel" where
step_intro:
"\<lbrakk>assert_free p2 \<subseteq> assert_free p1 \<union> cmd_vars c1 \<union> invvars;
  (* set \<Gamma>1 \<subseteq> locks \<Longrightarrow> *) set \<Gamma>2 \<subseteq> locks; assert_wf p2 {}; 
  assert_lows lev (Star p1 (Star (maybe_shared sh) (lockinvs \<Gamma>1))) s1 \<subseteq> assert_lows lev (Star p2 (Star (maybe_shared sh) (lockinvs \<Gamma>2))) s2;
  lows_free (Star p2 (Star (maybe_shared sh) (lockinvs \<Gamma>2))) \<subseteq> lows_free (Star p1 (Star (maybe_shared sh) (lockinvs \<Gamma>1)))\<rbrakk>
  \<Longrightarrow> step sh lev c1 \<Gamma>1 p1 \<Gamma>2 p2 s1 s2"


lemma step_reorder:
"set \<Gamma>1 = set \<Gamma>1' \<Longrightarrow> set \<Gamma>2 = set \<Gamma>2' \<Longrightarrow> step sh lev c1 \<Gamma>1 p1 \<Gamma>2 p2 s1 s2 = step sh lev c1 \<Gamma>1' p1 \<Gamma>2' p2 s1 s2"
  unfolding step.simps apply (clarsimp split del: if_splits)
  using lockinvs_lows lockinvs_lows_free lockinvs_wf by(auto split del: if_splits)

inductive_cases step_elim: "step sh lev c1 \<Gamma>1 p1 \<Gamma>2 p2 s1 s2"
find_theorems assert_wf
inductive pre :: "bool \<Rightarrow> 'level \<Rightarrow> locks \<Rightarrow> assert \<Rightarrow> assert \<Rightarrow> cmd \<Rightarrow> 'a conf \<Rightarrow> 'a conf \<Rightarrow> 'a pred" where
pre_intro:
"\<lbrakk>cmd_mod c1 \<inter> assert_free f = {};
  cmd_mod c1 \<inter> invvars = {};
  set \<Gamma>1 \<subseteq> locks; assert_wf p1 {}; assert_wf f {};
  cmd_step \<sigma> (Run c1 (set \<Gamma>1) s1  H1)  k;
  cmd_step \<sigma> (Run c1 (set \<Gamma>1) s1' H1') k';
  inv sh lev \<Gamma>1 p1 f (s1,H1) (s1',H1')\<rbrakk>
  \<Longrightarrow> pre sh lev (set \<Gamma>1) p1 f c1 k k' (s1,H1) (s1',H1')"

(* don't use this one *)
inductive_cases pre_elim_: "pre sh lev (set \<Gamma>1) p1 f c1 k k' (s1,H1) (s1',H1')"

lemma pre_elim:
  assumes
    "pre sh lev (set \<Gamma>1) p1 f c1 k k' (s1,H1) (s1',H1')"
  obtains (pre) \<sigma> where
    "cmd_mod c1 \<inter> assert_free f = {}"
    "cmd_mod c1 \<inter> invvars = {}"
    "set \<Gamma>1 \<subseteq> locks" "assert_wf p1 {}" "assert_wf f {}"
    "cmd_step \<sigma> (Run c1 (set \<Gamma>1) s1  H1)  k"
    "cmd_step \<sigma> (Run c1 (set \<Gamma>1) s1' H1') k'"
    "inv sh lev \<Gamma>1 p1 f (s1,H1) (s1',H1')"
  using assms by(auto elim!: pre_elim_ intro!: that, subst inv_reorder, auto)

lemma pre_pure[simp]:
"pre sh lev (set \<Gamma>) (Star (Rel (Pure b)) p) f c k k' (s, H) (s', H')
  = (pure_sem b s \<and> pure_sem b s' \<and> pre sh lev (set \<Gamma>) p f c k k' (s, H) (s', H'))"
  by(auto intro!: pre_intro elim!: pre_elim)

(* don't make this [simp] as it conflicts with pre_pure above *)
lemma pre_rel:
"pre sh lev (set \<Gamma>) (Star (Rel r) p) f c k k' (s, H) (s', H')
  = (rexpr_sem lev r s s' \<and> pre sh lev (set \<Gamma>) p f c k k' (s, H) (s', H'))"
  by(auto intro!: pre_intro elim!: pre_elim simp: inv_rel)

lemma pre_sec[simp]:
"pre sh lev (set \<Gamma>) (Star (Rel (Sec b (level_expr lev))) p) f c k k' (s, H) (s', H')
  = (expr_sem b s = expr_sem b s' \<and> pre sh lev (set \<Gamma>) p f c k k' (s, H) (s', H'))"
  by(auto elim!: pre_elim intro!: pre_intro inv_low)

lemma pre_elim_stop:
  assumes
    "pre sh lev (set \<Gamma>1) p1 f c1 (Stop \<Gamma>2 s2 H2) (Stop \<Gamma>2 s2' H2') (s1,H1) (s1',H1')"
  obtains (pre) \<sigma> where
    "cmd_mod c1 \<inter> assert_free f = {}"
    "set \<Gamma>1 \<subseteq> locks" "assert_wf p1 {}" "assert_wf f {}"
    (* add more stuff here if needed *)
    "coincide s1 s2 (- cmd_mod c1)"
    "coincide s1' s2' (- cmd_mod c1)"
    "inv sh lev \<Gamma>1 p1 f (s1,H1) (s1',H1')"
  using assms by (auto elim!: pre_elim dest!: cmd_step_mod)

lemma pre_elim_run:
  assumes
    "pre sh lev (set \<Gamma>1) p1 f c1 (Run c2 \<Gamma>2 s2 H2) (Run c2 \<Gamma>2 s2' H2') (s1,H1) (s1',H1')"
  obtains (pre) \<sigma> where
    "cmd_mod c1 \<inter> assert_free f = {}"
    "cmd_mod c1 \<inter> invvars = {}"
    "set \<Gamma>1 \<subseteq> locks" "assert_wf p1 {}" "assert_wf f {}"
    (* add more stuff here if needed *)
    "cmd_mod c2 \<subseteq> cmd_mod c1"
    "cmd_vars c2 \<subseteq> cmd_vars c1"
    "coincide s1 s2 (- cmd_mod c1)"
    "coincide s1' s2' (- cmd_mod c1)"
    "inv sh lev \<Gamma>1 p1 f (s1,H1) (s1',H1')"
  using assms by (auto elim!: pre_elim, metis cmd_step_mod cmd_step_preserves_mod cmd_step_preserves_vars conf_mod.simps conf_vars.simps)

inductive
rec    :: "bool \<Rightarrow> 'level \<Rightarrow> nat \<Rightarrow> locks \<Rightarrow> assert \<Rightarrow> assert \<Rightarrow> cmd \<Rightarrow> 'a conf \<Rightarrow> 'a conf \<Rightarrow> assert \<Rightarrow> 'a pred"
  and
secure :: "bool \<Rightarrow> 'level \<Rightarrow> nat \<Rightarrow> assert \<Rightarrow> cmd \<Rightarrow> assert \<Rightarrow> bool"
where

secure_stop[intro!]:
"\<lbrakk>inv sh lev \<Gamma>' q f (s2,H2) (s2',H2');
  step sh lev c1 \<Gamma>1 p1 \<Gamma>' q s1 s2\<rbrakk>
  \<Longrightarrow> rec sh lev n (set \<Gamma>1) p1 f c1 (Stop (set \<Gamma>') s2 H2) (Stop (set \<Gamma>') s2' H2') q (s1,H1) (s1',H1')" |

secure_rec[intro!]:
"\<lbrakk>inv sh lev \<Gamma>2 p2 f (s2,H2) (s2',H2');
  step sh lev c1 \<Gamma>1 p1 \<Gamma>2 p2 s1 s2;
  secure sh lev n p2 c2 q\<rbrakk>
  \<Longrightarrow> rec sh lev n (set \<Gamma>1) p1 f c1 (Run c2 (set \<Gamma>2) s2 H2) (Run c2 (set \<Gamma>2) s2' H2') q (s1,H1) (s1',H1')" |

[simp]: "secure sh lev 0 p1 c1 q" |

secure_intro:
"\<lbrakk>\<And> \<Gamma>1 s1 s1' H1 H1' k k' f.
        pre sh lev (set \<Gamma>1) p1 f c1 k k' (s1,H1) (s1',H1')
    \<Longrightarrow> rec sh lev n (set \<Gamma>1) p1 f c1 k k' q (s1,H1) (s1',H1')\<rbrakk>
  \<Longrightarrow> secure sh lev (Suc n) p1 c1 q"

(* adds that m is non-zero, fixes some \<Gamma>1 *)
lemma secure_intro':
  assumes "\<And> n \<Gamma>1 s1 s1' H1 H1' k k' f.
        m = Suc n
    \<Longrightarrow> pre sh lev (set \<Gamma>1) p1 f c1 k k' (s1,H1) (s1',H1')
    \<Longrightarrow> rec sh lev n (set \<Gamma>1) p1 f c1 k k' q (s1,H1) (s1',H1')"
  shows "secure sh lev m p1 c1 q"
  apply (cases m, simp_all)
  using assms by (blast intro!: secure_intro)

(* don't use this *)
inductive_cases rec_elim_:   "rec sh lev n L1 p1 f c k k' q (s,H) (s',H')"

lemma rec_elim:
  assumes "rec sh lev n (set \<Gamma>1) p1 f c1 k k' q (s1,H1) (s1',H1')"
  obtains
    (stop) \<Gamma>' s2 H2 s2' H2' where
      "k = Stop (set \<Gamma>') s2 H2"
      "k' = Stop (set \<Gamma>') s2' H2'"
      "inv sh lev \<Gamma>' q f (s2,H2) (s2',H2')"
      "step sh lev c1 \<Gamma>1 p1 \<Gamma>' q s1 s2" |
    (rec) p2 \<Gamma>2 c2 s2 H2 s2' H2' where
      "k = Run c2 (set \<Gamma>2) s2 H2"
      "k' = Run c2 (set \<Gamma>2) s2' H2'"
      "inv sh lev \<Gamma>2 p2 f (s2,H2) (s2',H2')"
      "step sh lev c1 \<Gamma>1 p1 \<Gamma>2 p2 s1 s2"
      "secure sh lev n p2 c2 q"
proof -
  note stop = that(1)
  note rec = that(2)

  then show thesis proof (cases rule: rec_elim_[OF assms])
    case (1 \<Gamma>' s2 H2 s2' H2' \<Gamma>1)
    with inv_reorder[OF 1(1)]  step_reorder[OF 1(1)]
    show ?thesis by (auto intro!: stop)
  next
    case (2 \<Gamma>2 p2 s2 H2 s2' H2' \<Gamma>1 c2)
    with inv_reorder[OF 2(1)] step_reorder[OF 2(1)]
    show ?thesis by (auto intro!: rec)
  qed
qed

lemma step_pure_dest:
  assumes "step sh lev c1 \<Gamma>1 (Star (Rel (Pure b)) p1) \<Gamma>' q s1 s2"
  assumes "expr_free b \<subseteq> assert_free p1"
  shows   "step sh lev c1 \<Gamma>1 p1 \<Gamma>' q s1 s2"
    using assms by (elim step_elim, intro step_intro) auto

lemma step_rel_dest:
  assumes "step sh lev c1 \<Gamma>1 (Star (Rel r) p1) \<Gamma>' q s1 s2"
  assumes "rexpr_free r \<subseteq> assert_free p1"
  shows   "step sh lev c1 \<Gamma>1 p1 \<Gamma>' q s1 s2"
    using assms by (elim step_elim, intro step_intro) auto

lemma rec_pure_dest:
  assumes "rec sh lev n (set \<Gamma>1) (Star (Rel (Pure b)) p1) f c1 k k' q (s1,H1) (s1',H1')"
  assumes "expr_free b \<subseteq> assert_free p1"
  shows   "rec sh lev n (set \<Gamma>1) p1 f c1 k k' q (s1,H1) (s1',H1') \<and> expr_free b \<subseteq> assert_free p1"
  using assms by (auto elim: rec_elim dest: step_pure_dest)

lemma rec_rel_dest:
  assumes "rec sh lev n (set \<Gamma>1) (Star (Rel r) p1) f c1 k k' q (s1,H1) (s1',H1')"
  assumes "rexpr_free r \<subseteq> assert_free p1"
  shows   "rec sh lev n (set \<Gamma>1) p1 f c1 k k' q (s1,H1) (s1',H1') \<and> rexpr_free r \<subseteq> assert_free p1"
  using assms by (auto elim: rec_elim dest: step_rel_dest)

inductive_cases secure_elim: "secure sh lev (Suc n) p1 c1 q"

lemma secure_step:
  assumes "secure sh lev (Suc n) p1 c1 q"
  assumes "pre sh lev (set \<Gamma>1) p1 f c1 k k' (s1,H1) (s1',H1')"
  shows   "rec sh lev n (set \<Gamma>1) p1 f c1 k k' q (s1,H1) (s1',H1')"
  using assms by (auto elim!: secure_elim)

lemma cmd_coincidences:
  assumes "coincide s s' (- cmd_mod c)"
  assumes "cmd_mod c \<inter> assert_free p = {}"
  obtains "coincide s s' (assert_free p)"
  using assms by blast

lemma secure_prefix:
  assumes "secure sh lev n p c q"
  assumes "m \<le> n"
  shows   "secure sh lev m p c q"
  using assms proof (induction m arbitrary: n p c q)
  case 0
  then show ?case
    by simp
next
  case (Suc m0)

  from Suc(3) obtain n0 where
    n0: "n = Suc n0" by (cases n) simp
  with Suc(2) have
    sec: "secure sh lev (Suc n0) p c q" by simp

  show ?case proof (rule secure_intro, goal_cases)
    case (1 \<Gamma>1 s1 s1' H1 H1' k k' f)
    from secure_step[OF sec this]
    show ?case using Suc(1) Suc(3) n0 by (auto elim!: rec_elim)
  qed
qed

lemma secure_skip:
  shows "secure sh lev n Emp Skip Emp"
  by (auto elim!: pre_elim intro!: secure_intro' step_intro)
lemma secure_assign:
  assumes
    "x \<notin> expr_free e"
    "x \<notin> invvars"
  shows
    "secure sh lev n Emp (Assign x e) (Rel (Pure (Eq (Var x) e)))"
  using assms by (auto simp: inv_def invvars_def elim!: pre_elim star_elim intro!: secure_intro' step_intro star_intro)

lemma secure_load:
  assumes
    "x \<notin> expr_frees [p,e,l]"
    "x \<notin> invvars"
  shows
    "secure sh lev n (Pto p l e) (Load x p) (Star (Rel (Pure (Eq (Var x) e))) (Pto p l e))"
  using assms
  apply (auto elim!: pre_elim intro!: secure_intro' step_intro)
  apply (auto simp: inv_def)
  by (auto intro!: star_intro elim!: star_elim)

lemma secure_store:
  shows
    "secure sh lev n (Star (Rel (Sec e' l)) (Pto p l e)) (Store p e') (Star (Rel (Sec e l)) (Pto p l e'))"
  by (auto simp: inv_def elim!: pre_elim intro!: secure_intro' step_intro pto_frame_write)

lemma shared_atomic_pre [simp]:
  "pre True lev (set \<Gamma>1) P f c k k' (s1, H1) (s1', H1') \<Longrightarrow>
   pre False lev (set \<Gamma>1) (Star P shared) f c k k' (s1, H1) (s1', H1')"
  apply(erule pre_elim)
  apply(rule pre_intro, simp_all add: shared_wf)
  by(simp add: inv_def star_ac star_com star_ass)

lemma shared_atomic_inv [simp]:
  "local.inv False lev \<Gamma>' (Star Q shared) f (s2, H2) (s2', H2') \<Longrightarrow>
   local.inv True lev \<Gamma>' Q f (s2, H2) (s2', H2')"
  by(simp add: inv_def star_ac star_com star_ass)


lemma shared_atomic_step [simp]:
  "step False lev c \<Gamma>1 (Star P shared) \<Gamma>' (Star Q shared) s1 s2 \<Longrightarrow> 
   step True lev c \<Gamma>1 P \<Gamma>' Q s1 s2"
  apply(erule step_elim)
  apply(rule step_intro)
  using shared_free by auto
   
lemma secure_shared_atomic:
  assumes
    "atomic c"
    "secure False lev n (Star P shared) c (Star Q shared)"
  shows
    "secure True lev n P c Q"
proof(rule secure_intro')
  fix n' \<Gamma>1 s1 s1' H1 H1' k k' f
  assume n_def: "n = Suc n'" and
         pre: "pre True lev (set \<Gamma>1) P f c k k' (s1, H1) (s1', H1')"

  have "n' < n" using n_def by blast

  with pre shared_atomic_pre secure_elim n_def "assms"(2) have 
  "rec False lev n' (set \<Gamma>1) (Star P shared) f c k k' (Star Q shared) (s1, H1) (s1', H1')"
    by metis
  thus "rec True lev n' (set \<Gamma>1) P f c k k' Q (s1, H1) (s1', H1')"
    apply(rule rec_elim)
     apply(fastforce intro: secure_stop)
    using pre_elim[OF pre]
    by (metis assms(1) atomic_is_atomic)
qed

lemma secure_lock:
  assumes "l \<in> locks"
  shows   "secure sh lev n Emp (Lock l) (lockinv l)"
proof (auto elim!: pre_elim intro!: secure_intro')
  fix n \<Gamma>1 s1 s1' H1 H1' f
  assume
    l: "l \<in> set \<Gamma>1"
  then obtain \<Gamma>2 where
    \<Gamma>2: "set \<Gamma>2 = set \<Gamma>1 - {l}"
    using remove_lock by force
  assume
    "inv sh lev \<Gamma>1 Emp f (s1, H1) (s1', H1')"
    "set \<Gamma>1 \<subseteq> locks"
  with l \<Gamma>2 assms have
    "rec sh lev n (set \<Gamma>1) Emp f (Lock l) (Stop (set \<Gamma>2) s1 H1) (Stop (set \<Gamma>2) s1' H1') (lockinv l) (s1, H1) (s1', H1')"
    apply (intro secure_stop)
    apply (simp add: inv_def lockinvs_sem_lock)
    apply (rule step_intro)
      apply simp_all
    subgoal by (rule lockinv_free)
      apply blast thm shared_wf
    using locks_wf by (auto simp: lockinvs_lows_lock lockinvs_lows_free_lock)
  with \<Gamma>2 show
    "rec sh lev n (set \<Gamma>1) Emp f (Lock l) (Stop (set \<Gamma>1 - {l}) s1 H1) (Stop (set \<Gamma>1 - {l}) s1' H1') (lockinv l) (s1, H1) (s1', H1')"
    by simp
qed

lemma secure_unlock:
  assumes "l \<in> locks"
  shows   "secure sh lev n (lockinv l) (Unlock l) Emp"
proof (auto elim!: pre_elim intro!: secure_intro')
  fix n \<Gamma>1 s1 s1' H1 H1' f
  assume
    "inv sh lev \<Gamma>1 (lockinv l) f (s1, H1) (s1', H1')"
    "l \<notin> set \<Gamma>1"
    "set \<Gamma>1 \<subseteq> locks"
  with assms have
    "rec sh lev n (set \<Gamma>1) (lockinv l) f (Unlock l) (Stop (set (l#\<Gamma>1)) s1 H1) (Stop (set (l#\<Gamma>1)) s1' H1') Emp (s1, H1) (s1', H1')"
    apply (intro secure_stop)
    using lockinvs_wf by (auto simp: inv_def intro!: step_intro)     (* do the intro before simplifying set (l#\<Gamma>1) *)
  then show "rec sh lev n (set \<Gamma>1) (lockinv l) f (Unlock l) (Stop (insert l (set \<Gamma>1)) s1 H1) (Stop (insert l (set \<Gamma>1)) s1' H1') Emp (s1, H1) (s1', H1')"
    by simp
qed

lemma inv_conseq:
  assumes "inv sh lev \<Gamma> p f (s,H) (s',H')"
  assumes "follows lev p p'"
  shows   "inv sh lev \<Gamma> p' f (s,H) (s',H')"
  using assms by (simp add: inv_def follows_frame)

lemma pre_conseq:
  assumes "pre sh lev (set \<Gamma>) p f c k k' (s, H1) (s', H1')"
  assumes "follows lev p p'"
  shows   "pre sh lev (set \<Gamma>) p' f c k k' (s, H1) (s', H1')"
  using assms by (auto elim!: pre_elim intro!: pre_intro simp: inv_conseq follows_assert_wf)

lemma step_conseq:
  assumes c: "step sh lev c \<Gamma> p' \<Gamma>' q' s s'"
  assumes p: "follows lev p p'"
  assumes q: "follows lev q' q"
shows   "step sh lev c \<Gamma> p \<Gamma>' q s s'"
  using c
    follows_assert_lows_subset[OF p]
    follows_assert_lows_subset[OF q]
    follows_assert_free_subset[OF p]
    follows_assert_free_subset[OF q]
    follows_assert_wf[OF p]
    follows_assert_wf[OF q]
    follows_lows_free_subset[OF p]
    follows_lows_free_subset[OF q]
  apply (clarsimp elim!: step_elim)   (* FFS *)
  apply (rule step_intro)
     apply blast
    apply blast
  apply (clarsimp | safe)+
  apply (smt UnE subsetCE subsetI sup.cobounded2 sup_commute)
      apply (smt UnE subsetCE subsetI sup.cobounded2 sup_commute)
  apply (smt UnE subsetCE subsetI sup.cobounded2 sup_commute)
  apply (smt DiffI Diff_insert_absorb Diff_subset_conv assert_lows.simps(1) subset_Diff_insert sup_bot.right_neutral sup_commute)
   apply (smt Diff_insert_absorb UnE assert_lows.simps(1) subset_Diff_insert sup_bot.right_neutral sup_commute)
  by(auto split del: if_splits)


lemma secure_conseq_pre:
  assumes "secure sh lev n p' c q"
  assumes "follows lev p p'"
  shows   "secure sh lev n p c q"
  using assms by (auto dest!: secure_step elim!: rec_elim intro: step_conseq pre_conseq intro!: secure_intro')

lemma secure_conseq_post:
  assumes "secure sh lev n p c q'"
  assumes "follows lev q' q"
  shows   "secure sh lev n p c q"
  using assms
  apply (induction n arbitrary: c p q rule: nat_less_induct)
  apply (auto intro!: secure_intro' dest!: secure_step)
  apply (elim rec_elim)
  by (auto intro: inv_conseq step_conseq)

lemma secure_conseq:
  assumes "secure sh lev n p' c q'"
  assumes "follows lev p p'"
  assumes "follows lev q' q"
  shows   "secure sh lev n p c q"
  using assms secure_conseq_pre secure_conseq_post by blast

lemma secure_split:
  assumes c1: "secure sh lev n (Star (Rel (Pure b)) p) c q"
  assumes c2: "secure sh lev n (Star (Rel (Pure (Not b))) p) c q"
  assumes b:  "expr_free b \<subseteq> assert_free p"
  shows       "secure sh lev n (Star (Rel (Sec b (level_expr lev))) p) c q"
proof (rule secure_intro', goal_cases)
  case (1 m \<Gamma> s1 s1' H1 H1' k k' f)
  note m[simp] = 1(1)
  note pre = 1(2)
  from pre have bs: "pure_sem b s1 = pure_sem b s1'"
    by (auto simp: pure_sem_def elim: pre_elim)
  show ?case proof (cases "pure_sem b s1")
    case True
    have p': "follows lev (Star (Rel (Pure b)) (Star (Rel (Sec b (level_expr lev))) p)) (Star (Rel (Pure b)) p)"
      by (auto simp: follows_def implies_def)
    have pre1: "pre sh lev (set \<Gamma>) (Star (Rel (Pure b)) (Star (Rel (Sec b (level_expr lev))) p)) f c k k' (s1, H1) (s1', H1')"
      using pre bs True by simp
    note c1' = secure_conseq_pre[OF c1 p', simplified]
    with secure_step[OF c1' pre1] show ?thesis
      by (auto dest: rec_pure_dest)
  next
    case False
    have p': "follows lev (Star (Rel (Pure (Not b))) (Star (Rel (Sec b (level_expr lev))) p)) (Star (Rel (Pure (Not b))) p)"
      by (auto simp: follows_def implies_def)
    have pre1: "pre sh lev (set \<Gamma>) (Star (Rel (Pure (Not b))) (Star (Rel (Sec b (level_expr lev))) p)) f c k k' (s1, H1) (s1', H1')"
      using pre bs False by simp
    note c2' = secure_conseq_pre[OF c2 p', simplified]
    with secure_step[OF c2' pre1] show ?thesis
      by (auto dest: rec_pure_dest)
  qed
qed

lemma secure_rsplit:
  assumes c1: "secure sh lev n (Star (Rel r) p) c q"
  assumes c2: "secure sh lev n (Star (Rel (RNot r)) p) c q"
  assumes b:  "rexpr_free r \<subseteq> assert_free p"
  shows       "secure sh lev n p c q"
proof (rule secure_intro', goal_cases)
  case (1 m \<Gamma> s1 s1' H1 H1' k k' f)
  note m[simp] = 1(1)
  note pre = 1(2)
  show ?case proof (cases "rexpr_sem lev r s1 s1'")
    case True
    have pre1: "pre sh lev (set \<Gamma>) (Star (Rel r) p) f c k k' (s1, H1) (s1', H1')"
      using pre pre_rel True by simp
    with secure_step[OF c1[simplified] pre1] b show ?thesis
      by (blast dest!: rec_rel_dest)
  next
    case False
    have pre1: "pre sh lev (set \<Gamma>) (Star (Rel (RNot r)) p) f c k k' (s1, H1) (s1', H1')"
      using pre pre_rel False by simp 
    with secure_step[OF c2[simplified] pre1] b show ?thesis
      using rec_rel_dest by auto
  qed
qed

lemma secure_if:
  assumes c1: "secure sh lev n (Star (Rel (Pure b)) p) c1 q"
  assumes c2: "secure sh lev n (Star (Rel (Pure (Not b))) p) c2 q"
  assumes b:  "expr_free b \<subseteq> assert_free p"
  shows       "secure sh lev (Suc n) (Star (Rel (Sec b (level_expr lev))) p) (If b c1 c2) q"
proof (rule secure_intro)
  fix \<Gamma> s1 s1' H1 H1' k k' f
  assume "pre sh lev (set \<Gamma>) (Star (Rel (Sec b (level_expr lev))) p) f (cmd.If b c1 c2) k k' (s1, H1) (s1', H1')"
  then show "rec sh lev n (set \<Gamma>) (Star (Rel (Sec b (level_expr lev))) p) f (cmd.If b c1 c2) k k' q (s1, H1) (s1', H1')"
  proof (cases rule: pre_elim)
    case (pre \<sigma>)
    from pre(8) have
      bs: "pure_sem b s1 = pure_sem b s1'"
      unfolding pure_sem_def by simp
    (* This whole thing is a bit brittle to avoid picking auto
       to instantiate the successor assertion from the plain invariant pre(4).
       Instead we're explicitly spelling out the strong invariant
       including the conditional evaluation *and* the security classification,
       and then get rid of the second by consequence
       but *keeping* the evaluation of the conditional to match the hypothesis c1/c2.
     *)
    show ?thesis proof (cases "pure_sem b s1")
      case True
      from True pre(8) bs have
        inv: "inv sh lev \<Gamma> (Star (Rel (Pure b)) (Star (Rel (Sec b (level_expr lev))) p)) f (s1, H1) (s1', H1')"
        by simp
      have "follows lev (Star (Rel (Pure b)) (Star (Rel (Sec b (level_expr lev))) p)) (Star (Rel (Pure b)) p)"
        by (auto simp: follows_def implies_def)
      with True pre(1-7) b bs c1 show ?thesis
        by (auto intro: inv step_intro secure_conseq_pre)
    next
      case False
      from False pre(8) bs have
        inv: "inv sh lev \<Gamma> (Star (Rel (Pure (Not b))) (Star (Rel (Sec b (level_expr lev))) p)) f (s1, H1) (s1', H1')"
        by simp
      have "follows lev (Star (Rel (Pure (Not b))) (Star (Rel (Sec b (level_expr lev))) p)) (Star (Rel (Pure (Not b))) p)"
        by (auto simp: follows_def implies_def)
      with False pre(1-7) b bs c2 show ?thesis 
        by (auto intro: inv step_intro secure_conseq_pre)
    qed
  qed
qed

lemma secure_if':
  assumes c1: "\<And> n. secure sh lev n (Star (Rel (Pure b)) p) c1 q"
  assumes c2: "\<And> n. secure sh lev n (Star (Rel (Pure (Not b))) p) c2 q"
  assumes b:  "expr_free b \<subseteq> assert_free p"
  shows       "secure sh lev n (Star (Rel (Sec b (level_expr lev))) p) (If b c1 c2) q"
  using assms secure_if by (cases n, auto)

lemma inv_frame_fst:
"inv sh lev \<Gamma>' p (Star q f) (s2, H2) (s2', H2')
  = inv sh lev \<Gamma>' (Star p q) f (s2, H2) (s2', H2')"
  unfolding inv_def by (simp add: star_com23_)

lemma inv_frame_snd:
"inv sh lev \<Gamma>' q (Star p f) (s2, H2) (s2', H2') = inv sh lev \<Gamma>' (Star p q) f (s2, H2) (s2', H2')"
  unfolding inv_def by (simp add: star_com23_ star_com12_)

lemma pre_frame:
  assumes pre: "pre sh lev (set \<Gamma>) (Star p1 p2) f c k k' (s1, H1) (s1', H1')"
  assumes p2:  "cmd_mod c \<inter> assert_free p2 = {}"
  shows "pre sh lev (set \<Gamma>) p1 (Star p2 f) c k k' (s1, H1) (s1', H1')"
  using assms by (auto elim!: pre_elim intro!: pre_intro simp: Int_Un_distrib inv_frame_fst)  

lemma pre_par:
  assumes pre: "pre sh lev (set \<Gamma>) (Star p1 p2) f (Par c1 c2) k k' (s1, H1) (s1', H1')"
  assumes p1:  "cmd_mod c2 \<inter> assert_free p1 = {}"
  assumes p2:  "cmd_mod c1 \<inter> assert_free p2 = {}"
  obtains
    (fst) k1 k1' where
      "k  = (par Fst c1 c2 k1)" 
      "k' = (par Fst c1 c2 k1')"
      "pre sh lev (set \<Gamma>) p1 (Star p2 f) c1 k1 k1' (s1, H1) (s1', H1')" |
    (snd) k2 k2' where
      "k  = (par Snd c1 c2 k2)" 
      "k' = (par Snd c1 c2 k2')"
      "pre sh lev (set \<Gamma>) p2 (Star p1 f) c2 k2 k2' (s1, H1) (s1', H1')"
using pre proof (cases rule: pre_elim)
  case (pre \<sigma>)
  from pre(6) show ?thesis proof (cases rule: step_par_split)
    case (fst \<sigma>' k1)
    from fst pre(1-5) pre(7-8) p2 show ?thesis (* don't add pre(4) or the last auto loops in simp *)
      by (auto elim!: step_par_fst intro!: that(1) pre_intro simp: Int_Un_distrib inv_frame_fst)
  next
    case (snd \<sigma>' k2)
    from snd pre(1-5) pre(7-8) p1 show ?thesis (* don't add pre(4) or the last auto loops in simp *)
      by (auto elim!: step_par_snd intro!: that(2) pre_intro simp: Int_Un_distrib inv_frame_snd)
  qed
qed

lemma step_par_fst':
  assumes "step sh lev c1 \<Gamma> p \<Gamma>' p' s1 s2"
  assumes "assert_lows lev q s1 \<subseteq> assert_lows lev q s2"
  assumes "assert_wf q {}"
  shows   "step sh lev (Par c1 c2) \<Gamma> (Star p q) \<Gamma>' (Star p' q) s1 s2"
  using assms by (auto intro!: step_intro elim!: step_elim split: if_splits)

lemma step_par_snd':
  assumes "step sh lev c2 \<Gamma> q \<Gamma>' q' s1 s2"
  assumes "assert_lows lev p s1 \<subseteq> assert_lows lev p s2"
  assumes "assert_wf p {}"
  shows   "step sh lev (Par c1 c2) \<Gamma> (Star p q) \<Gamma>' (Star p q') s1 s2"
  using assms by (auto intro!: step_intro elim!: step_elim split: if_splits)

lemma step_frame:
  assumes "step sh lev c \<Gamma> p \<Gamma>' p' s1 s2"
  assumes "assert_lows lev q s1 \<subseteq> assert_lows lev q s2"
  assumes "assert_wf q {}"
  shows   "step sh lev c \<Gamma> (Star p q) \<Gamma>' (Star p' q) s1 s2"
  using assms by (auto intro!: step_intro elim!: step_elim split: if_splits)

lemma secure_seq:
  assumes c1: "secure sh lev n p c1 q"
  (* the additional assumption m \<le> is useful in the while rule *)
  assumes c2: "\<And> m. m \<le> n \<Longrightarrow> secure sh lev m q c2 r"
  shows   "secure sh lev n p (Seq c1 c2) r"
using assms proof (induction n arbitrary: c1 p, simp)
  case (Suc n)
  show ?case proof (intro secure_intro)
    fix \<Gamma>1 s1 s1' H1 H1' k k' f
    assume
      "pre sh lev (set \<Gamma>1) p f (Seq c1 c2) k k' (s1, H1) (s1', H1')"
    then obtain \<sigma> k1 k1' where
       k: "k = seq k1 c2" "k' = seq k1' c2" and
       "cmd_mod (Seq c1 c2) \<inter> assert_free f = {}"
       "cmd_mod (Seq c1 c2) \<inter> invvars = {}" "assert_wf p {}" "assert_wf f {}"
       "set \<Gamma>1 \<subseteq> locks" "cmd_step \<sigma> (Run c1 (set \<Gamma>1) s1 H1) k1" "cmd_step \<sigma> (Run c1 (set \<Gamma>1) s1' H1') k1'" "local.inv sh lev \<Gamma>1 p f (s1, H1) (s1', H1')"
      by (auto elim: pre_elim dest!: step_seq)
    then have
      "pre sh lev (set \<Gamma>1) p f c1 k1 k1' (s1, H1) (s1', H1')"
      by (intro pre_intro)
         (simp add: Int_Un_distrib2)+
    from secure_step[OF Suc.prems(1) this]
    show "rec sh lev n (set \<Gamma>1) p f (Seq c1 c2) k k' r (s1, H1) (s1', H1')" proof (cases rule: rec_elim)
      case (stop \<Gamma>' s2 H2 s2' H2')
      with k show ?thesis
        by (auto elim!: step_elim intro!: step_intro Suc.prems(2) split: if_splits)
    next
      case (rec p2 \<Gamma>2 c2 s2 H2 s2' H2')
      with k show ?thesis 
        by (auto elim!: step_elim intro!: step_intro Suc.IH Suc.prems(2) split: if_splits)
    qed
  qed
qed

lemma secure_while:
  assumes body: "\<And> n. secure sh lev n (Star (Rel (Pure b)) (Star (Rel (Sec b (level_expr lev))) p)) c (Star (Rel (Sec b (level_expr lev))) p)"
  shows   "secure sh lev k (Star (Rel (Sec b (level_expr lev))) p) (While b c) (Star (Rel (Pure (Syntax.Not b))) p)"
proof (induction k rule: nat_less_induct, rule secure_intro')
  fix n m \<Gamma>1 s1 s1' H1 H1' k k' f
  assume
    m: "n = Suc m"
  assume
    "\<forall>m<n. secure sh lev m (Star (Rel (Sec b (level_expr lev))) p) (While b c) (Star (Rel (Pure (Syntax.Not b))) p)"
  with m have
    hyp: "\<And> k. k\<le>m \<Longrightarrow> secure sh lev k (Star (Rel (Sec b (level_expr lev))) p) (While b c) (Star (Rel (Pure (Syntax.Not b))) p)"
    by auto
  assume
    "pre sh lev (set \<Gamma>1) (Star (Rel (Sec b (level_expr lev))) p) f (While b c) k k' (s1, H1) (s1', H1')"
  then show
    "rec sh lev m (set \<Gamma>1) (Star (Rel (Sec b (level_expr lev))) p) f (While b c) k k' (Star (Rel (Pure (Syntax.Not b))) p) (s1, H1) (s1', H1')"
  proof (cases rule: pre_elim)
    case (pre \<sigma>)
    from pre(8) have
      bs: "pure_sem b s1 = pure_sem b s1'"
      unfolding pure_sem_def by simp

    show ?thesis proof (cases "pure_sem b s1")
      case True
      have inv: "inv sh lev \<Gamma>1 (Star (Rel (Pure b)) (Star (Rel (Sec b (level_expr lev))) p)) f (s1, H1) (s1', H1')"
        using True pre(8) bs by simp
      from True pre(1-7) bs show ?thesis
        thm secure_seq
        apply (auto dest!: While_unwind)
        apply (rule inv)
        by (auto intro!: step_intro secure_seq body hyp)
    next
      case False
      with pre bs show ?thesis
        by (auto dest!: While_exit intro: step_intro)
    qed
  qed
qed

lemma secure_frame:
  assumes
    "secure sh lev n p c q"
    "cmd_mod c \<inter> assert_free r = {}"
  shows "secure sh lev n (Star p r) c (Star q r)"
using assms proof (induction n arbitrary: c p q rule: nat_less_induct)
  case (1 m)
  then have hyp:
    "\<And> n p c q.
      \<lbrakk>n < m;
      secure sh lev n p c q;
      cmd_mod c \<inter> assert_free r = {}\<rbrakk>
      \<Longrightarrow> secure sh lev n (Star p r) c (Star q r)"
    by blast

  note sec = 1(2)
  note r = 1(3)

  show ?case proof (rule secure_intro')
    fix n \<Gamma>1 s1 s1' H1 H1' k k' f
    assume
      m: "m = Suc n"
    then have
      "n < m"  by simp
    note hyp = hyp[OF this]

    from m sec have
      sec: "secure sh lev (Suc n) p c q" by simp
    assume
      pre: "pre sh lev (set \<Gamma>1) (Star p r) f c k k' (s1, H1) (s1', H1')"

    note pre_frame[OF pre r]
    note rec = secure_step[OF sec this]

    show "rec sh lev n (set \<Gamma>1) (Star p r) f c k k' (Star q r) (s1, H1) (s1', H1')"
    proof (cases rule: rec_elim[OF rec])
      case (1 s2 H2 s2' H2')
      with pre r show ?thesis 
        by (auto simp: inv_frame_fst intro!: step_frame elim!: pre_elim_stop elim: cmd_coincidences)
    next
      case (2 p2 \<Gamma>2 c2 s2 H2 s2' H2')
      with pre r show ?thesis
        by (auto simp: inv_frame_fst intro!: hyp step_frame elim!: pre_elim_run elim: cmd_coincidences)
    qed
  qed
qed

lemma secure_frame_com:
"secure sh lev n (Star p1 p2) c (Star q1 q2)
  = secure sh lev n (Star p2 p1) c (Star q2 q1)"
  by (auto intro: secure_conseq)

lemma secure_par:
  assumes
    "secure sh lev k p1 c1 q1"
    "secure sh lev k p2 c2 q2"
    "cmd_mod c1 \<inter> cmd_vars c2 = {}"
    "cmd_mod c2 \<inter> cmd_vars c1 = {}"
    "cmd_mod c1 \<inter> assert_free p2 = {}"
    "cmd_mod c1 \<inter> assert_free q2 = {}"
    "cmd_mod c2 \<inter> assert_free p1 = {}"
    "cmd_mod c2 \<inter> assert_free q1 = {}"
  shows "secure sh lev k (Star p1 p2) (Par c1 c2) (Star q1 q2)"
using assms proof (induction k arbitrary: c1 p1 c2 p2 rule: nat_less_induct)
  case (1 n)
  note sec1 = 1(2)
  note sec2 = 1(3)
  note mod = 1(4-)

  from 1(1) have hyp:
    "\<And> m c1 p1 c2 p2.
      m < n \<Longrightarrow>
      \<lbrakk>secure sh lev m p1 c1 q1;
       secure sh lev m p2 c2 q2;
       cmd_mod c1 \<inter> cmd_vars c2 = {};
       cmd_mod c2 \<inter> cmd_vars c1 = {};
       cmd_mod c1 \<inter> assert_free p2 = {};
       cmd_mod c1 \<inter> assert_free q2 = {};
       cmd_mod c2 \<inter> assert_free p1 = {};
       cmd_mod c2 \<inter> assert_free q1 = {}\<rbrakk>
    \<Longrightarrow> secure sh lev m (Star p1 p2) (Par c1 c2)(Star q1 q2)"
    by auto

  show ?case proof (rule secure_intro')
    fix m \<Gamma>1 s1 s1' H1 H1' k k' f

    assume
      m: "n = Suc m"
    then have "m < n" by simp
    note hyp = hyp[OF this]

    from m sec1 sec2 have
      sec1: "secure sh lev (Suc m) p1 c1 q1" and
      sec2: "secure sh lev (Suc m) p2 c2 q2"
      by simp_all

    assume
      pre: "pre sh lev (set \<Gamma>1) (Star p1 p2) f (Par c1 c2) k k' (s1, H1) (s1', H1')"

    from pre have
      invvars: "cmd_mod c1 \<inter> invvars = {}" "cmd_mod c2 \<inter> invvars = {}"
      by (elim pre_elim, simp add: Int_Un_distrib2)+

    from pre mod(5) mod(3) show
      "rec sh lev m (set \<Gamma>1) (Star p1 p2) f (Par c1 c2) k k' (Star q1 q2) (s1, H1) (s1', H1')"

    proof (cases rule: pre_par)
      case (fst k1 k1')
      note pre1 = fst(3)
      note rec1 = secure_step[OF sec1 pre1]

      then show ?thesis proof (cases rule: rec_elim)
        case (stop s2 H2 s2' H2')
       have
          "secure sh lev m p2 c2 q2"
          by (rule secure_prefix, rule sec2) simp
        from this mod(6) have
          "secure sh lev m (Star q1 p2) c2 (Star q1 q2)"
          unfolding secure_frame_com by (rule secure_frame)
        with stop fst mod(3) show ?thesis
          by (auto simp: inv_frame_fst Int_Un_distrib elim: cmd_coincidences intro!: step_par_fst' elim!: pre_elim_stop)
      next
        case (rec p1' \<Gamma>2 c1' s2 H2 s2' H2')
        have
          "secure sh lev m p2 c2 q2"
          by (rule secure_prefix, rule sec2) simp
        with rec fst mod show ?thesis
          apply (auto simp: inv_frame_fst Int_Un_distrib elim: cmd_coincidences intro!: step_par_fst' elim!: pre_elim_run)
        proof (rule hyp, simp_all)
          assume
            "cmd_mod c1' \<subseteq> cmd_mod c1"
            "cmd_vars c1' \<subseteq> cmd_vars c1"
          with mod show
            "cmd_mod c1' \<inter> cmd_vars c2  = {}" 
            "cmd_mod c2  \<inter> cmd_vars c1' = {}"
            "cmd_mod c1' \<inter> assert_free p2 = {}"
            "cmd_mod c1' \<inter> assert_free q2 = {}"
            by auto
        next
          assume
            "step sh lev c1 \<Gamma>1 p1 \<Gamma>2 p1' s1 s2"
          then have
            p1': "assert_free p1' \<subseteq> assert_free p1 \<union> cmd_vars c1 \<union> invvars"
            by (auto elim!: step_elim)
          assume
            "cmd_mod c1' \<subseteq> cmd_mod c1"
            "cmd_vars c1' \<subseteq> cmd_vars c1"
          with p1' mod invvars show "cmd_mod c2 \<inter> assert_free p1' = {}"
            by auto
        qed
      qed
    next
      case (snd k2 k2')
      note pre2 = snd(3)
      note rec2 = secure_step[OF sec2 pre2]

      then show ?thesis proof (cases rule: rec_elim)
        case (stop s2 H2 s2' H2')
        have
          "secure sh lev m p1 c1 q1"
          by (rule secure_prefix, rule sec1) simp
        note secure_frame[OF this mod(4)]
        with stop snd mod(5) show ?thesis
          by (auto simp: inv_frame_snd Int_Un_distrib elim: cmd_coincidences intro!: step_par_snd' elim!: pre_elim_stop)
      next
        case (rec p2' \<Gamma>2 c2' s2 H2 s2' H2')
        have
          "secure sh lev m p1 c1 q1"
          by (rule secure_prefix, rule sec1) simp
        with rec snd mod show ?thesis
          apply (auto simp: inv_frame_snd Int_Un_distrib elim: cmd_coincidences intro!: step_par_snd' elim!: pre_elim_run)
        proof (rule hyp, simp_all)
          assume
            "cmd_mod c2' \<subseteq> cmd_mod c2"
            "cmd_vars c2' \<subseteq> cmd_vars c2"
          with mod show
            "cmd_mod c1  \<inter> cmd_vars c2' = {}" 
            "cmd_mod c2' \<inter> cmd_vars c1  = {}"
            "cmd_mod c2' \<inter> assert_free p1 = {}"
            "cmd_mod c2' \<inter> assert_free q1 = {}"
            by auto
        next
          assume
            "step sh lev c2 \<Gamma>1 p2 \<Gamma>2 p2' s1 s2"
          then have
            p2': "assert_free p2' \<subseteq> assert_free p2 \<union> cmd_vars c2 \<union> invvars"
            by (auto elim!: step_elim)
          assume
            "cmd_mod c2' \<subseteq> cmd_mod c2"
            "cmd_vars c2' \<subseteq> cmd_vars c2"
          with p2' mod invvars show "cmd_mod c1 \<inter> assert_free p2' = {}"
            by auto
        qed
      qed
    qed
  qed
qed

text {*
  when proving that we can eliminate exists in the precondition, we need
  to modify the frame temporarily to add some extra information to it.
*}
abbreviation mf :: "assert \<Rightarrow> var \<Rightarrow> var \<Rightarrow> assert" where
  "mf f x y \<equiv> (assert_ren f x y)"

lemma inv_exD:
  assumes inv: "local.inv sh lev \<Gamma>1 (assert.Ex x P) f (s, H) (s', H')"
  assumes notin: "x \<notin> invvars"
  assumes \<Gamma>1locks: "set \<Gamma>1 \<subseteq> locks"
  assumes ynotin: "y \<notin> invvars \<union> {x} \<union> assert_free P \<union> assert_vars f"
  obtains v v' where "local.inv sh lev \<Gamma>1 P (mf f x y) (s(x := v, y := (s x)), H) (s'(x := v', y := (s' x)), H')"
proof -
(*Hij: the ith part of the heap union the jth part of the heap*)
  from inv obtain H1 H234 H1' H234' where
    seps: "sep H H1 H234" "sep H' H1' H234'" and
    precond: "assert_sem lev (Ex x P) (s,H1) (s',H1')" and
    rest: "star (assert_sem lev (lockinvs \<Gamma>1)) (star (assert_sem lev (maybe_shared sh)) (assert_sem lev f))
           (s, H234) (s', H234')"
    by(auto simp: inv_def elim!: star_elim intro!: star_intro)
  from precond obtain v v' where
    precond: "assert_sem lev P ((s(x:=v,y:=(s x))),H1) ((s'(x:=v',y:=(s' x))),H1')"
    using ynotin by(auto elim!: ex_elim)   
  
  from rest obtain H2 H34 H2' H34' where
    seps2: "sep H234 H2 H34" "sep H234' H2' H34'" and
    locks: "assert_sem lev (lockinvs \<Gamma>1) (s,H2) (s',H2') " and
    rest: " star (assert_sem lev (maybe_shared sh)) (assert_sem lev f) (s, H34) (s', H34')" 
      by(auto elim: star_elim intro: star_intro)
    from locks have
    locks: "assert_sem lev (lockinvs \<Gamma>1) ((s(x:=v, y:=(s x))),H2) ((s'(x:=v',y:=(s' x))),H2')" 
      using notin \<Gamma>1locks invvars_def invs_def ynotin by simp

  from rest obtain H3 H4 H3' H4' where
    seps3: "sep H34 H3 H4" "sep H34' H3' H4'" and
    sh: "assert_sem lev (maybe_shared sh) (s,H3) (s',H3') " and
    rest: " (assert_sem lev f) (s, H4) (s', H4')"
      by(auto elim: star_elim intro: star_intro)
  from sh have
    sh: "assert_sem lev (maybe_shared sh) ((s(x:=v,y:=(s x))),H3) ((s'(x:=v',y:=(s' x))),H3')"
      using notin \<Gamma>1locks invvars_def invs_def ynotin by simp

    find_theorems assert_ren assert_sem
  from rest have
    f:"(assert_sem lev (mf f x y)) (s(x:=v,y:=(s x)), H4) (s'(x:=v',y:=(s' x)), H4')" 
    using ynotin assert_ren star_intro assert_ren_mod  fun_upd_twist 
    by (smt UnI2 assert_coincidence assert_free_vars assert_ren_triv subsetCE)

  have "local.inv sh lev \<Gamma>1 P (mf f x y) (s(x := v,y:=(s x)), H) (s'(x := v',y:=(s' x)), H')"
    (* prove precond *)
    apply(simp add: inv_def split del: if_splits)  (* split del to prevent case splitting on maybe_shared *)
    apply(rule star_intro)
    using seps apply blast
    using seps apply blast
     apply(rule precond)

    (* prove locks *)
    apply(rule star_intro)
    using seps2 apply blast
    using seps2 apply blast
     apply(rule locks)

    (* prove sh *)
    apply(rule star_intro)
    using seps3 apply blast
    using seps3 apply blast
     apply(rule sh)

    (*prove f*)
   using f by simp
  thus ?thesis using that by blast
qed

lemma inv_undo_mf:
  assumes inv: "local.inv sh lev \<Gamma>1 Q (mf f x y) (t1, H) (t1', H')"
  assumes notin: "x \<notin> invvars \<union> assert_free Q"
  assumes \<Gamma>1locks: "set \<Gamma>1 \<subseteq> locks"
  assumes s1_def: "s1 = t1(x := (t1 y),y := w)"
  assumes s1'_def: "s1' = t1'(x := (t1' y), y := w')"
  assumes ynotin: "y \<notin> invvars \<union> {x} \<union> assert_free Q \<union> assert_vars f"
  shows "local.inv sh lev \<Gamma>1 Q f (s1, H) (s1', H')"
proof -
(*Hij: the ith part of the heap union the jth part of the heap*)
  from inv obtain H1 H234 H1' H234' where
    seps: "sep H H1 H234" "sep H' H1' H234'" and
    postcond: "assert_sem lev Q (t1,H1) (t1',H1')" and
    rest: "star (assert_sem lev (lockinvs \<Gamma>1)) (star (assert_sem lev (maybe_shared sh)) (assert_sem lev (mf f x y)))
           (t1, H234) (t1', H234')"
    by(auto simp: inv_def elim!: star_elim intro!: star_intro)
  from postcond
  have postcond: "assert_sem lev Q (s1,H1) (s1',H1')"
    using notin s1_def s1'_def ynotin by simp 
  
  from rest obtain H2 H34 H2' H34' where
    seps2: "sep H234 H2 H34" "sep H234' H2' H34'" and
    locks: "assert_sem lev (lockinvs \<Gamma>1) (t1,H2) (t1',H2') " and
    rest: " star (assert_sem lev (maybe_shared sh)) (assert_sem lev (mf f x y)) (t1, H34) (t1', H34')" 
      by(auto elim: star_elim intro: star_intro)
    from locks have
    locks: "assert_sem lev (lockinvs \<Gamma>1) (s1,H2) (s1',H2')" 
      using notin \<Gamma>1locks invvars_def invs_def s1_def s1'_def ynotin by simp

  from rest obtain H3 H4 H3' H4' where
    seps3: "sep H34 H3 H4" "sep H34' H3' H4'" and
    sh: "assert_sem lev (maybe_shared sh) (t1,H3) (t1',H3') " and
    rest: " (assert_sem lev (mf f x y)) (t1, H4) (t1', H4')"
      by(auto elim: star_elim intro: star_intro)
  from sh have
    sh: "assert_sem lev (maybe_shared sh) (s1,H3) (s1',H3')"
      using notin \<Gamma>1locks invvars_def invs_def s1_def s1'_def ynotin by simp
  
  from rest have
    f:"(assert_sem lev f) (s1, H4) (s1', H4')"
    using ynotin assert_ren_undo assert_ren  s1_def s1'_def assert_ren_assert_vars 
    by (meson assert_coincidence assert_free_vars subset_iff sup.cobounded2)

  have "local.inv sh lev \<Gamma>1 Q f (s1, H) (s1', H')"
    (* prove postcond *)
    apply(simp add: inv_def split del: if_splits)  (* split del to prevent case splitting on maybe_shared *)
    apply(rule star_intro)
    using seps apply blast
    using seps apply blast
     apply(rule postcond)

    (* prove locks *)
    apply(rule star_intro)
    using seps2 apply blast
    using seps2 apply blast
     apply(rule locks)

    (* prove sh *)
    apply(rule star_intro)
    using seps3 apply blast
    using seps3 apply blast
     apply(rule sh)

    (*prove f*)
    apply(rule f)
    done

  thus ?thesis  by blast
qed

lemma inv_exI:
  assumes inv: "local.inv sh lev \<Gamma> p2 (mf f x y) (t1, H) (t1', H')" 
  assumes notin: "x \<notin> invvars"
  assumes \<Gamma>locks: "set \<Gamma> \<subseteq> locks" 
  assumes s1_def: "s1 = t1(x := (t1 y),y := w)"
  assumes s1'_def: "s1' = t1'(x := (t1' y), y := w')"
  assumes ynotin: "y \<notin> invvars \<union> {x} \<union> assert_free p2 \<union> assert_vars f"
  shows "local.inv sh lev \<Gamma> (assert.Ex x p2) f (s1,H) (s1',H')"
proof -
(*Hij: the ith part of the heap union the jth part of the heap*)
  from inv obtain H1 H234 H1' H234' where
    seps: "sep H H1 H234" "sep H' H1' H234'" and
    precond: "assert_sem lev p2 (t1,H1) (t1',H1')" and
    rest: "star (assert_sem lev (lockinvs \<Gamma>)) (star (assert_sem lev (maybe_shared sh)) (assert_sem lev (mf f x y)))
           (t1, H234) (t1', H234')"
    by(auto simp: inv_def elim!: star_elim intro!: star_intro)

  (* TODO clean this up *)
  have precond: "assert_sem lev (Ex x p2) (s1,H1) (s1',H1')"
    apply clarsimp
    apply(rule_tac v="t1 x" and v'="t1' x" in ex_intro)
    apply(simp add: s1_def s1'_def)
    using precond ynotin
    by (metis UnI1 UnI2 assert_coincidence fun_upd_triv fun_upd_twist)

  from rest obtain H2 H34 H2' H34' where
    seps2: "sep H234 H2 H34" "sep H234' H2' H34'" and
    locks: "assert_sem lev (lockinvs \<Gamma>) (t1,H2) (t1',H2') " and
    rest: " star (assert_sem lev (maybe_shared sh)) (assert_sem lev (mf f x y)) (t1, H34) (t1', H34')" 
      by(auto elim: star_elim intro: star_intro)
    from locks have
    locks: "assert_sem lev (lockinvs \<Gamma>) (s1,H2) (s1',H2')" 
      using notin \<Gamma>locks invvars_def invs_def s1_def s1'_def ynotin by simp

  from rest obtain H3 H4 H3' H4' where
    seps3: "sep H34 H3 H4" "sep H34' H3' H4'" and
    sh: "assert_sem lev (maybe_shared sh) (t1,H3) (t1',H3') " and
    rest: " (assert_sem lev (mf f x y)) (t1, H4) (t1', H4')"
      by(auto elim: star_elim intro: star_intro)
  from sh have
    sh: "assert_sem lev (maybe_shared sh) (s1,H3) (s1',H3')"
      using notin \<Gamma>locks invvars_def invs_def s1_def s1'_def ynotin by simp
  
  from rest have
    f:"(assert_sem lev f) (s1, H4) (s1', H4')"
    using ynotin 
     assert_ren_undo assert_ren  s1_def s1'_def assert_ren_assert_vars 
    by (meson assert_coincidence assert_free_vars subset_iff sup.cobounded2)

  have "local.inv sh lev \<Gamma> (Ex x p2) f (s1, H) (s1', H')"
    (* prove postcond *)
    apply(simp add: inv_def split del: if_splits)  (* split del to prevent case splitting on maybe_shared *)
    apply(rule star_intro)
    using seps apply blast
    using seps apply blast
    using precond apply simp

    (* prove locks *)
    apply(rule star_intro)
    using seps2 apply blast
    using seps2 apply blast
     apply(rule locks)

    (* prove sh *)
    apply(rule star_intro)
    using seps3 apply blast
    using seps3 apply blast
     apply(rule sh)

    (*prove f*)
    apply(rule f)
    done
  thus ?thesis  by blast
qed

(* FIXME: move to Semantics or somewhere *)
lemma assert_free_ex_subset:
  "assert_free (Ex x f) \<subseteq> assert_free f"
  by auto

lemma assert_ren_wf':
  "y \<notin> assert_vars p \<Longrightarrow> y \<notin> vs \<Longrightarrow> assert_wf p vs \<Longrightarrow>
   assert_wf (assert_ren p x y) (vs \<union> (if x \<in> vs then {y} else {}))"
  apply(induct p x y arbitrary: vs rule: assert_ren.induct)
  apply (auto) (* FIXME: beer offence *)
     apply(fastforce split: if_splits simp: insert_absorb insert_commute insert_iff)
    apply(fastforce split: if_splits simp: insert_absorb insert_commute insert_iff dest: assert_wf_mono subsetD)
proof -
  fix z :: "char list" and pa :: assert and ya :: "char list" and vsa :: "char list set"
  assume a1: "\<And>vs. \<lbrakk>ya \<notin> vs; assert_wf pa vs\<rbrakk> \<Longrightarrow> assert_wf (assert_ren pa z ya) (vs \<union> (if z \<in> vs then {ya} else {}))"
  assume "z \<notin> vsa"
  assume a2: "assert_wf pa (insert z vsa)"
  assume a3: "ya \<notin> vsa"
  assume "ya \<noteq> z"
  moreover
  { assume "ya \<notin> insert z vsa \<and> vsa \<subseteq> insert ya (insert z vsa)"
    then have "\<exists>C. (ya \<notin> C \<and> assert_wf pa C) \<and> insert ya vsa \<subseteq> C \<union> (if z \<in> C then {ya} else {})"
      using a2 by fastforce
    then have "assert_wf (assert_ren pa z ya) (insert ya vsa)"
      using a1 by (meson assert_wf_mono) }
  ultimately show "assert_wf (assert_ren pa z ya) (insert ya vsa)"
    using a3 by fastforce
next
  fix z p x y vs
  assume "\<And>vs. \<lbrakk>y \<notin> vs; assert_wf p vs\<rbrakk> \<Longrightarrow> assert_wf (assert_ren p x y) (vs \<union> (if x \<in> vs then {y} else {}))"
         "y \<notin> vs"
         "assert_wf p (insert z vs)" "y \<noteq> z" "y \<notin> assert_vars p" "x \<notin> vs" "x \<noteq> z"
  thus " assert_wf (assert_ren p x y) (insert z vs)"
    by (meson assert_wf_mono insert_iff sup_ge1)
qed

corollary assert_wf_ren:
  "y \<notin> assert_vars p \<Longrightarrow> assert_wf p {} \<Longrightarrow> assert_wf (assert_ren p x y) {}"
  apply(rule assert_wf_mono)
   apply(erule assert_ren_wf')
  by auto

lemma pre_ex:
  assumes pre: "pre sh lev (set \<Gamma>1) (assert.Ex x P) f c k k' (s1, H1) (s1', H1')" and
        notin: "x \<notin> invvars \<union> cmd_vars c \<union> assert_free Q"
  assumes ynotin: "y \<notin> invvars \<union> {x} \<union> assert_free P \<union> assert_vars f \<union> cmd_vars c"
  obtains v v' where 
        "pre sh lev (set \<Gamma>1) P (mf f x y) c (conf_upd (conf_upd k x v) y (s1 x)) (conf_upd (conf_upd k' x v') y (s1' x)) ((s1(x := v,y := (s1 x))), H1) ((s1'(x := v',y := (s1' x))),H1')"
  using assms  apply(clarsimp  elim!: pre_elim)
  apply(erule inv_exD, simp_all)
  apply(rule that)
  apply(rule pre_intro)
  using cmd_mod_vars assert_ren_free_subset
         apply blast
        apply clarsimp
       apply clarsimp
  using assert_wf_mono apply blast
  using assert_wf_ren apply blast
  using assert_free_ex_subset cmd_mod_vars by(auto intro: step_ex)

(* FIXME: move these 3 lemmas to Semantics.thy or similar *)
lemma step_ex_blah:
  "cmd_step \<sigma> (Run c (set \<Gamma>) s H) k' \<Longrightarrow> x \<notin> cmd_vars c \<Longrightarrow> cmd_step \<sigma> (Run c (set \<Gamma>) (s(x := v)) H) k'' \<Longrightarrow> k' = (conf_upd k'' x (s x))"
  apply(drule_tac s="s(x := v)" and v="s x" in step_ex, assumption)
  apply simp
  done

lemma conf_upd_twice[simp]:
  "conf_upd (conf_upd k x v) x v' = conf_upd k x v'"
  by(case_tac k, auto)

lemma conf_upd_ind[simp]:
  "x \<noteq> y \<Longrightarrow> conf_upd (conf_upd k y v) x v' = conf_upd (conf_upd k x v') y v"
  by(case_tac k, auto)


lemma secure_ex:
 "(secure sh lev n P c Q) \<Longrightarrow> x \<notin> invvars \<union> cmd_vars c \<union> assert_free Q \<Longrightarrow>
   assert_wf (Ex x P) {} \<Longrightarrow>
   secure sh lev n (assert.Ex x P) c Q"
proof (induction n arbitrary: c P Q rule: nat_less_induct)
  case (1 n) 
  show ?case
  proof(rule secure_intro')
    fix m \<Gamma>1 s1 s1' H1 H1' k k' f
    assume n_def: "n = Suc m"
    assume pre: "pre sh lev (set \<Gamma>1) (assert.Ex x P) f c k k' (s1, H1) (s1', H1')"

    from 1(2) have "secure sh lev n P c Q" by blast
    hence H: "\<And>\<Gamma>1 s1 s1' H1 H1' k k' f.
       pre sh lev (set \<Gamma>1) P f c k k' (s1, H1) (s1', H1') \<Longrightarrow>
       rec sh lev m (set \<Gamma>1) P f c k k' Q (s1, H1) (s1', H1')"
      using n_def secure_elim by metis

    obtain y where
    ynotin: "y \<notin> invvars \<union> {x} \<union> assert_free P \<union> assert_vars f \<union> cmd_vars c"
      using fresh_var invvars_finite assert_free_finite cmd_vars_finite  assert_vars_finite
      by (metis finite.emptyI finite.insertI finite_UnI)

    from pre obtain t1 t1'  l l' v v' where
      t1_def: "t1 = (s1(x := v,y := (s1 x)))" and
      t1'_def: "t1' = (s1'(x := v',y := (s1' x)))" and
      l_def: "l = conf_upd (conf_upd k x v) y (s1 x)" and
      l'_def: "l' = conf_upd (conf_upd k' x v') y (s1' x)" and
      pre_upd: "pre sh lev (set \<Gamma>1) P (mf f x y) c l l' (t1, H1) (t1',H1')" using pre_ex 1 ynotin 
      by metis

    have [simp]: "x \<noteq> y" using ynotin by blast
    have k_def: "k = conf_upd (conf_upd l x (s1 x)) y (s1 y)"
      apply(simp add: l_def)
      by (metis (mono_tags, lifting) "1.prems"(2) UnI1 UnI2 fun_upd_triv pre pre_elim_ step_ex_blah ynotin)  

    have k'_def: "k' = conf_upd (conf_upd l' x (s1' x)) y (s1' y)"
      apply(simp add: l'_def)
      by (metis (mono_tags, lifting) "1.prems"(2) UnI1 UnI2 fun_upd_triv pre pre_elim_ step_ex_blah ynotin)

    have "m < n" using n_def by auto

    have \<Gamma>1locks: "set \<Gamma>1 \<subseteq> locks" using pre pre_elim by blast

    have wf1: "assert_wf (lockinvs \<Gamma>1) {}"
      using \<Gamma>1locks lockinvs_wf locks_wf by blast

    have xnotin1: "x \<notin> assert_free (lockinvs \<Gamma>1)"
      using 1(3) \<Gamma>1locks
      unfolding invvars_def invs_def lockinvs_assert_free  by blast

    hence xnotin2: "x \<notin> lows_free (lockinvs \<Gamma>1)"
      using lows_free_assert_free_subset by blast

    hence wf3: "assert_wf (lockinvs \<Gamma>1) {x}"
      using assert_wf_unI[OF wf1, where vs'="{x}"] by auto

    have xnotin3: "x \<notin> assert_free (maybe_shared sh)"
      using 1(3) unfolding invvars_def invs_def by auto
    hence xnotin4: "x \<notin> lows_free (maybe_shared sh)"
      using lows_free_assert_free_subset by blast

    hence wf2: "assert_wf (maybe_shared sh) {x}"
      using assert_wf_unI[OF shared_wf, where vs'="{x}"] by auto

    have wf4: "assert_wf P {x}" using "1.prems"(3) by simp
    hence wf5: "assert_wf P {}" using assert_wf_mono by blast

    have wf: "assert_wf (lockinvs \<Gamma>1) {}"
             "assert_wf (maybe_shared sh) {x}" 
             "assert_wf (lockinvs \<Gamma>1) {x}"
             "assert_wf P {x}"
             "assert_wf P {}" using wf1 wf2 wf3 wf4 wf5 by blast+

    have lows_P[simp]: "\<And> s v. assert_lows lev P (s(x := v)) = assert_lows lev P s" 
      using wf assert_wf_lows by blast 

    from pre_upd H have
    "rec sh lev m (set \<Gamma>1) P (mf f x y) c l l' Q (t1, H1) (t1', H1')" by blast

    thus "rec sh lev m (set \<Gamma>1) (assert.Ex x P) f c k k' Q (s1, H1) (s1', H1')"
      (* use case analysis on the rec assumption *)
      apply(rule rec_elim)

       (* the stop stop case *)
       apply(rename_tac t2 H2 t2' H2')
       apply(simp add: k_def k'_def)
       apply(rule secure_stop)
        apply(rule inv_undo_mf)
             apply assumption
      using "1.prems"(2) apply blast
      using step_elim apply blast  
      using l_def ynotin apply auto[1]
      using l'_def apply auto[1]
      using ynotin using step_elim
        apply blast
      using `x \<notin> invvars \<union> cmd_vars c \<union> assert_free Q` 
       apply(subgoal_tac "set \<Gamma>1 \<subseteq> locks \<and> y \<notin> invvars \<union> cmd_vars c \<union> assert_free Q")

        (* FFS horrible to prove step here *)
      using ynotin  
        apply(clarsimp elim!: step_elim intro!: step_intro split del: if_splits simp: t1_def t1'_def invvars_def invs_def)[1]
         apply(rule step_intro, clarsimp+)
            apply (metis (no_types, lifting) UN_insert UnE invs_def invvars_def subsetCE)
           apply blast 
          apply clarsimp
      using "1.prems"(2) ynotin 
         apply auto[1] 
        apply (clarsimp split del: if_splits)
         apply(intro conjI)
      using lows_free_assert_free_subset
           apply (smt Diff_empty Diff_insert0 Diff_subset_conv insert_Diff subsetCE)
      using lows_free_assert_free_subset 
         apply blast 
         apply(subgoal_tac "x \<notin> lows_free (lockinvs \<Gamma>')")
      using lows_free_assert_free_subset 
         apply blast
        apply(clarsimp simp: lockinvs_lows_free)
        apply (metis lows_free_assert_free_subset subsetCE)
        (* end proof of step *) 

       (* prove the subgoal_tac above *)
      using ynotin
       apply(auto elim!: star_elim ex_elim)[1]
        apply (meson pre_elim pre_upd subsetCE)  
       apply (meson Un_iff contra_subsetD step_elim)

      (* this is the run run case of rec implies rec *)
      apply(rename_tac t2 H2 t2' H2')
      apply(subgoal_tac "assert_wf p2 {} \<and> assert_wf p2 {x}")

      (* first prove that secure holds recursively (using the induction hypothesis 1(1) *)
      apply(subgoal_tac "secure sh lev m (assert.Ex x p2) c2 Q")
       prefer 2
       apply(subgoal_tac "x \<notin> cmd_vars c2") 
        apply(rule 1(1)[rule_format])
           apply(simp add: n_def)
          apply assumption
      using 1(3) apply fastforce
         apply fastforce

      using cmd_step_preserves_vars
        apply (metis "1.prems"(2) UnI1 UnI2 conf_vars.simps(3) pre_elim pre_upd subsetCE) (* slow *)

      (* now that we know secure holds recursively, we can prove rec implies rec for the run run case *)
      apply(simp add: k_def k'_def)
      apply(rule_tac ?p2.0="assert.Ex x p2" in secure_rec)
        apply(subgoal_tac "s1 x = t2 y \<and> s1' x = t2' y")
         apply(rule_tac w="s1 y" and w'="s1' y" in inv_exI)
              apply assumption
      using "1.prems"(2) apply blast
      using step_elim apply blast
           apply clarsimp
          apply clarsimp
         apply (metis (no_types, lifting) Un_iff step_elim subsetCE ynotin)
        apply(simp add: l_def l'_def) 
        apply(case_tac k, simp+)
        apply(case_tac k', simp+)
        apply(fastforce)

       (* prove step, horribly *)
       apply(erule step_elim)
       apply(rule step_intro)
           apply auto[3]       
        using wf "1.prems"(2) apply (clarsimp split del: if_splits) 
        apply(subgoal_tac "cmd_mod c \<inter> assert_free f = {} \<and> cmd_mod c \<inter> invvars = {} \<and> set \<Gamma>1 \<subseteq> locks")
      using assert_wf_lows assert_wf_mono
      using "1.prems"(2) t1_def t1'_def ynotin 
          apply (smt UnI1 Un_iff assert_lows.simps(1) assert_lows_coincidence locks_lows_coincidence shared_lows_coincidence singletonI subsetCE) 
      using pre_elim pre
         apply metis
        (* prove lows_free containmant *)
        apply (clarsimp split del: if_splits)
        apply(rule conjI)                   
         apply (clarsimp split del: if_splits)
         apply blast
        apply (metis wf Diff_empty Diff_insert0 assert_wf_lows_free insert_disjoint(1))
       apply assumption

      (* prove assert_wf properties *)
      apply(rule context_conjI)
      using step_elim apply blast
      (* prove assert_wf (assert.Ex x p2) {} *)
      apply(rule_tac vs="{}" and p="Star P (Star (maybe_shared sh) (lockinvs \<Gamma>1))" in lows_free_assert_wf_mono)
      using wf shared_wf by(auto elim!: step_elim)
  qed
qed

lemma step_refl[simp]:
"step sh lev c \<Gamma> p \<Gamma> p s s = (set \<Gamma> \<subseteq> locks \<and> assert_wf p {})"
  unfolding step.simps by auto

lemma step_trans:
  assumes "step sh lev c1 \<Gamma>1 p1 \<Gamma>2 p2 s1 s2"
  assumes "step sh lev c2 \<Gamma>2 p2 \<Gamma>3 p3 s2 s3"
  assumes "cmd_vars c2 \<subseteq> cmd_vars c1"
  shows   "step sh lev c1 \<Gamma>1 p1 \<Gamma>3 p3 s1 s3"
  using assms by (elim step_elim, intro step_intro, auto split: if_splits)

lemma post_coincide:
  assumes "inv sh lev \<Gamma>' q f (s,H) (s',H')"
  assumes "step sh lev c \<Gamma> p \<Gamma>' q s0 s"
  shows   "coincide H H' (assert_lows lev p s0)"
          "coincide H H' (assert_lows lev (maybe_shared sh) s0)"
          "coincide H H' (assert_lows lev (lockinvs \<Gamma>) s0)"
proof -
  let ?shar = "maybe_shared sh"
  have
    "coincide H H' (assert_lows lev (Star q (Star ?shar (lockinvs \<Gamma>'))) s)"
  proof
    show
      "star (assert_sem lev f) (assert_sem lev (Star q (Star ?shar (lockinvs \<Gamma>')))) (s, H) (s', H')"
      using assms(1) unfolding inv_def by (simp add: star_ac star_ass star_com split: if_splits)
    show
      "lev \<le> lev" ..
  qed

  moreover have
    "assert_lows lev p s0 \<subseteq> assert_lows lev q s  \<union> (assert_lows lev ?shar s \<union> assert_lows lev (lockinvs \<Gamma>') s)"
    using assms(2) by (elim step_elim)

  moreover have
    "assert_lows lev (lockinvs \<Gamma>) s0 \<subseteq> assert_lows lev q s  \<union> (assert_lows lev ?shar s \<union> assert_lows lev (lockinvs \<Gamma>') s)"
    using assms(2) by (elim step_elim)

  moreover have
    "assert_lows lev ?shar s0 \<subseteq> assert_lows lev q s  \<union> (assert_lows lev ?shar s \<union> assert_lows lev (lockinvs \<Gamma>') s)"
    using assms(2) by (elim step_elim)


  ultimately show "coincide H H' (assert_lows lev p s0)"
                  "coincide H H' (assert_lows lev ?shar s0)"
                  "coincide H H' (assert_lows lev (lockinvs \<Gamma>) s0)"
      apply -
       by ((rule coincide_mono, auto)[1])+
qed

lemma secure_steps:
  assumes
    (* pick a large enough step counter,
       can always shorten using secure_prefix *)
    "secure sh lev (length \<sigma>) p1 c1 q"
  assumes
    "cmd_steps \<sigma> (Run c1 (set \<Gamma>1) s1  H1)  k"
    "cmd_steps \<sigma> (Run c1 (set \<Gamma>1) s1' H1') k'"
    "cmd_mod c1 \<inter> assert_free f = {}"
    "cmd_mod c1 \<inter> invvars = {}"
    "set \<Gamma>1 \<subseteq> locks" "assert_wf p1 {}" "assert_wf f {}"
    "inv sh lev \<Gamma>1 p1 f (s1,H1) (s1',H1')"
  obtains
    (stop) \<Gamma> s H s' H' where
      "k = Stop (set \<Gamma>) s H"
      "k' = Stop (set \<Gamma>) s' H'"
      "inv sh lev \<Gamma> q f (s,H) (s',H')"
      "step sh lev c1 \<Gamma>1 p1 \<Gamma> q s1 s" |
    (rec) p \<Gamma> c s H s' H' where
      "k = Run c (set \<Gamma>) s H"
      "k' = Run c (set \<Gamma>) s' H'"
      "inv sh lev \<Gamma> p f (s,H) (s',H')"
      "step sh lev c1 \<Gamma>1 p1 \<Gamma> p s1 s"
  using assms
proof (induction "(length \<sigma>)" arbitrary: \<sigma> \<Gamma>1 p1 c1 s1 H1 s1' H1' rule: nat_less_induct)
  case 1
  note hyp = 1(1)
  note step_stop = 1(2)
  note step_rec = 1(3)
  note sec = 1(4)
  note steps = 1(5) 1(6)
  note side = 1(7-11)
  note inv = 1(12)

  from steps
  show ?case proof (cases rule: steps_both)
    case refl
    with step_rec inv side show ?thesis by auto
  next
    case (step \<sigma>1 \<sigma>2 k2 k2')
    from step have "length \<sigma> \<noteq> 0"
      by auto
    then obtain n where n: "length \<sigma> = Suc n"
      using not0_implies_Suc by blast
    with sec have
      sec: "secure sh lev (Suc n) p1 c1 q"
      by simp
    have
      pre: "pre sh lev (set \<Gamma>1) p1 f c1 k2 k2' (s1, H1) (s1', H1')"
      using side step(3) step(4) inv by (auto intro: pre_intro)
    
    from secure_step[OF sec pre]
    show ?thesis proof (cases rule: rec_elim)
      case (stop \<Gamma>' s2 H2 s2' H2')
      show ?thesis using stop step by (auto intro: step_stop)
    next
      case (rec p2 \<Gamma>2 c2 s2 H2 s2' H2')
      from step(1-2) have \<sigma>2: "length \<sigma>2 < length \<sigma>" by simp
      with n have "length \<sigma>2 \<le> n" by simp
      then have sec':
        "secure sh lev (length \<sigma>2) p2 c2 q" using secure_prefix[OF rec(5)] by simp

      (* abbreviate this specific instance of the subformula so that we only have to look at it occasionally *)
      define A where "A = (\<forall>\<Gamma> s H. k = Stop (set \<Gamma>) s H \<longrightarrow> (\<forall>s' H'. k' = Stop (set \<Gamma>) s' H' \<longrightarrow> local.inv sh lev \<Gamma> q f (s, H) (s', H') \<longrightarrow> step sh lev c2 \<Gamma>2 p2 \<Gamma> q s2 s \<longrightarrow> thesis))"
      define B where "B = (\<forall>c \<Gamma> s H. k = Run c (set \<Gamma>) s H \<longrightarrow> (\<forall>s' H'. k' = Run c (set \<Gamma>) s' H' \<longrightarrow> (\<forall>p. local.inv sh lev \<Gamma> p f (s, H) (s', H') \<longrightarrow> step sh lev c2 \<Gamma>2 p2 \<Gamma> p s2 s \<longrightarrow> thesis)))"

      from hyp have hyp:
        "\<lbrakk>length \<sigma>2 < length \<sigma>;
          length \<sigma>2 = length \<sigma>2;
          secure sh lev (length \<sigma>2) p2 c2 q;
          cmd_mod c2 \<inter> assert_free f = {};
          cmd_mod c2 \<inter> invvars = {};
          set \<Gamma>2 \<subseteq> locks; assert_wf p2 {}; assert_wf f {};
          inv sh lev \<Gamma>2 p2 f (s2, H2) (s2', H2');
          cmd_steps \<sigma>2 (Run c2 (set \<Gamma>2) s2 H2) k;
          cmd_steps \<sigma>2 (Run c2 (set \<Gamma>2) s2' H2') k';
          A; B\<rbrakk>
         \<Longrightarrow> thesis"
        using A_def B_def by metis

      from rec cmd_step_preserves_mod[OF step(3)] have
        mod:  "cmd_mod c2 \<subseteq> cmd_mod c1" by simp
      from rec cmd_step_preserves_vars[OF step(3)] have
        vars: "cmd_vars c2 \<subseteq> cmd_vars c1" by simp

      show ?thesis proof (rule hyp[OF \<sigma>2 refl sec'])
        from rec side step mod show
          "cmd_mod c2 \<inter> assert_free f = {}"
          "cmd_mod c2 \<inter> invvars = {}"
          "set \<Gamma>2 \<subseteq> locks"
          "local.inv sh lev \<Gamma>2 p2 f (s2, H2) (s2', H2')"
          "cmd_steps \<sigma>2 (Run c2 (set \<Gamma>2) s2 H2) k"
          "cmd_steps \<sigma>2 (Run c2 (set \<Gamma>2) s2' H2') k'"
          by (auto elim!: step_elim)
      next
        from vars show A unfolding A_def using step_trans[OF rec(4)] by (auto intro: step_stop)
      next
        from vars show B unfolding B_def using step_trans[OF rec(4)] by (auto intro: step_rec)
      next
        show "assert_wf p2 {}" using rec(4) step_elim by blast
      next
        show "assert_wf f {}" using 1(11) by assumption
      qed
    qed
  qed
qed

end

end