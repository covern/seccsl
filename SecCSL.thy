theory SecCSL
  imports Soundness
begin

context concurrency
begin

(* Pure vs relational implications *)
lemma Imp_weaken:
  "follows lev (Rel (Pure (Imp p q)))
               (Rel (RImp (Pure p) (Pure q)))"
  unfolding follows_def subset_def implies_def 
  by auto

(* Conditional labels vs conditional labelling *)
lemma Sec_cond:
  "  assert_sem lev (Stars [Rel (Sec b (level_expr lev)), Rel (Sec e (Ite b l1 l2))])
   = assert_sem lev (Stars [Rel (Sec b (level_expr lev)), (Cond b (Rel (Sec e l1)) (Rel (Sec e l2)))])"
  apply (intro pred_eq)
  by (auto simp: cond_def sec.simps pure_sem_def)

lemma Cond_true:
  assumes "\<And> s. assert_lows lev q s \<subseteq> assert_lows lev p s"
  shows "follows lev (Star (Rel (Pure b)) (Cond b p q)) p"
  unfolding follows_def implies_def subset_def
  using assms by auto

lemma Cond_false:
  assumes "\<And> s. assert_lows lev p s \<subseteq> assert_lows lev q s"
  shows "follows lev (Star (Rel (Pure (Not b))) (Cond b p q)) q"
  unfolding follows_def implies_def subset_def
  using assms by auto

lemma sec_cong:
  assumes "\<And> e. e \<in> set es \<Longrightarrow> assert_sem lev (Rel (Sec e l)) (s,h) (s',h')"
  assumes "es \<noteq> []"
  shows   "assert_sem lev (Rel (Sec (App f es) l)) (s,h) (s',h')"
proof -
  from assms obtain e where "e \<in> set es"
    by fastforce
  with assms have"h = Map.empty" "h' = Map.empty" "expr_sem l s = expr_sem l s'"
    by (auto simp: sec.simps)
  with assms show ?thesis
    by (auto simp: sec.simps expr_sems_map intro!: map_cong)
       (metis (no_types, lifting) map_eq_conv)
qed

(* Proposition 1: (13) expressed as a proper entailment *)
lemma prop_one_12:
  "follows l (And (Rel (Pure (Eq e\<^sub>p e\<^sub>v))) (And (Rel (Pure (Eq (level_expr l\<^sub>1) (level_expr l\<^sub>2)))) (Rel (Sec e\<^sub>p (level_expr l\<^sub>1))))) (Rel (Sec e\<^sub>v (level_expr l\<^sub>2)))"
  apply (clarsimp simp: sec.simps follows_def implies_def subset_def Lower_def pure_sem_def)
  apply(rule conjI)
   apply (auto simp: Eq_def; metis to_val_to_level true_neq_false)[1]
  by blast

(* Proposition 1: (14) *)
lemma prop_one_14:
  "follows l (And (Rel (Sec e l\<^sub>1)) (And (Rel (Pure (Lower l\<^sub>1 l\<^sub>2))) (Rel (Sec l\<^sub>2 (level_expr l))))) (Rel (Sec e l\<^sub>2))"  
  apply (clarsimp simp: sec.simps follows_def implies_def subset_def Lower_def pure_sem_def, safe)
  using true_neq_false dual_order.trans by blast

(* Proposition 1: (15) *)
lemma prop_one_15:
  "follows l (Rel (Sec l' (level_expr l))) (Rel (Sec (App f []) l'))"  
  by (clarsimp simp: sec.simps follows_def implies_def subset_def Lower_def pure_sem_def)

lemma prop_one_15_low:
  "follows l Emp (Rel (Sec (App f []) (level_expr l)))"  
  by (clarsimp simp: sec.simps follows_def implies_def subset_def Lower_def pure_sem_def level_expr_def)

(* Proposition 1: (20) *)

lemma prop_one_20:
  assumes "\<forall> s. assert_lows l p s = assert_lows l q s"
  shows   "follows l (Star (Rel (Pure b)) (Cond b p q)) p"
  unfolding follows_def implies_def subset_def using assms by auto

lemma prop_one_20':
  assumes "\<forall> s. assert_lows l p s = assert_lows l q s"
  shows   "follows l (Star (Rel (Pure (Not b))) (Cond b p q)) q"
  unfolding follows_def implies_def subset_def using assms by auto

fun Ands :: "expr list \<Rightarrow> expr \<Rightarrow> assert" where
"Ands [] l = Emp" |  (* not needed but helps with some lemmas *)
"Ands [e] l = Rel (Sec e l)" |
"Ands (e#es) l = (And (Rel (Sec e l)) (Ands es l))"

lemma assert_free_Ands[simp]:
  "assert_free (Ands (e#es) l) = (expr_free e \<union> expr_free l \<union> (assert_free (Ands es l)))"
  by (induct es l rule: Ands.induct, auto simp: level_expr_def)

lemma assert_lows_Ands [simp]:
  "assert_lows lev (Ands es l) s = {}"
  by (induct es l rule: Ands.induct, auto)

lemma Ands_assert_sec:
  "assert_sem lev (Ands es l) (s,h) (s',h') \<Longrightarrow> 
   (\<And>e. e \<in> set es \<Longrightarrow> assert_sem lev (Rel (Sec e l)) (s,h) (s',h'))"
  by (induct es l rule: Ands.induct, auto)

(* Proposition 1 (16) *)
lemma prop_one_16:
  "es \<noteq> [] \<Longrightarrow> follows l' (Ands es l) (Rel (Sec (App f es) l))"
  apply (simp add: follows_def)
  apply (rule conjI)
   apply (clarsimp simp: implies_def)
   apply(rule sec_cong[simplified])
  using Ands_assert_sec apply (fastforce+)[2]
  by(induction es, (force simp: subset_def)+)

(* Proposition 1 (17) -- note that we need to keep the LHS of the entailment on the RHS
   to ensure that the set of low locations doesn't decrease *)
lemma prop_one_17:
  "follows l' (Star (Pto e l e') (Rel (Pure (Lower l (level_expr l'))))) (Star (Pto e l e') (And (Rel (Sec e l)) (Rel (Sec e' l))))"
  unfolding implies_def follows_def subset_def
  by (auto elim!: star_elim intro!: star_intro simp: Lower_def)

(* Proposition 2 (18) *)
lemma prop_one_18:
"\<lbrakk>follows lev p p'; follows lev q q'\<rbrakk>
  \<Longrightarrow> follows lev (Star p q) (Star p' q')"
  by auto

(* Lemma 1 *)
thm assert_lows_coincide (* see Separation.thy *)

(* Lemma 2 *)
lemma secure:
  assumes "prove sh lev p1 c1 q"
  shows   "secure sh lev n p1 c1 q"
using assms proof (induction p1 c1 q arbitrary: n rule: prove.induct)
  case skip
  then show ?case by (rule secure_skip)
next
  case (assign x e \<Gamma>)
  then show ?case by (rule secure_assign)
next
  case (load x p e l \<Gamma>)
  then show ?case by (rule secure_load)
next
  case (store e' l p e)
  then show ?case by (rule secure_store)
next
  case (shared_atomic c lev P Q)
  then show ?case by (blast intro: secure_shared_atomic)
next
  case (lock l)
  then show ?case by (rule secure_lock)
next
  case (unlock l)
  then show ?case by (rule secure_unlock)
next
  case (if_ b p c1 q c2)
  then show ?case by (blast intro: secure_if')
next
  case (while b p c)
  then show ?case by (blast intro: secure_while)
next
  case (seq p c1 r c2 q)
  then show ?case by (blast intro: secure_seq)
next
  case (par p1 c1 q1 p2 c2 q2)
  then show ?case by (auto intro: secure_par)
next
  case (frame c r p q)
  then show ?case by (auto intro: secure_frame)
next
  case (conseq p p' q' q c)
  show ?case apply (rule secure_conseq) using conseq by auto
next
  case (ex sh lev P c Q x n)
  show ?case apply (rule secure_ex) using ex by auto
next
  case (split sh lev b p c q)
  then show ?case by (auto intro: secure_split)
next
  case (rsplit sh lev r p c q)
  then show ?case by (auto intro: secure_rsplit)
qed

(* Corollary 1 *)
corollary safety:
  assumes
    "\<And>n. secure sh lev n p1 c1 q"
  assumes
    "cmd_steps \<sigma> (Run c1 (set \<Gamma>1) s1  H1)  k"
    "cmd_steps \<sigma> (Run c1 (set \<Gamma>1) s1' H1') k'"
    "cmd_mod c1 \<inter> assert_free f = {}" 
    "cmd_mod c1 \<inter> invvars = {}"
    "set \<Gamma>1 \<subseteq> locks" "assert_wf p1 {}" "assert_wf f {}"
    "inv sh lev \<Gamma>1 p1 f (s1,H1) (s1',H1')"
  shows
    "k \<noteq> Abort \<and> k' \<noteq> Abort"
  using assms by (cases rule: secure_steps)  auto

(* Theorem 1 *)
theorem correctness:
  assumes
    "\<And>n. secure sh lev n p c q"
  assumes
    "cmd_steps \<sigma> (Run c (set \<Gamma>1) s1  H1)  (Stop (set \<Gamma>2) s2  H2)"
    "cmd_steps \<sigma> (Run c (set \<Gamma>1) s1' H1') (Stop (set \<Gamma>2) s2' H2')"
    "cmd_mod c \<inter> assert_free f = {}"
    "cmd_mod c \<inter> invvars = {}"
    "set \<Gamma>1 \<subseteq> locks" "assert_wf p {}" "assert_wf f {}"
    "inv sh lev \<Gamma>1 p f (s1,H1) (s1',H1')"
  shows
    "inv sh lev \<Gamma>2 q f (s2,H2) (s2',H2')"
using assms proof (cases rule: secure_steps)
  case (stop \<Gamma> s H s' H')
  then show ?thesis using inv_reorder[of \<Gamma> \<Gamma>2 sh lev q f] by simp
next
  case (rec p \<Gamma> c s H s' H')
  then show ?thesis by simp
qed

inductive conf_loweq :: "'a conf \<Rightarrow> 'a conf \<Rightarrow> 'a set \<Rightarrow> bool" where
"conf_loweq Abort Abort xs" | (* cannot occur *)
"coincide h h' xs \<Longrightarrow> conf_loweq (Stop L s h) (Stop L s' h') xs" |
"coincide h h' xs \<Longrightarrow> conf_loweq (Run c L s h) (Run c L s' h') xs"

(* Theorem 2 *)
theorem noninterference:
  notes if_split[split del]
  notes if_split_asm[split del]
  assumes
    "\<And>n. secure sh lev n p1 c1 q"
  assumes
    "cmd_steps \<sigma> (Run c1 (set \<Gamma>1) s1  H1)  k"
    "cmd_steps \<sigma> (Run c1 (set \<Gamma>1) s1' H1') k'"
    "cmd_mod c1 \<inter> assert_free f = {}"
    "cmd_mod c1 \<inter> invvars = {}"
    "set \<Gamma>1 \<subseteq> locks" "assert_wf p1 {}" "assert_wf f {}"
    "inv sh lev \<Gamma>1 p1 f (s1,H1) (s1',H1')"
  shows
    "conf_loweq k k' (assert_lows lev p1 s1 \<union> assert_lows lev (lockinvs \<Gamma>1) s1 \<union> assert_lows lev (maybe_shared sh) s1)"
  using assms 
  by (cases rule: secure_steps) (auto intro!: post_coincide conf_loweq.intros)+

end

end