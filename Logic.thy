theory Logic
  imports Separation
begin

type_synonym ctx = "lsym list"

locale concurrency = semantics _ _ _ _ _ to_level
  for to_level :: "'a \<Rightarrow> ('level::complete_lattice)" +
  fixes lockinv :: "lsym \<Rightarrow> assert"
  fixes shared :: assert

  fixes locks :: locks
  assumes shared_wf: "assert_wf shared {}"
  assumes locks_wf: "l \<in> locks \<Longrightarrow>  assert_wf (lockinv l) {}" 
  assumes finite_locks[intro]: "finite locks"
 

context concurrency
begin

definition invs :: "assert set" where
"invs \<equiv> (insert shared {lockinv l | l. l \<in> locks})"

definition invvars :: "var set" where
"invvars = \<Union>(assert_free ` invs)"

lemma invvars_finite[simp, intro!]:
  "finite (invvars)"
  by (auto simp: invvars_def invs_def intro!: finite_UN_I finite_image_set)

definition subset :: "('a state \<Rightarrow> 'a set) \<Rightarrow> ('a state \<Rightarrow> 'a  set) \<Rightarrow> bool" where
"subset p q \<equiv> \<forall> s. p s \<subseteq> q s"

lemma
subset_refl[simp]:   "subset p p" and
subset_trans[intro]: "subset p q \<Longrightarrow> subset q r \<Longrightarrow> subset p r"
  unfolding subset_def by blast+

definition follows :: "'level \<Rightarrow> assert \<Rightarrow> assert \<Rightarrow> bool" where
"follows lev p q \<equiv>   implies (assert_sem lev p) (assert_sem lev q)
               \<and> subset (assert_lows lev p) (assert_lows lev q)
               \<and> (assert_free q \<subseteq> assert_free p)
               \<and> (lows_free q \<subseteq> lows_free p)
               \<and> (assert_wf p {} \<longrightarrow> assert_wf q {})"

lemma
follows_refl[simp]:   "follows lev p p" and
follows_trans[intro]: "follows lev p q \<Longrightarrow> follows lev q r \<Longrightarrow> follows lev p r"
  unfolding follows_def by auto

lemma Sec_mono:
  "lev \<le> lev' \<Longrightarrow> follows lev'' (Rel (Sec x (level_expr lev))) (Rel (Sec x (level_expr lev')))"
  by(auto elim!: sec_elim intro!: sec_intro simp add: follows_def implies_def subset_def level_expr_def)

text {*
  The @{term "lev' \<le> lev''"} here is needed to prevent the set of low locations from
  decreasing.
*}
lemma Pto_mono:
  "lev \<le> lev' \<Longrightarrow>  lev' \<le> lev'' \<Longrightarrow> follows lev'' (Pto x (level_expr lev) y) (Pto x (level_expr lev') y)"
  apply(simp add: follows_def implies_def subset_def level_expr_def)
  by (clarsimp elim!: sec_elim intro!: sec_intro simp: sec_intro)
  

lemma subset_Star_intro[intro]:
"subset (assert_lows lev p) (assert_lows lev q)
  \<Longrightarrow> subset (assert_lows lev (Star p r)) (assert_lows lev (Star q r))"
  unfolding subset_def by auto

lemma follows_assert_lows_subset:
"follows lev p q \<Longrightarrow> assert_lows lev p s \<subseteq> assert_lows lev q s"
  unfolding follows_def subset_def by auto

lemma follows_assert_free_subset:
"follows lev p q \<Longrightarrow> assert_free  q \<subseteq> assert_free  p"
  unfolding follows_def by auto

lemma follows_assert_wf:
"follows lev p q \<Longrightarrow> assert_wf p {} \<Longrightarrow> assert_wf q {}"
  unfolding follows_def by auto

lemma follows_lows_free_subset:
"follows lev p q \<Longrightarrow> lows_free  q \<subseteq> lows_free  p"
  unfolding follows_def by auto

lemma follows_frame:
"\<lbrakk>follows lev p q; star (assert_sem lev p) R (s,h) (s',h')\<rbrakk>
  \<Longrightarrow> star (assert_sem lev q) R (s,h) (s',h')"
  unfolding follows_def by auto

lemma follows_Star_intro[intro]:
"follows lev p q \<Longrightarrow> follows lev (Star p r) (Star q r)"
  unfolding follows_def by auto

lemma follows_Star_com[intro]:
"follows lev (Star p q) (Star q p)"
  unfolding follows_def subset_def using star_com by auto

definition lockinvs :: "ctx \<Rightarrow> assert" where
"lockinvs \<Gamma> = Stars (map lockinv (remdups \<Gamma>))"

fun atomic :: "cmd \<Rightarrow> bool" where
"atomic (Assign _ _) = True" |
"atomic (Load _ _) = True" |
"atomic (Store _ _) = True" |
"atomic (Lock _) = True" |
"atomic (Unlock _) = True" |
"atomic _ = False"

lemma atomic_is_atomic [simp]:
  "atomic c \<Longrightarrow> cmd_step sched (Run c \<Gamma> s h) (Run c' \<Gamma>' s' h') \<Longrightarrow> False"
  by(induction c, auto)

abbreviation 
  lows_ind :: "'level \<Rightarrow> assert \<Rightarrow> var \<Rightarrow> bool"
  where
  "lows_ind lev p x \<equiv> \<forall>s v. assert_lows lev p (s(x := v)) = assert_lows lev p s"

inductive prove :: "bool \<Rightarrow> 'level \<Rightarrow> assert \<Rightarrow> cmd \<Rightarrow> assert \<Rightarrow> bool" where
skip:
"prove sh lev Emp Skip Emp" |

assign:
"\<lbrakk>x \<notin> expr_free e; x \<notin> invvars\<rbrakk>
  \<Longrightarrow> prove sh lev Emp (Assign x e) (Rel (Pure (Eq (Var x) e)))" |

load:
"\<lbrakk>x \<notin> expr_frees [p,e,l]; x \<notin> invvars\<rbrakk>
  \<Longrightarrow> prove sh lev (Pto p l e) (Load x p) (Star (Rel (Pure (Eq (Var x) e))) (Pto p l e))" |

store:
"prove sh lev (Star (Rel (Sec e' l)) (Pto p l e)) (Store p e') (Star (Rel (Sec e l)) (Pto p l e'))" |

shared_atomic:
"\<lbrakk>atomic c; prove False lev (Star P shared) c (Star Q shared)\<rbrakk>
  \<Longrightarrow> prove True lev P c Q" |

lock:
"\<lbrakk>l \<in> locks\<rbrakk>
  \<Longrightarrow> prove sh lev Emp (Lock l) (lockinv l)" |

unlock:
"\<lbrakk>l \<in> locks\<rbrakk>
  \<Longrightarrow> prove sh lev (lockinv l) (Unlock l) Emp" |

if_:
"\<lbrakk>prove sh lev (Star (Rel (Pure b)) p) c1 q;
  prove sh lev (Star (Rel (Pure (Not b))) p) c2 q;
  expr_free b \<subseteq> assert_free p\<rbrakk>
  \<Longrightarrow> prove sh lev (Star (Rel (Sec b (level_expr lev))) p) (If b c1 c2) q" |

while:
"\<lbrakk>prove sh lev (Star (Rel (Pure b)) (Star (Rel (Sec b (level_expr lev))) p)) c (Star (Rel (Sec b (level_expr lev))) p);
  expr_free b \<subseteq> assert_free p\<rbrakk>
  \<Longrightarrow> prove sh lev (Star (Rel (Sec b (level_expr lev))) p) (While b c) (Star (Rel (Pure (Not b))) p)" |

seq:
"\<lbrakk>prove sh lev p c1 r; prove sh lev r c2 q\<rbrakk>
  \<Longrightarrow> prove sh lev p (Seq c1 c2) q" |

par:
"\<lbrakk>prove sh lev p1 c1 q1; prove sh lev p2 c2  q2;
  cmd_mod c1 \<inter> cmd_vars c2 = {};
  cmd_mod c2 \<inter> cmd_vars c1 = {};
  cmd_mod c1 \<inter> assert_free p2 = {};
  cmd_mod c1 \<inter> assert_free q2 = {};
  cmd_mod c2 \<inter> assert_free p1 = {};
  cmd_mod c2 \<inter> assert_free q1 = {}\<rbrakk>
  \<Longrightarrow> prove sh lev (Star p1 p2) (Par c1 c2) (Star q1 q2)" |

frame:
"cmd_mod c \<inter> assert_free r = {}
  \<Longrightarrow> prove sh lev p c q \<Longrightarrow> prove sh lev (Star p r) c (Star q r)" |

conseq:
"\<lbrakk>follows lev p p'; follows lev q' q\<rbrakk>
  \<Longrightarrow> prove sh lev p' c q' \<Longrightarrow> prove sh lev p c q" |

ex:
  "prove sh lev P c Q \<Longrightarrow> 
   x \<notin>  invvars \<union> cmd_vars c \<union> assert_free Q \<Longrightarrow> 
   assert_wf (Ex x P) {} \<Longrightarrow>
   prove sh lev (Ex x P) c Q" |

split:
"\<lbrakk>prove sh lev (Star (Rel (Pure b)) p) c q;
  prove sh lev (Star (Rel (Pure (Not b))) p) c q;
  expr_free b \<subseteq> assert_free p\<rbrakk>
  \<Longrightarrow> prove sh lev (Star (Rel (Sec b (level_expr lev))) p) c q" |

rsplit:
"\<lbrakk>prove sh lev (Star (Rel r) p) c q; prove sh lev (Star (Rel (RNot r)) p) c q;
   rexpr_free r \<subseteq> assert_free p\<rbrakk>
  \<Longrightarrow> prove sh lev p c q"

lemma cond:
"\<lbrakk>prove sh lev (Star (Rel (Pure b)) p1) c q;
  prove sh lev (Star (Rel (Pure (Not b))) p2) c q;
  assert_lows lev p1 = assert_lows lev p2; 
  expr_free b \<subseteq> assert_free p1 \<union> assert_free p2\<rbrakk>
  \<Longrightarrow> prove sh lev (Star (Rel (Sec b (level_expr lev))) (Cond b p1 p2)) c q"
proof (intro split, goal_cases)
case 1
  show ?case proof (rule conseq)
    show "follows lev (Star (Rel (Pure b)) (Cond b p1 p2)) (Star (Rel (Pure b)) p1)"
      unfolding follows_def implies_def subset_def using 1 by auto
    show "follows lev q q" by simp
    show "prove sh lev (Star (Rel (Pure b)) p1) c q" by (rule 1(1))
  qed
next
  case 2
  show ?case proof (rule conseq)
    show "follows lev (Star (Rel (Pure (Not b))) (Cond b p1 p2)) (Star (Rel (Pure (Not b))) p2)"
      unfolding follows_def implies_def subset_def using 2 by auto
    show "follows lev q q" by simp
    show "prove sh lev (Star (Rel (Pure (Not b))) p2) c q" by (rule 2(2))
  qed
next
  case 3
  then show ?case by auto
qed

end

end