theory Shared
imports Locks
begin

context concurrency begin

lemma shared_free[intro]:
"assert_free shared \<subseteq> invvars"
  unfolding invvars_def invs_def by auto

lemma shared_sem_coincidence[simp]:
"\<lbrakk>x \<notin> invvars\<rbrakk>
  \<Longrightarrow> assert_sem lev shared (s(x:=v),h) (s'(x:=v'),h') = assert_sem lev shared (s,h) (s',h')"
  by(auto simp: invvars_def invs_def)

lemma shared_lows_coincidence[simp]:
"\<lbrakk>x \<notin> invvars\<rbrakk>
  \<Longrightarrow> assert_lows lev shared (s(x:=v)) = assert_lows lev shared s"
  by(auto simp: invvars_def invs_def)

end

end