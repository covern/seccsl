theory Locks
  imports Logic
begin

context concurrency
begin

lemma remove_lock:
"l \<in> set \<Gamma> \<Longrightarrow> \<exists> \<Gamma>'. set \<Gamma> - {l} = set \<Gamma>'"
proof -
  have "set \<Gamma> - {l} = set (filter (\<lambda> x. x \<noteq> l) \<Gamma>)"
    by auto
  then show "\<exists> \<Gamma>'. set \<Gamma> - {l} = set \<Gamma>'" ..
qed

lemma
lockinvs_Nil[simp]:   "lockinvs [] = Emp" and
lockinvs_Cons[simp]:  "l \<notin> set \<Gamma> \<Longrightarrow> lockinvs (l#\<Gamma>) = Star (lockinv l) (lockinvs \<Gamma>)" and
lockinvs_Cons'[simp]: "l \<in> set \<Gamma> \<Longrightarrow> lockinvs (l#\<Gamma>) = lockinvs \<Gamma>"
  unfolding lockinvs_def by auto

lemma lockinvs_sem_reorder:
"\<lbrakk>set \<Gamma> = set \<Gamma>'\<rbrakk>
  \<Longrightarrow> assert_sem lev (lockinvs \<Gamma>) = assert_sem lev (lockinvs \<Gamma>')"
  unfolding lockinvs_def by (auto intro: stars_reorder_perm perm_map_remdups)

lemma lockinvs_lows:
"assert_lows lev (lockinvs \<Gamma>) s = \<Union> {assert_lows lev (lockinv l) s | l. l \<in> set \<Gamma>}"
  by (induction \<Gamma>, simp, case_tac "a \<in> set \<Gamma>", auto)

lemma lockinvs_lows_free:
"lows_free (lockinvs \<Gamma>) = \<Union> {lows_free (lockinv l) | l. l \<in> set \<Gamma>}"
  by (induction \<Gamma>, simp, case_tac "a \<in> set \<Gamma>", auto)

lemma lockinvs_assert_free:
"assert_free (lockinvs \<Gamma>) = \<Union> {assert_free (lockinv l) | l. l \<in> set \<Gamma>}"
  by (induction \<Gamma>, simp, case_tac "a \<in> set \<Gamma>", auto)

corollary lockinvs_lows_reorder:
"\<lbrakk>set \<Gamma> = set \<Gamma>'\<rbrakk>
  \<Longrightarrow> assert_lows lev (lockinvs \<Gamma>) = assert_lows lev (lockinvs \<Gamma>')"
  by (rule ext, simp add: lockinvs_lows)

corollary lockinvs_lows_free_reorder:
"\<lbrakk>set \<Gamma> = set \<Gamma>'\<rbrakk>
  \<Longrightarrow> lows_free (lockinvs \<Gamma>) = lows_free (lockinvs \<Gamma>')"
  by (simp add: lockinvs_lows_free)

lemma lockinvs_wf:
"assert_wf  (lockinvs \<Gamma>) vs =  (\<forall>l\<in>set \<Gamma>. assert_wf (lockinv l) vs)"
  by (induction \<Gamma>, simp, case_tac "a \<in> set \<Gamma>", auto)

corollary lockinvs_wf_reorder:
"\<lbrakk>set \<Gamma> = set \<Gamma>'\<rbrakk>
  \<Longrightarrow> assert_wf (lockinvs \<Gamma>) vs = assert_wf (lockinvs \<Gamma>') vs"
  by (simp add: lockinvs_wf)

lemma lockinvs_reorder:
  assumes "set \<Gamma> = set \<Gamma>'"
  obtains "assert_lows lev (lockinvs \<Gamma>) = assert_lows lev (lockinvs \<Gamma>')"
          "assert_sem lev (lockinvs \<Gamma>) = assert_sem lev (lockinvs \<Gamma>')"
          "assert_wf (lockinvs \<Gamma>) vs = assert_wf (lockinvs \<Gamma>') vs"
  using lockinvs_sem_reorder lockinvs_lows_reorder assms lockinvs_wf_reorder  
  by blast

(* use the four corollaries below *)
corollary lockinvs_sem_lock:
"\<lbrakk>l \<in> set \<Gamma>; set \<Gamma>' = set \<Gamma> - {l}\<rbrakk>
  \<Longrightarrow> assert_sem lev (lockinvs \<Gamma>) = star (assert_sem lev (lockinv l)) (assert_sem lev (lockinvs \<Gamma>'))"
  using lockinvs_sem_reorder[of \<Gamma> "l#\<Gamma>'" lev] by auto

corollary lockinvs_sem_unlock:
"\<lbrakk>l \<notin> set \<Gamma>; set \<Gamma>' = insert l (set \<Gamma>)\<rbrakk>
  \<Longrightarrow> assert_sem lev (lockinvs \<Gamma>') = star (assert_sem lev (lockinv l)) (assert_sem lev (lockinvs \<Gamma>))"
  using lockinvs_sem_reorder[of  \<Gamma>' "l#\<Gamma>" lev] by auto

corollary lockinvs_lows_lock:
"\<lbrakk>l \<in> set \<Gamma>; set \<Gamma>' = set \<Gamma> - {l}\<rbrakk>
  \<Longrightarrow> assert_lows lev (lockinvs \<Gamma>) s = assert_lows lev (lockinv l) s \<union> assert_lows lev (lockinvs \<Gamma>') s"
  using lockinvs_lows_reorder[of \<Gamma> "l#\<Gamma>'" lev] 
  using list.simps(15) by force

corollary lockinvs_lows_free_lock:
"\<lbrakk>l \<in> set \<Gamma>; set \<Gamma>' = set \<Gamma> - {l}\<rbrakk>
  \<Longrightarrow> lows_free (lockinvs \<Gamma>) = lows_free (lockinv l)  \<union> lows_free (lockinvs \<Gamma>')"
  using lockinvs_lows_free_reorder[of \<Gamma> "l#\<Gamma>'"] 
  using list.simps(15) by force

corollary lockinvs_lows_unlock:
"\<lbrakk>l \<notin> set \<Gamma>; set \<Gamma>' = insert l (set \<Gamma>)\<rbrakk>
  \<Longrightarrow> assert_lows lev (lockinvs \<Gamma>') s = assert_lows lev (lockinv l) s \<union> assert_lows lev (lockinvs \<Gamma>) s"
  using lockinvs_lows_reorder[of  \<Gamma>' "l#\<Gamma>" lev] by simp

corollary lockinvs_lows_free_unlock:
"\<lbrakk>l \<notin> set \<Gamma>; set \<Gamma>' = insert l (set \<Gamma>)\<rbrakk>
  \<Longrightarrow> lows_free (lockinvs \<Gamma>') = lows_free (lockinv l) \<union> lows_free (lockinvs \<Gamma>)"
  using lockinvs_lows_free_reorder[of  \<Gamma>' "l#\<Gamma>"] by simp

lemma \<Gamma>_eq[intro!]:     "P \<Gamma> \<Longrightarrow> \<exists>\<Gamma>'. set \<Gamma>' = set \<Gamma> \<and> P \<Gamma>'" by auto
lemma \<Gamma>_insert[intro!]: "P (l # \<Gamma>) \<Longrightarrow> \<exists>\<Gamma>'. set \<Gamma>' = insert l (set \<Gamma>) \<and> P \<Gamma>'" by (drule \<Gamma>_eq, simp)

lemma lockinv_free[intro]:
"l \<in> locks \<Longrightarrow> assert_free (lockinv l) \<subseteq> invvars"
  unfolding invvars_def invs_def by auto

(* FIXME: move to Separation or elsewhere *)
lemma lows_free_assert_free_subset[intro]:
  "lows_free p \<subseteq> assert_free p"
  by(induct p, auto)

lemma lockinv_lows_free_innvars[intro]:
"l \<in> locks \<Longrightarrow> lows_free (lockinv l) \<subseteq> invvars"
  using lockinv_free lows_free_assert_free_subset by auto

lemma lockinv_lows[intro]:
"l \<in> set \<Gamma> \<Longrightarrow> assert_lows lev (lockinv l) s \<subseteq> assert_lows lev (lockinvs \<Gamma>) s"
  unfolding lockinvs_lows by auto

lemma lockinvs_lows_mono[intro]:
"set \<Gamma>1 \<subseteq> set \<Gamma>2 \<Longrightarrow> assert_lows lev (lockinvs \<Gamma>1) s \<subseteq> assert_lows lev (lockinvs \<Gamma>2) s"
  unfolding lockinvs_lows by auto

(* wrt loc *)
lemma locks_sem_coincidence[simp]:
"\<lbrakk>x \<notin> invvars; set \<Gamma> \<subseteq> locks\<rbrakk>
  \<Longrightarrow> assert_sem lev (lockinvs \<Gamma>) (s(x:=v),h) (s'(x:=v'),h') = assert_sem lev (lockinvs \<Gamma>) (s,h) (s',h')"
proof (induction \<Gamma> arbitrary: h h', simp) (* note: heap will be smaller! *)
  case (Cons l \<Gamma>)
    from Cons(2) Cons(3) have
      l: "\<And> h h'. assert_sem lev (lockinv l) (s(x:=v),h) (s'(x:=v'),h') = assert_sem lev (lockinv l) (s,h) (s',h')"
      unfolding invvars_def invs_def by auto
  show ?case proof (cases "l \<in> set \<Gamma>")
    case True
    with Cons show ?thesis by simp
  next
    case False
    with l Cons show ?thesis
      by (auto simp del: fun_upd_apply intro!: star_coincidence)
  qed
qed

lemma locks_lows_coincidence[simp]:
"\<lbrakk>x \<notin> invvars; set \<Gamma> \<subseteq> locks\<rbrakk>
  \<Longrightarrow> assert_lows lev (lockinvs \<Gamma>) (s(x:=v)) = assert_lows lev (lockinvs \<Gamma>) s"
proof (induction \<Gamma>, simp)
  case (Cons l \<Gamma>)
    from Cons(2) Cons(3) have
      l: " assert_lows lev (lockinv l) (s(x:=v)) = assert_lows lev (lockinv l) s"
      unfolding invvars_def invs_def by auto
  then show ?case proof (cases "l \<in> set \<Gamma>")
    case True
    with Cons show ?thesis by simp
  next
    case False
    with l Cons show ?thesis
      by (auto simp del: fun_upd_apply)
  qed
qed

end

end