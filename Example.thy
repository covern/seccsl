theory Example
imports SecCSL
begin


definition locks :: locks where
  "locks \<equiv> {''mutex''}"

lemma finite_locks [simp]:
  "finite locks"
  by(auto simp: locks_def)

text {* We consider a two-element security lattice: *}
datatype level = High | Low


text {* @{term level} forms a (complete) lattice: *}
instantiation level :: complete_lattice
begin

definition top_level_def: "top = High"
definition sup_level_def: "sup d1 d2 = (if (d1 = High \<or> d2 = High) then High else Low)"
definition inf_level_def: "inf d1 d2 = (if (d1 = Low \<or> d2 = Low) then Low else High)"
definition bot_level_def: "bot = Low"
definition less_eq_level_def: "d1 \<le> d2 = (d1 = d2 \<or> d1 = Low)"
definition less_level_def: "d1 < d2 = (d1 = Low \<and> d2 = High)"
definition Sup_level_def: "Sup S = (if (High \<in> S) then High else Low)"
definition Inf_level_def: "Inf S = (if (Low \<in> S) then Low else High)"

instance
  apply (intro_classes)
                 using level.exhaust less_level_def less_eq_level_def inf_level_def sup_level_def apply auto[10]
       apply (metis Inf_level_def level.exhaust less_eq_level_def)
      apply (metis Inf_level_def level.exhaust less_eq_level_def)
     using level.exhaust less_level_def less_eq_level_def inf_level_def sup_level_def Inf_level_def Sup_level_def top_level_def bot_level_def 
     by auto
end

text {* The type of values stored in memory *}
type_synonym val = int

definition true :: val where "true \<equiv> 1"
definition false :: val where "false \<equiv> 0"

lemma true_neq_false [simp]:
  "true \<noteq> false"
  by(auto simp: true_def false_def)

definition low :: val where "low \<equiv> 0"
definition high :: val where "high \<equiv> 1"

definition to_val :: "level \<Rightarrow> val" where "to_val l \<equiv> (if l = High then 1 else 0)"

definition to_level :: "val \<Rightarrow> level" where
  "to_level v \<equiv> (if v = 0 then Low
                 else if v = 1 then High
                 else undefined)"

lemma to_level_bot_top [simp]:
  "to_level low = bot"
  "to_level high = top"
  by(auto simp: to_level_def low_def high_def bot_level_def top_level_def)

fun level_str :: "level \<Rightarrow> string" where
  "level_str High = ''high''" |
  "level_str Low =  ''low''"

lemma to_val_to_level [simp]:
  "to_level (to_val lev) = lev"
  using to_val_def to_level_def 
  using level.exhaust by auto


axiomatization F :: "fsym \<Rightarrow> val list \<Rightarrow> val" where
  low   [simp]: "F (''low'')   [] = low" and
  high  [simp]: "F (''high'')  [] = high" and
  not   [simp]: "F (''not'')   [a] = (if a = true then false else true)" and
  imp   [simp]: "F (''imp'')   [a, b] = (if a = true \<longrightarrow> b = true then true else false)" and
  eq    [simp]: "F (''='')     [a, b] = (if a = b then true  else false)" and
  ite   [simp]: "F (''ite'')   [a, b, c] = (if a = true then b else c)" and
  lower [simp]: "F (''lower'') [a, b] = (if (to_level a) \<le> (to_level b) then true else false)"

lemma to_val_simps [simp]:
  "to_val level.High = high"
  "to_val level.Low = low"
  by(auto simp: to_val_def high_def low_def)

lemma level_sym_works [simp]: "F (  (level_str lev)) [] = to_val lev"
  by(case_tac lev, auto)

abbreviation highexpr :: expr where
  "highexpr \<equiv> App (  ''high'')  []"

abbreviation lowexpr :: expr where
  "lowexpr \<equiv> App (  ''high'')  []"


definition mutex_inv :: assert where
  "mutex_inv \<equiv> (Ex (''v'') (Pto (expr.Var (''REG'')) lowexpr (expr.Var (''v''))))" 

axiomatization lockinv :: "lsym \<Rightarrow> assert" where
  lockinv_mutex [simp]: "lockinv (''mutex'') = mutex_inv"

interpretation conc:  semantics F true false low high to_level to_val level_str
  by(unfold_locales, simp_all)

(* conc is some arbitrary prefix that gets added to the lemma names that are
   generated when interpreting the locale, concurrency is the locale, 
   the rest is a list of parameters.
   here we instantiate the 'shared' invariant as just Emp *)
interpretation conc: concurrency F true false low high to_val level_str to_level lockinv Emp locks
  by(unfold_locales, simp_all add: locks_def mutex_inv_def)
   

declare [[coercion_enabled]]
        [[coercion "Var :: var \<Rightarrow> expr"]]

(*  TODO: add precedence numbers to these abbreviations *)
abbreviation star_notation :: "assert \<Rightarrow> assert \<Rightarrow> assert" (infixr "\<star>" 100) where
  "star_notation \<equiv> Star"

abbreviation pto_notation :: "expr \<Rightarrow> expr \<Rightarrow> expr \<Rightarrow> assert" ("_ \<mapsto>\<^bsup>_\<^esup> _" [200,200,200] 100) where
  "pto_notation \<equiv> Pto"

abbreviation seq_notation  (infixr ";//" 80) where
  "seq_notation \<equiv> Seq"

abbreviation load_notation  ("_ ::= [_]" [200,200] 100) where
  "load_notation \<equiv> Load"

abbreviation store_notation  ("[_] ::= _" [200,200] 100) where
  "store_notation \<equiv> Store"

definition t1 :: "string \<Rightarrow> cmd" where
  "t1 x \<equiv> Lock ''mutex'' ;
          x ::= [''REG''] ;
          [''REG''] ::= x ;
          Unlock ''mutex''"

definition prog :: cmd where
  "prog \<equiv> Par (t1 ''x'') (t1 ''y'')"

lemma emp_star:"conc.follows lev Emp (Star Emp Emp)"
  by (simp add: conc.follows_def conc.subset_def)

lemma star_emp:"conc.follows lev (Star Emp Emp) Emp"
 by (simp add: conc.follows_def conc.subset_def)

lemma [simp]:
  "{lockinv l | l. l \<in> locks} = {mutex_inv}"
  apply(simp add: locks_def)
  done

lemma invvars_simp [simp]:
  "conc.invvars = {''REG''}"
  by(simp add: conc.invvars_def conc.invs_def mutex_inv_def)

abbreviation prove_notation ("_,_ \<turnstile> {_} _ {_}" [200,200,200,200,200] 60)
where
  "prove_notation \<equiv> conc.prove"


(****** proof starts here ********)

(*Start proving 1st sec entailment*)

lemma impl_holds:  "var \<noteq> ''v'' \<Longrightarrow>
       var \<noteq> ''REG'' \<Longrightarrow>
       conc.assert_sem lev (Rel (Pure (Eq (Var var) (Var ''v''))) \<star> Var ''REG'' \<mapsto>\<^bsup>lowexpr\<^esup> Var ''v'') (s, h) (s', h') \<Longrightarrow>
       conc.assert_sem lev (Star (Rel (Sec (Var var) lowexpr)) (Var ''REG'' \<mapsto>\<^bsup>lowexpr\<^esup> Var ''v'')) (s, h) (s', h')"
  apply(simp)  (*what is this saying? what is conc.sec Map.empty? sec vs. Sec?*)
  by (metis (full_types) conc.expr_sem.simps(1) conc.sec_elim conc.sec_intro)

term lowexpr  (*low level in expr form*)

(*seems we need more higher level lemmas for proving entailments*)
lemma sec_entail:"var \<noteq> ''v'' \<Longrightarrow>
    var \<noteq> ''REG'' \<Longrightarrow>
 conc.follows lev ((Rel (Pure (Eq (Var var) (Var ''v'')))) \<star> Var ''REG'' \<mapsto>\<^bsup>lowexpr\<^esup> Var ''v'')
(Star (Rel (Sec (Var var) lowexpr)) (Var ''REG'' \<mapsto>\<^bsup>lowexpr\<^esup> Var ''v'')) "  (*need sugar for And Eq as well..*)
  apply(unfold conc.follows_def)
  apply(rule conjI)
   apply(unfold conc.implies_def)
   apply(rule allI)+   
   apply(rule impI) 
   apply(rule impl_holds)  
   apply(assumption)+
  apply(rule conjI)
   apply(unfold conc.subset_def)
   apply(rule allI)
  apply(simp)+
  done

(*start proving 2nd sec entailment*)

lemma lev_High: "top \<le> lev \<Longrightarrow> lev = High"
  by (metis top.extremum_unique top_level_def) (*how does metis work? how do I access lemma in another file*)

thm conc.assert_lows.induct
lemma reg_visible: "conc.assert_lows High mutex_inv s = {s ''REG''} "
  apply(simp add:mutex_inv_def)
  by (simp add: top_level_def)

lemma REG_visible:"\<And>s. var \<noteq> ''v'' \<Longrightarrow> var \<noteq> ''REG'' \<Longrightarrow> top \<le> lev \<Longrightarrow> s ''REG'' \<in> conc.assert_lows lev mutex_inv s"
proof -
  fix s
  assume "top \<le> lev"
  from this have 1:"lev = High"
    by (simp add: lev_High)
  have 2:"s ''REG'' \<in> conc.assert_lows High mutex_inv s" 
    by (simp add: reg_visible)
  from this and 1 show "s ''REG'' \<in> conc.assert_lows lev mutex_inv s" by auto
qed


lemma impl_holds_2:"var \<noteq> ''v'' \<Longrightarrow> var \<noteq> ''REG'' \<Longrightarrow>
       conc.assert_sem lev (Rel (Sec (Var ''v'') lowexpr) \<star> Var ''REG'' \<mapsto>\<^bsup>lowexpr\<^esup> Var var) (s, h) (s', h') \<Longrightarrow>
       conc.assert_sem lev (lockinv ''mutex'') (s, h) (s', h')"
  apply(simp)   (*what is this saying?*)
  apply(unfold mutex_inv_def)
  apply(simp)+
  apply(simp add:conc.ex.simps)
  apply(simp add:conc.sec.simps)
  apply(simp add:conc.pto.simps)
  by blast
(*what are conc.ex conc.sec conc.pto? difference between Sec and sec etc.*)

  (* conc.expr_sem.simps(1) conc.sec_elim conc.sec_intro*)
  thm conc.ex.simps

lemma sec_entail_2:" var \<noteq> ''v'' \<Longrightarrow>
    var \<noteq> ''REG'' \<Longrightarrow> conc.follows lev (Rel (Sec (Var ''v'') lowexpr) \<star> Var ''REG'' \<mapsto>\<^bsup>lowexpr\<^esup> Var var) (lockinv ''mutex'')" 
  apply(unfold conc.follows_def)
  apply(rule conjI)
   apply(unfold conc.implies_def)
   apply(rule allI)+
   apply(rule impI)
   prefer 2
   apply(rule conjI)
    apply(unfold conc.subset_def)
    apply(simp)+
    apply(rule impI)   (*is s a stack? yeah*)
  apply(rule allI)
  apply(rule REG_visible) 
      apply(assumption)+
   apply clarsimp
   apply(rule conjI)
    apply (metis conc.lockinv_free insert_commute invvars_simp lockinv_mutex locks_def singletonI subset_insertI2)
   apply(clarsimp simp: mutex_inv_def)
  apply(rule impl_holds_2)
  apply(assumption)+
  done

(*prove the thread*)
lemma t1_prove:
  "var \<noteq> ''v'' \<Longrightarrow> var \<noteq> ''REG'' \<Longrightarrow> conc.prove sh lev Emp (t1 var) Emp"
  apply(simp add: t1_def)
  apply(rule conc.seq)
   apply(rule conc.lock)
   apply(simp add: locks_def)
  apply(simp add: mutex_inv_def)
  apply(rule conc.ex)
   prefer 2
    apply (simp)  
   prefer 2
   apply simp
  apply(rule conc.seq)
   apply(rule conc.load)
    apply (simp)
   apply simp
  apply(rule conc.seq)  (*pure exp: do not depend on the heap. *)
   apply(rule conc.conseq)
     apply(rule sec_entail)
      apply(assumption)+
    prefer 2
    apply(rule conc.store)
   prefer 2
   apply(rule conc.unlock)
   apply (simp add: locks_def)
  apply(rule sec_entail_2)
   apply(assumption)+
  done

thm conc.store

(* prove the entire example program*)
lemma prog_prove:
  "conc.prove sh lev Emp prog Emp"
  apply(rule conc.conseq)
    apply(rule emp_star)
   apply(rule star_emp)
  apply(simp add: prog_def)
  apply(rule conc.par)
         apply(rule t1_prove)
          apply(simp)
         apply(simp)
        apply(rule t1_prove) 
         apply(simp)
        apply(simp)
  apply (simp add: t1_def)+
  done

end
