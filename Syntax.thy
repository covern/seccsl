theory Syntax
  imports Main
begin

type_synonym var  = string
type_synonym fsym = string (* functions *)

datatype expr
  = Var var
  | App fsym "expr list"

fun
  expr_free  :: "expr \<Rightarrow> var set" and
  expr_frees :: "expr list \<Rightarrow> var set" where
"expr_free (Var x) = {x}" |
"expr_free (App f as) = expr_frees as" |
"expr_frees [] = {}" |
"expr_frees (e#es) = expr_free e \<union> expr_frees es"

definition Low :: "expr" where
"Low = App (''low'') []"

definition High :: "expr" where
"High = App (''high'') []"

definition Eq :: "expr \<Rightarrow> expr \<Rightarrow> expr" where
"Eq a b = App (''='') [a, b]"

definition Not :: "expr \<Rightarrow> expr" where
"Not p = App (''not'') [p]"

definition Imp :: "expr \<Rightarrow> expr \<Rightarrow> expr" where
"Imp p q = App (''imp'') [p, q]"

definition Lower :: "expr \<Rightarrow> expr \<Rightarrow> expr" where
"Lower l l' = App (''lower'') [l, l']"

definition Ite :: "expr \<Rightarrow> expr \<Rightarrow> expr \<Rightarrow> expr" where
"Ite a b c = App (''ite'') [a, b, c]"

lemma expr_frees_append[simp]:
"expr_frees (as @ bs) = expr_frees as \<union> expr_frees bs"
  by (induction as) auto

lemma
expr_free_Low[simp]:  "expr_free Low = {}" and
expr_free_High[simp]: "expr_free High = {}"
  unfolding Low_def High_def by simp_all

lemma expr_free_Eq[simp]:
"expr_free (Eq a b) = expr_free a \<union> expr_free b"
  unfolding Eq_def by simp

lemma expr_free_Not[simp]:
"expr_free (Not a) = expr_free a"
  unfolding Not_def by simp

lemma expr_free_Imp[simp]:
"expr_free (Imp a b) = expr_free a \<union> expr_free b"
  unfolding Imp_def by simp

lemma expr_free_Ite[simp]:
"expr_free (Ite a b c) = expr_free a \<union> expr_free b \<union> expr_free c"
  unfolding Ite_def by auto

lemma fresh_var:
  assumes "finite (xs :: var set)"
  obtains x where "x \<notin> xs"
  using assms ex_new_if_finite
  using infinite_UNIV_listI by auto

fun
  expr_ren  :: "expr \<Rightarrow> var \<Rightarrow> var \<Rightarrow> expr" and
  expr_rens :: "expr list \<Rightarrow> var \<Rightarrow> var \<Rightarrow> expr list" where
"expr_ren (Var x) y z = (if x = y then Var z else Var x)" |
"expr_ren (App f as) y z = App f (expr_rens as y z)" |
"expr_rens [] y z = []" |
"expr_rens (e#es) y z = expr_ren e y z # expr_rens es y z"

text {* relational expressions *}
datatype rexpr = 
  Pure expr | Sec expr expr | RImp rexpr rexpr

primrec rexpr_ren :: "rexpr \<Rightarrow> var \<Rightarrow> var \<Rightarrow> rexpr" where
  "rexpr_ren (Pure e) x y = (Pure (expr_ren e x y))" |
  "rexpr_ren (Sec e l) x y = (Sec (expr_ren e x y) (expr_ren l x y))" |
  "rexpr_ren (RImp r s) x y = (RImp (rexpr_ren r x y) (rexpr_ren s x y))"

fun rexpr_free :: "rexpr \<Rightarrow> var set" where
"rexpr_free (Pure e) = expr_free e" |
"rexpr_free (Sec e l) = expr_free e \<union> expr_free l" |
"rexpr_free (RImp e f) = rexpr_free e \<union> rexpr_free f"

datatype assert
  = Emp
  | Rel rexpr
  | Pto expr expr expr
  | And  assert assert
  | Star assert assert
  | Cond expr assert assert
  | Ex var assert

fun Stars :: "assert list \<Rightarrow> assert" where
"Stars [] = Emp" |
"Stars (p#ps) = Star p (Stars ps)"

fun assert_free :: "assert \<Rightarrow> var set" where
"assert_free Emp = {}" |
"assert_free (Rel p) = rexpr_free p" |
"assert_free (Pto p l e) = expr_free p \<union> expr_free e \<union> expr_free l" |
"assert_free (And  p q) = assert_free p \<union> assert_free q" |
"assert_free (Star p q) = assert_free p \<union> assert_free q" |
"assert_free (Cond b p q) = expr_free b \<union> assert_free p \<union> assert_free q" |
"assert_free (Ex x p) = assert_free p - {x}"

text {* free and bound variables of an assertion *}
fun assert_vars :: "assert \<Rightarrow> var set" where
"assert_vars Emp = {}" |
"assert_vars (Rel r) = rexpr_free r" |
"assert_vars (Pto p l e) = expr_free p \<union> expr_free e \<union> expr_free l" |
"assert_vars (And  p q) = assert_vars p \<union> assert_vars q" |
"assert_vars (Star p q) = assert_vars p \<union> assert_vars q" |
"assert_vars (Cond b p q) = expr_free b \<union> assert_vars p \<union> assert_vars q" |
"assert_vars (Ex x p) = assert_vars p \<union> {x}"

definition 
  fresh :: "var set \<Rightarrow> var" where
  "fresh xs \<equiv> (SOME y. y \<notin> xs)"

lemma fresh_is_fresh[simp]:
  "finite xs \<Longrightarrow> fresh xs \<notin> xs"
  apply(simp add: fresh_def)
  using someI fresh_var by metis


type_synonym lsym = string (* locks *)

datatype cmd
  = Skip
  | Assign var  expr
  | Load   var  expr
  | Store  expr expr
  | Lock   lsym
  | Unlock lsym
  | Seq cmd cmd
  | Par cmd cmd
  | If expr cmd cmd
  | While expr cmd

fun cmd_vars :: "cmd \<Rightarrow> var set" where
"cmd_vars Skip = {}" |
"cmd_vars (Assign x e) = {x} \<union> expr_free e" |
"cmd_vars (Load x p) = {x} \<union> expr_free p" |
"cmd_vars (Store p e) =  expr_free p \<union> expr_free e" |
"cmd_vars (Lock l) = {}" |
"cmd_vars (Unlock l) = {}" |
"cmd_vars (Seq c1 c2) = cmd_vars c1  \<union> cmd_vars c2" |
"cmd_vars (Par c1 c2) = cmd_vars c1  \<union> cmd_vars c2" |
"cmd_vars (If p c1 c2) = expr_free p \<union> cmd_vars c1 \<union> cmd_vars c2" |
"cmd_vars (While p c)= expr_free p \<union> cmd_vars c"

fun cmd_mod :: "cmd \<Rightarrow> var set" where
"cmd_mod Skip = {}" |
"cmd_mod (Assign x e) = {x}" |
"cmd_mod (Load x p) = {x}" |
"cmd_mod (Store p e) = {}" |
"cmd_mod (Lock l) = {}" |
"cmd_mod (Unlock l) = {}" |
"cmd_mod (Seq c1 c2) = cmd_mod c1  \<union> cmd_mod c2" |
"cmd_mod (Par c1 c2) = cmd_mod c1  \<union> cmd_mod c2" |
"cmd_mod (If p c1 c2) = cmd_mod c1 \<union> cmd_mod c2" |
"cmd_mod (While p c)= cmd_mod c"

lemma cmd_mod_vars:
"cmd_mod c \<subseteq> cmd_vars c"
  by (auto, induction c rule: cmd_mod.induct, auto)

lemma expr_free_finite[simp, intro!]:
  "finite (expr_free e)"
  "finite (expr_frees es)"
   apply(induct rule: expr_free_expr_frees.induct)
  by auto

  
lemma cmd_vars_finite[simp, intro!]: 
  "finite (cmd_vars c)"
  apply(induct rule: cmd_vars.induct)
  by auto

lemma rexpr_free_finite[simp, intro!]:
  "finite (rexpr_free r)"
  by(induct rule: rexpr_free.induct, auto)

lemma assert_free_finite[simp, intro!]:
  "finite (assert_free P)"
  by(induct rule: assert_free.induct, auto)

lemma assert_vars_finite[simp, intro!]:
  "finite (assert_vars P)"
  by(induct P, auto)

fun assert_ren :: "assert \<Rightarrow> var \<Rightarrow> var \<Rightarrow> assert" where
"assert_ren Emp x y = Emp" |
"assert_ren (Rel r) x y = Rel (rexpr_ren r x y)" |
"assert_ren (Pto p l e) x y = Pto (expr_ren p x y) (expr_ren l x y) (expr_ren e x y)" |
"assert_ren (And p q) x y = And (assert_ren p x y) (assert_ren q x y)" |
"assert_ren (Star p q) x y = Star (assert_ren p x y) (assert_ren q x y)" |
"assert_ren (Cond b p q) x y = Cond (expr_ren b x y) (assert_ren p x y) (assert_ren q x y)" |
(* don't bother to avoid bound variable capture, so we assume y \<notin> assert_vars; however do
   rename the bound variable so that afterwards it won't be in assert_vars allowing us to
   e.g. compose a second renaming to undo the first one *)
"assert_ren (Ex z p) x y = (if x=z then (Ex y (assert_ren p x y)) 
                            else Ex z (assert_ren p x y))"


lemma fresh_is_fresh'[simp]:
  "finite vs \<Longrightarrow> y \<in> vs \<Longrightarrow> y = fresh vs \<Longrightarrow> False"
  using fresh_is_fresh by auto

end